"""
Script to test batch processing.
"""

import logging
import os

import ska_ser_logging
from ska_sdp_scripting import ProcessingBlock
from spectral_line_imaging_params import PIPELINE_VERSION, SpectralLineImagingParams

ska_ser_logging.configure_logging()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

SDP_DATA_PVC_NAME = os.getenv("SDP_DATA_PVC_NAME")


def setup_envs(parameters):
    """
    Setup environment variables needed for the pipeline
    """
    parameters = {**parameters, **parameters.get("dask", {})}
    parameters["worker"] = {
        **parameters.get("worker", {}),
        "volume": {
            "name": SDP_DATA_PVC_NAME,
            "path": "/mnt/data",
        },
    }

    parameters["image"] = (
        f"artefact.skao.int/ska-sdp-spectral-line-imaging:{PIPELINE_VERSION}"
    )

    return parameters


def get_upstream_pb_id(pb, pb_id):
    """
    Find the PB id for the upstream script that is started
    with the spectral line imaging. We need this to determine
    in which directory the data are saved by the mswriter.
    Upstream PB needs to be defined in dependencies for
    spectral line imaging.
    """
    dependencies = pb.get_dependencies()
    try:
        upstream_pb_id = dependencies[0].pb_id
    except IndexError:
        # dependencies list is empty
        upstream_pb_id = pb_id

    return upstream_pb_id


def update_parameters_for_execution(parameters, eb_id, upstream_pb_id, pb_id):
    """
    Update the parameters for running the pipeline
    """
    ms_name = parameters.get("msname", None)
    pvc_path = parameters["worker"]["volume"]["path"]
    # default MS dir in chart is /mnt/data/
    # f"/product/{eb_id}/ska-sdp/{upstream_pb_id}/" is given by ADR-55
    ms_dir = f"{pvc_path}/product/{eb_id}/ska-sdp/{upstream_pb_id}"

    ms_path = f"{ms_dir}/{ms_name}"
    results_dir = f"{pvc_path}/product/{eb_id}/ska-sdp/{pb_id}"
    additional_args = parameters.get("additional_args", [])

    model_image_name = parameters.get("skymodel_image", "")
    model_image_path = (
        f"{pvc_path}/product/{eb_id}/ska-sdp/{upstream_pb_id}/{model_image_name}"
    )

    command_args = [
        "run",
        "--input",
        f"{ms_path}",
        "--output",
        f"{results_dir}",
        "--set",
        "parameters.read_model.image",
        f"{model_image_path}",
    ] + additional_args

    parameters["pipeline"] = {
        "enabled": True,
        "name": "sip",
        "command": "spectral-line-imaging-pipeline",
        "args": command_args,
    }

    LOG.info(
        "Spectral line imaging pipeline will run with the following args: %s",
        command_args,
    )


def main(pb):
    """Execute processing"""
    pb.validate_parameters(model=SpectralLineImagingParams)

    parameters = pb.get_parameters()

    parameters = setup_envs(parameters)
    pb_id = os.getenv("SDP_PB_ID")
    upstream_pb_id = get_upstream_pb_id(pb, pb_id)

    # Create work phase
    LOG.info("Creating work phase")
    work_phase = pb.create_phase("Work", [])

    with work_phase:
        eb_id = work_phase._eb_id
        update_parameters_for_execution(parameters, eb_id, upstream_pb_id, pb_id)

        LOG.info("Executing pipeline")
        work_phase.ee_deploy_helm("dask", parameters)

        LOG.info("Done, now idling...")
        work_phase.wait_loop(work_phase.is_eb_finished)


if __name__ == "__main__":
    with ProcessingBlock() as processing_block:
        main(processing_block)
