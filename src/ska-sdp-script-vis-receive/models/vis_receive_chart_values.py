"""vis-receive helm chart values model."""

# pylint: disable=invalid-name

from __future__ import annotations

from typing import Annotated

from models.vis_receive_script_params import VisReceiveParams
from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass

from .k8s_defs import EnvVar, Probe
from .receive_processor_chart_values import ReceiveProcessorValues


@dataclass
class UVW:
    """UVW engine config."""

    disable_astropy_iers_autodownload: bool = True


@dataclass(config=ConfigDict(populate_by_name=True))
class VisReceiveValues:
    """
    Helm values read by vis-receive helm chart.

    Based on ska-sdp-helmdeploy-charts/charts/ska-sdp-helmdeploy-vis-receive/values.yaml
    """

    script: str
    podSettings: list[VisReceiveParams.PodSettings]
    dataProductStorage: Annotated[
        DataProductStorage, Field(alias="data-product-storage")
    ]
    env: list[EnvVar]
    receiver: Receiver
    processors: list[ReceiveProcessorValues]

    @dataclass
    class SdpConfigDb:
        """SdpConfigDb config."""

        host: str
        port: int

    @dataclass
    class ScanProvider:
        """ScanProvider config."""

        execution_block_id: str

    @dataclass
    class TelescopeModel:
        """TelescopeModel config."""

        execution_block_id: str
        telmodel_key: str

    @dataclass
    class Reception:
        """Reception config."""

        stats_receiver_kafka_config: str
        continuous_mode: bool = True
        transport_protocol: str = "udp"

    @dataclass
    class Options:
        """Options config."""

        reception: VisReceiveValues.Reception | None = None
        telescope_model: VisReceiveValues.TelescopeModel | None = None
        scan_provider: VisReceiveValues.ScanProvider | None = None
        sdp_config_db: VisReceiveValues.SdpConfigDb | None = None
        uvw: UVW = Field(default_factory=UVW)

    @dataclass
    # pylint: disable-next=too-many-instance-attributes
    class Receiver:
        """Receiver config."""

        image: str | None = None
        version: str | None = None
        imagePullPolicy: str | None = None
        executable: str | None = None
        streams: int | None = None
        portStart: int | None = None
        instances: int | None = None
        verbose: bool | None = None
        options: VisReceiveValues.Options | None = None
        readinessProbe: Probe | None = None

    @dataclass
    class DataProductStorage:
        """DataProductStorage config."""

        name: str | None = None
        mountPath: str | None = None
