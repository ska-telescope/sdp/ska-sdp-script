Visibility Receive Script
=========================

This script deploys the SDP visibility receiver
and updates the receive addresses attribute of the Processing Block with DNS-based IP addresses.

For a full description
visit the `online documentation <https://developer.skao.int/projects/ska-sdp-script/en/latest/scripts/vis-receive.html`_.
