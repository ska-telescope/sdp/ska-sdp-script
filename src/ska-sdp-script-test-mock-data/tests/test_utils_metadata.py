"""
Unittests for utils_metadata.py
"""

import os
import tempfile
from pathlib import Path
from unittest.mock import Mock, patch

import pytest
import yaml
from benedict import benedict
from generate_mock_data.utils_metadata import (
    METADATA_FILE,
    _add_metadata_files,
    _find_files,
    create_metadata,
    update_detailed_pointing_metadata,
)
from ska_sdp_dataproduct_metadata import ObsCore
from utils import EB_ID, EXPECTED_METADATA, PB_ID, MockPointingTable, assert_json_data


@pytest.fixture(name="metadata_object")
def create_metadata_fixture(default_config):
    """Test generating an initial metadata file with data"""

    with patch(
        "ska_sdp_dataproduct_metadata.metadata.new_config_client"
    ) as mock_config, patch.dict(os.environ, {"SDP_PB_ID": PB_ID}, clear=True):
        mock_config.return_value = default_config
        with tempfile.TemporaryDirectory() as temp_dir:
            metadata = create_metadata(f"{temp_dir}")
            yield metadata


def test_create_metadata_from_env_var(metadata_object):
    """
    Unit test for create_metadata
    """

    result = benedict(metadata_object.output_path, format="yaml")
    assert_json_data(EXPECTED_METADATA, result)


def test_create_metadata_with_pb_id_input(default_config):
    """
    Unit test for create_metadata with specified PB_ID
    """
    with patch(
        "ska_sdp_dataproduct_metadata.metadata.new_config_client"
    ) as mock_config:
        mock_config.return_value = default_config
        with tempfile.TemporaryDirectory() as temp_dir:
            path = f"{temp_dir}/product/{EB_ID}/" f"ska-sdp/{PB_ID}/"

            metadata = create_metadata(path, PB_ID)

            with open(metadata.output_path, "r", encoding="utf8") as file:
                result = yaml.safe_load(file)

            assert_json_data(EXPECTED_METADATA, result)


def test_create_metadata_with_files(default_config):
    """
    Unit test for create_metadata with specified PB_ID and
    associated file
    """

    with patch(
        "ska_sdp_dataproduct_metadata.metadata.new_config_client"
    ) as mock_config:
        mock_config.return_value = default_config
        with tempfile.TemporaryDirectory() as temp_dir:
            path = f"{temp_dir}/product/{EB_ID}/ska-sdp/{PB_ID}/"
            os.makedirs(path)
            # Create initial metadata file
            metadata = create_metadata(path, PB_ID)
            with tempfile.NamedTemporaryFile(dir=path, suffix=".ms") as associated_file:
                expected_metadata = {
                    **EXPECTED_METADATA,
                    "files": [
                        {
                            "crc": None,
                            "description": "Unknown file",
                            "path": (
                                f"/product{associated_file.name.split('product')[1]}"
                            ),
                            "status": "done",
                        }
                    ],
                }

                # Add associated file to metadata
                metadata = create_metadata(path, PB_ID)

                with open(metadata.output_path, "r", encoding="utf8") as file:
                    result = yaml.safe_load(file)

                assert_json_data(expected_metadata, result)


@patch(
    "generate_mock_data.utils_metadata.import_pointingtable_from_hdf5",
    Mock(return_value=MockPointingTable(1)),
)
def test_update_detailed_pointing_obscore(metadata_object):
    """Test updating basic obscore values for pointing script"""

    with tempfile.NamedTemporaryFile(suffix=".hdf5") as file_hdf:
        expected_metadata = {
            **EXPECTED_METADATA,
            "obscore": {
                "instrument_name": ObsCore.SKA_MID,
                "dataproduct_type": ObsCore.DataProductType.POINTING.value,
                "calib_level": ObsCore.CalibrationLevel.LEVEL_0.value,
                "obs_collection": (
                    f"{ObsCore.SKA}/"
                    f"{ObsCore.SKA_MID}/"
                    f"{ObsCore.DataProductType.POINTING.value}"
                ),
                "access_format": ObsCore.AccessFormat.HDF5.value,
                "facility_name": "SKA-Observatory",
                "obs_id": "",
                "access_estsize": round(os.path.getsize(file_hdf.name) / 1024.0),
                "target_name": "mock_source",
                "s_ra": 294.8542,
                "s_dec": -63.7126,
                "t_min": 0.0,
                "t_max": 300.0,
                "em_min": 0.2220684874074074,
                "em_max": 0.2220684874074074,
                "pol_states": "stokesI",
                "pol_xel": 1,
            },
        }

        metadata = update_detailed_pointing_metadata(metadata_object, file_hdf.name)

        with open(metadata.output_path, "r", encoding="utf8") as file:
            result = yaml.safe_load(file)

        assert_json_data(expected_metadata, result)


def test_find_files():
    """Test find files method"""

    with tempfile.TemporaryDirectory() as temp_dir:
        path = f"{temp_dir}/product/{EB_ID}/ska-sdp/{PB_ID}"
        os.makedirs(path)
        Path(f"{path}/file.hdf").touch()
        Path(f"{path}/{METADATA_FILE}").touch()

        expected_files = [
            {
                "name": f"{path}/file.hdf",
                "description": "Unknown file",
            }
        ]

        found_files = _find_files(path)

        assert len(found_files) == len(expected_files)
        for file_idx, file in enumerate(found_files):
            assert_json_data(file, expected_files[file_idx])


def test_add_metadata_files(metadata_object):
    """
    Unit test for add_metadata_files
    """

    file_list = [
        {
            "name": f"/test_mount/product/{EB_ID}/ska-sdp/{PB_ID}/file.hdf",
            "description": "Unknown file",
        }
    ]

    _add_metadata_files(metadata_object, file_list)

    data = metadata_object.get_data()
    added_files = [file.path for file in data.files]
    file_status = [file.status for file in data.files]

    assert added_files == [f"/product/{EB_ID}/ska-sdp/{PB_ID}/file.hdf"]
    assert file_status == ["done"]

    # Add another file to the list
    file_list = [
        {
            "name": f"/test_mount/product/{EB_ID}/ska-sdp/{PB_ID}/file.hdf",
            "description": "Unknown file",
        },
        {
            "name": f"/test_mount/product/{EB_ID}/ska-sdp/{PB_ID}/file2.hdf",
            "description": "Unknown file",
        },
    ]

    _add_metadata_files(metadata_object, file_list)

    # Verify that the original file is not repeated and the new file is added
    data = metadata_object.get_data()
    added_files = [file.path for file in data.files]
    file_status = [file.status for file in data.files]

    assert added_files == [
        f"/product/{EB_ID}/ska-sdp/{PB_ID}/file.hdf",
        f"/product/{EB_ID}/ska-sdp/{PB_ID}/file2.hdf",
    ]
    assert file_status == ["done", "done"]
