.. _test_realtime:

.. include:: ../../../src/ska-sdp-script-test-realtime/README.rst

Processing block parameters
---------------------------

.. autopydantic_settings:: test_realtime_params.TestRealtimeParams

.. include:: ../../../src/ska-sdp-script-test-realtime/CHANGELOG.rst