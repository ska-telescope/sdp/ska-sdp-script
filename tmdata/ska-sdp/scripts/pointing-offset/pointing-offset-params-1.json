{
  "$defs": {
    "DataProductStorage": {
      "description": "PVC name and path to data product storage",
      "properties": {
        "name": {
          "default": null,
          "description": "PVC name pointing to the data product storage.The default value comes from the SDP_DATA_PVC_NAME environment variable.",
          "title": "PVC name",
          "type": "string"
        },
        "mountPath": {
          "default": "/mnt/data",
          "description": "Path where the data product storage PVC is mounted in the pointing pipeline container",
          "title": "Path where the PVC is mounted in the pointing pipeline container",
          "type": "string"
        }
      },
      "title": "DataProductStorage",
      "type": "object"
    },
    "Env": {
      "description": "User-defined Environment variables",
      "properties": {
        "name": {
          "description": "User-defined environment variable name",
          "title": "Environment variable name",
          "type": "string"
        },
        "value": {
          "description": "User-defined environment variable value",
          "title": "Environment variable value",
          "type": "string"
        }
      },
      "required": [
        "name",
        "value"
      ],
      "title": "Env",
      "type": "object"
    },
    "TelescopeModel": {
      "description": "Telescope Model Information",
      "properties": {
        "telmodel_sources": {
          "description": "Telescope model sources",
          "items": {
            "type": "string"
          },
          "title": "Telescope Model sources",
          "type": "array"
        },
        "static_rfi_key": {
          "default": "",
          "description": "Filename/key to static RFI mask in Telescope Model",
          "title": "Static RFI key",
          "type": "string"
        }
      },
      "title": "TelescopeModel",
      "type": "object"
    }
  },
  "additionalProperties": false,
  "description": "Pointing offset script parameters",
  "properties": {
    "version": {
      "default": "0.9.0",
      "description": "Version of the pointing offset calibration pipeline used by the helm chart for deployment",
      "pattern": "^[a-zA-Z0-9_\\.-]+$",
      "title": "Pointing offset calibration pipeline Version",
      "type": "string"
    },
    "image": {
      "default": "artefact.skao.int/ska-sdp-wflow-pointing-offset",
      "description": "The OCI image used by the helm chart to launch the pointing offset calibration pipeline",
      "pattern": "^[a-zA-Z0-9_:\\./-]+$",
      "title": "OCI image of the pointing offset calibration pipeline",
      "type": "string"
    },
    "imagePullPolicy": {
      "default": "IfNotPresent",
      "description": "Kubernetes image pull policy used by the helm chart (Always, IfNotPresent, Never)",
      "pattern": "^(Always|IfNotPresent|Never)$",
      "title": "Pull policy of the image",
      "type": "string"
    },
    "command": {
      "const": "pointing-offset",
      "default": "pointing-offset",
      "description": "The command the helm chart deploys the pointing offset pipeline with",
      "enum": [
        "pointing-offset"
      ],
      "title": "Run command for the pointing offset calibration pipeline",
      "type": "string"
    },
    "encoding": {
      "const": "msgpack_numpy",
      "default": "msgpack_numpy",
      "description": "Encoding used by the QueueConnector to decode pointing data from Kafka",
      "enum": [
        "msgpack_numpy"
      ],
      "title": "Pointing data encoding",
      "type": "string"
    },
    "kube_namespaces": {
      "default": null,
      "description": "Kubernetes namespace where the pointing offset calibration pipeline is deployed. The default is given by the SDP_HELM_NAMESPACE environment variable.",
      "title": "Kubernetes namespace",
      "type": "string"
    },
    "dataProductStorage": {
      "$ref": "#/$defs/DataProductStorage",
      "title": "Data Product storage PVC"
    },
    "kafka_topic": {
      "default": "pointing_offset",
      "description": "Kafka topic that the QueueConnector will use to load pointing offset data from",
      "title": "Kafka topic",
      "type": "string"
    },
    "telescope_model": {
      "$ref": "#/$defs/TelescopeModel",
      "description": "Dictionary of path and file values to specify static data for the pipeline coming from Telescope Model data",
      "title": "Telescope Model reference"
    },
    "env": {
      "description": "User-defined environment variables",
      "items": {
        "$ref": "#/$defs/Env"
      },
      "title": "Environment variables",
      "type": "array"
    },
    "queue_connector_configuration": {
      "default": {},
      "description": "Configuration for connecting to message queue exchanges. This field is deprecated and will be removed once the QC can be configured via data flow objects.",
      "title": "Queue connector configuration",
      "type": "object"
    },
    "num_scans": {
      "default": 5,
      "description": "Expected number of scans in a pointing observation",
      "title": "Number of scans",
      "type": "integer"
    },
    "additional_args": {
      "default": [
        "--use_source_offset_column"
      ],
      "description": "Additional arguments to be passed to the pointing offset calibration pipeline",
      "items": {
        "type": "string"
      },
      "title": "Additional arguments",
      "type": "array"
    },
    "tango_attribute": {
      "default": "pointing_offset",
      "description": "Prefix to the tango attribute name which will hold the pointing offsets on the QueueConnector device. (i.e. '<tango_attribute>_{dish_id}')",
      "title": "Tango attribute",
      "type": "string"
    },
    "args": {
      "description": "CLI arguments for the pointing offset calibration pipeline.",
      "items": {
        "type": "string"
      },
      "title": "Pointing offset calibration pipeline arguments",
      "type": "array"
    }
  },
  "title": "pointing-offset",
  "type": "object"
}