"""Kuberentes resource definitions."""

# pylint: disable=invalid-name

from pydantic.dataclasses import dataclass


@dataclass
class ContainerPort:
    """List of ports to expose from the container.

    https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#ports
    """  # noqa

    containerPort: int
    hostIP: str | None = None
    hostPort: int | None = None
    name: str | None = None
    protocol: str | None = None


@dataclass
class EnvVar:
    """Environment Variable provided to a container.

    https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#environment-variables
    """  # noqa

    name: str
    value: str | None = None


@dataclass
class ExecAction:
    """Exec specifies the action to take.

    https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#LifecycleHandler
    """  # noqa

    command: list[str]
    """Command is the command line to execute inside the container.

    The working directory for the command is root ('/') in the container's filesystem.
    """


@dataclass
class HTTPGetAction:
    """HTTPGetAction describes an action based on HTTP Get requests."""

    port: int | str
    host: str | None = None


@dataclass
class TCPSocketAction:
    """TCPSocketAction describes an action based on opening a socket."""

    port: int | str
    host: str | None = None


@dataclass
class Probe:  # pylint: disable=too-many-instance-attributes
    """Probe describes a health check to be performed against a container.

    https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#Probe
    """  # noqa

    exec: ExecAction | None = None
    httpGet: HTTPGetAction | None = None
    tcpSocket: TCPSocketAction | None = None
    initialDelaySeconds: int | None = None
    terminationGracePeriodSeconds: int | None = None
    periodSeconds: int | None = None
    timeoutSeconds: int | None = None
    failureThreshold: int | None = None
    successThreshold: int | None = None


@dataclass
class ContainerSpec:  # pylint: disable=too-many-instance-attributes
    """A single application container to be run within a pod.

    Definition at
    https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#Container
    """  # noqa

    name: str
    image: str
    imagePullPolicy: str | None = None
    command: list[str] | None = None
    args: list[str] | None = None
    workingDir: str | None = None
    ports: list[ContainerPort] | None = None
    env: list[EnvVar] | None = None
    readinessProbe: Probe | None = None
