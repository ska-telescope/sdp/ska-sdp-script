"""
Validate latest JSON schema against latest Pydantic model.

This script runs in a job (validate_schema) on every commit.
"""

import json

import pytest
from export_schemas import (
    ALLOWED_SCRIPT_NAMES,
    generate_schema,
    get_latest_json_schema,
    get_pydantic_model,
)


def validate_json_against_pydantic(pydantic_model, json_file):
    """Validate latest JSON schema against latest Pydantic model"""
    with open(json_file, encoding="utf-8") as jfile:
        json_data = json.load(jfile)

    schema = generate_schema(pydantic_model)

    assert schema == json_data


@pytest.mark.parametrize(
    "script",
    ALLOWED_SCRIPT_NAMES,
)
def test(script):
    """Run validation as a pytest unit test"""

    json_file = get_latest_json_schema(script)
    model = get_pydantic_model(script)
    validate_json_against_pydantic(model, json_file)
