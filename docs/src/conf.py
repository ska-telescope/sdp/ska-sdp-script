# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import sys
from pathlib import Path


# load extension to create tables of available sdp scripts for documentation
sys.path.insert(0, str(Path("_ext").resolve()))
import create_scripts_tables


# add scripts to system path
sys.path.insert(0, str(Path("../../scripts").resolve()))
from export_schemas import ALLOWED_SCRIPT_NAMES


# add scripts to system path
for script in ALLOWED_SCRIPT_NAMES:
    sys.path.insert(
        0,
        str(Path(f"../../src/ska-sdp-script-{script}").resolve()),
    )


def setup(app):
    app.connect("env-before-read-docs", _create_scripts_tables)


def _create_scripts_tables(app, env, docname):
    create_scripts_tables.main()


# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "SDP Scripts"
copyright = "2019-2025 SKA SDP Developers"
author = "SKA SDP Developers"
version = "1.0.0"
release = "1.0.0"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.intersphinx",
    "sphinxcontrib.autodoc_pydantic",
    "sphinx_copybutton"
]

# needs to be temporarily disabled because of this Issue:
# https://github.com/mansenfranzen/autodoc_pydantic/issues/301
# it affects the pointing-offset script parameters
suppress_warnings = ["docutils"]

exclude_patterns = []

rst_prolog = """
.. |vis-recv-chart| replace:: :external+ska-sdp-helmdeploy-charts:doc:`vis-receive Helm Chart <charts/vis-receive>`
"""

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "ska_ser_sphinx_theme"

# -- Extension configuration -------------------------------------------------

autodoc_pydantic_settings_hide_paramlist = True

# Intersphinx configuration for links to external projects
# KEEP SORTED ALPHABETICALLY
intersphinx_mapping = {
    "ska-sdp-cbf-emulator": (
        "https://developer.skao.int/projects/ska-sdp-cbf-emulator/en/latest",
        None,
    ),
    "ska-sdp-config": (
        "https://developer.skao.int/projects/ska-sdp-config/en/latest",
        None,
    ),
    "ska-sdp-helmdeploy": (
        "https://developer.skao.int/projects/ska-sdp-helmdeploy/en/latest",
        None,
    ),
    "ska-sdp-helmdeploy-charts": (
        "https://developer.skao.int/projects/ska-sdp-helmdeploy-charts/en/latest",
        None,
    ),
    "ska-sdp-integration": (
        "https://developer.skao.int/projects/ska-sdp-integration/en/latest",
        None,
    ),
    "ska-sdp-lmc-queue-connector": (
        "https://developer.skao.int/projects/ska-sdp-lmc-queue-connector/en/latest",
        None,
    ),
    "ska-sdp-mock-dish-devices": (
        "https://ska-telescope-ska-sdp-mock-dish-devices.readthedocs.io/en/latest",
        None,
    ),
    "ska-sdp-notebooks": (
        "https://developer.skao.int/projects/ska-sdp-notebooks/en/latest",
        None,
    ),
    "ska-sdp-spectral-line-imaging": (
        "https://developer.skao.int/projects/ska-sdp-spectral-line-imaging/en/latest",
        None,
    ),
    "ska-sdp-wflow-pointing-offset": (
        "https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest",
        None,
    ),
    "ska-telmodel": (
        "https://developer.skao.int/projects/ska-telmodel/en/latest",
        None,
    ),
}
