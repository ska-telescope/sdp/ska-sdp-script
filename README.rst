SDP Scripts
===========

Repository containing SDP processing scripts.

Standard CI machinery
---------------------

This repository is set up to use the
`Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile>`__
and `CI jobs <https://gitlab.com/ska-telescope/templates-repository>`__
maintained by the System Team. For any questions, please look at the
documentation in those repositories or ask for support on Slack in the
#team-system-support channel.

To keep the Makefiles up to date in this repository, follow the
instructions at:
https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile#keeping-up-to-date

Contributing to this repository
-------------------------------

`Black <https://github.com/psf/black>`__,
`isort <https://pycqa.github.io/isort/>`__, and various linting tools
are used to keep the Python code in good shape. Please check that your
code follows the formatting rules before committing it to the
repository. You can apply Black and isort to the code with:

.. code:: bash

   make python-format

and you can run the linting checks locally using:

.. code:: bash

   make python-lint

The linting job in the CI pipeline does the same checks, and it will
fail if the code does not pass all of them.

Creating a new script release
-----------------------------

To release a processing script, create a branch and update the
following: - In the processing script’s directory: - update the .release
file with the new version - update the pyproject.toml file with the new
version - update the CHANGELOG of the script - update the documentation
as needed - In the root of the repository: - update the scripts.yaml
file and include the new version

Create an MR, get it approved and merge it. Upon merge, the
oci-image-publish job is triggered, which will build and publish the
updated images. It will only publish the new version if that version
does not yet exist in harbor.skao.int.

Next, tag the repository itself by running:

.. code:: bash

   make tag-with-script SCRIPT=<script-directory>

Replace ``<script-directory>`` with the directory name of the script,
e.g. to tag with the pointing-offset script’s release tag run:

.. code:: bash

   make tag-with-script SCRIPT=ska-sdp-script-pointing-offset

This will create and push a git tag to the repository with tag taken
from the .release file of the given script. It will not trigger any jobs
that publish images. This helps finding the code of various script
versions more easily.

Creating a new tmdata release
-----------------------------

Telescope model data residing in the ``tmdata`` directory is released
via the usual release procedure used in other repositories, i.e. by
bumping the version of ska-sdp-scripts and tagging the repository:

- Check out the master branch and pull the latest code
- Update the version number in ``.release``, ``pyproject.toml`` and
  ``docs/conf.py`` with

  - ``make bump-patch-release``,
  - ``make bump-minor-release``, or
  - ``make bump-major-release``

- Create the git tag with ``make git-create-tag``
- Push the changes with ``make git-push-tag``

Once the CI pipeline finished, the new version will be available at the
`Central Artefact
Repository <https://artefact.skao.int/#browse/browse:raw-telmodel:gitlab.com%2Fska-telescope%2Fsdp%2Fska-sdp-script%2Ftmdata>`__.

Dependencies management
-----------------------

Poetry is used as dependency manager for all SDP Processing Scripts. To
install the script’s requirements, ``cd`` into the script’s directory
and run:

.. code:: bash

   poetry install

Note that each script has its own dependencies. If you work with
multiple scripts, you need to make sure the relevant requirements are
installed. Before committing a new branch to the repository,
``pyproject.toml`` needs to be maintained to update the version of
project, dependencies, and other related information. You should add new
dependencies, and remove dependencies that are no longer needed. After
modifying ``pyproject.toml``, you must run the following command to
update the ``poetry.lock`` file:

.. code:: bash

   poetry lock

`SKA repository tooling -
ska-rt <https://gitlab.com/ska-telescope/ska-rt>`__ is also integrated
with each script. It provides a CLI for managing SKA dependencies, which
are listed in the given script’s ``skart.toml`` file.

Run the following command to update dependencies to their latest
development versions published in GitLab:

.. code:: bash

   skart update

If you want to update with versions released to the Central Artefact
Repository, use

.. code:: bash

   skart update release
