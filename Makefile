include .make/base.mk

# Needs to be placed before oci.mk import to properly override
OCI_IMAGE_ROOT_DIR := src

# List of images that we will be considered for publishing
# Update list with new scripts, or if an older one needs releasing
# which is not listed here.
IMAGE_LIST := "ska-sdp-script-pointing-offset \
	ska-sdp-script-test-batch \
	ska-sdp-script-test-dask \
	ska-sdp-script-test-slurm \
	ska-sdp-script-test-realtime \
	ska-sdp-script-test-receive-addresses \
	ska-sdp-script-vis-receive \
	ska-sdp-script-test-mock-data \
	ska-sdp-script-spectral-line-imaging"

ifeq ($(OCI_IMAGE),)
$(info "Setting images to build ...")

RELEASE_SUPPORT := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))/.make-release-support
GET_OCI_IMAGES := $(shell pwd)/bash_scripts/get-oci-images-to-build.sh

# List of OCI images we want to build and publish
OCI_IMAGES := $(shell . $(GET_OCI_IMAGES); getOciImagesToBuild $(OCI_IMAGE_ROOT_DIR) $(IMAGE_LIST))

$(info "Building $(OCI_IMAGES)")
endif

include .make/oci.mk
include .make/python.mk
include .make/tmdata.mk

PYTHON_LINT_TARGET := src/ scripts/

# Treat warnings as errors
DOCS_SPHINXOPTS = -W --keep-going

# E203: whitespace before ':'
# W503: line break before binary operator
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,W503
PYTHON_LINE_LENGTH = 88  # default black line length

# W0212: protected-access
# E0401: import-error -> each script will have its own requirements, not installed for linting
# R0801: duplicate-code -> processing scripts repeat some code that is needed in each
PYTHON_SWITCHES_FOR_PYLINT = --disable=W0212,E0401,R0801

ifneq ($(SCRIPT),)
SCRIPT_TAG=$(shell . $(RELEASE_SUPPORT) ; RELEASE_CONTEXT_DIR="$(OCI_IMAGE_ROOT_DIR)/$(strip $(SCRIPT))" setContextHelper; CONFIG=${CONFIG} setReleaseFile; getTag)
endif

.PHONY: tag-with-script

## TARGET: tag-with-script
## SYNOPSIS: make tag-with-script SCRIPT=<script-dir>
## VARS:
##		SCRIPT=<directory in src of the script to tag with> -- e.g. ska-sdp-script-test-batch
##
##  Tag the repository using "tag" from .release of the given SCRIPT
tag-with-script: ## tag the ska-sdp-script repository with the released script's tag
	echo $(SCRIPT_TAG)
	git tag -a $(SCRIPT_TAG) -m "Release $(SCRIPT_TAG)"
	git push origin $(SCRIPT_TAG)
