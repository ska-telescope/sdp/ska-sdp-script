"""Common Kafka functions."""

import logging
import os
import sys

from kafka.admin import KafkaAdminClient, NewTopic
from kafka.admin.new_partitions import NewPartitions
from kafka.errors import InvalidPartitionsError

if sys.version_info >= (3, 12, 0):
    import six

    sys.modules["kafka.vendor.six.moves"] = six.moves

logger = logging.getLogger(__name__)

KAFKA_HOST = os.getenv("SDP_KAFKA_HOST", "localhost:9092")


def create_topic_with_partitions(topic: str, partitions: int = 1):
    """Create the topic, and set the amount of partitions it should have."""
    admin_client = KafkaAdminClient(
        bootstrap_servers=KAFKA_HOST,
    )

    topics = admin_client.list_topics()
    if topic in topics:
        logger.info("Topic exists: %s", topic)
        try:
            admin_client.create_partitions({topic: NewPartitions(partitions)})
            logger.info("Topic partitions count updated")
        except InvalidPartitionsError as err:
            if f"Topic already has {partitions} partition" in str(err):
                logger.info("Topic already has required partitions")
            else:
                raise err
    else:
        logger.info("Topic doesn't exist: %s", topic)
        topic_list = [
            NewTopic(name=topic, num_partitions=partitions, replication_factor=1)
        ]
        admin_client.create_topics(new_topics=topic_list, validate_only=False)
