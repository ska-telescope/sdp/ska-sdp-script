"""
Unit tests for the model SpectralLineImagingParams
"""

import pytest
from spectral_line_imaging_params import SpectralLineImagingParams


def test_validate_basic_schema():
    """
    Assert if basic schema validation can be performed
    """
    parameters = {
        "msname": "MWA.ps",
        "dask": {
            "worker": {
                "replicas": "4",
                "resources": {
                    "limits": {
                        "cpu": 2,
                        "memory": "1GB",
                    },
                },
            },
        },
        "skymodel_image": "test-model-%s-image.fits",
    }

    SpectralLineImagingParams(**parameters)


def test_additional_args_can_be_passed():
    """
    Assert if additional arguments to the pipeline can be passed
    in parameters
    """
    parameters = {
        "msname": "MWA.ps",
        "additional_args": ["--with-report"],
        "skymodel_image": "test-model-%s-image.fits",
    }

    SpectralLineImagingParams(**parameters)


def test_throw_validation_error_on_incorrect_value():
    """
    Assert if a validation error is thrown when the type of a
    parameter does not match
    """
    parameters = {
        "msname": "MWA.ps",
        "dask": {
            "worker": {
                "volume": {"name": "pvc", "path": "/mnt/data"},
                "replicas": "A",
            },
        },
        "skymodel_image": "test-model-%s-image.fits",
    }

    with pytest.raises(ValueError):
        SpectralLineImagingParams(**parameters)


def test_throw_validate_error_on_unknown_key_in_dask_params():
    """
    Assert if a validation error is thrown when unknown key is passed
    to dask parameters
    """
    parameters = {
        "msname": "MWA.ps",
        "dask": {"foo": "bar"},
        "skymodel_image": "test-model-%s-image.fits",
    }

    with pytest.raises(ValueError):
        SpectralLineImagingParams(**parameters)


def test_throw_validate_error_on_unknown_key_in_dask_worker_params():
    """
    Assert if a validation error is thrown when unknown key is passed
    to dask worker parameters
    """
    parameters = {
        "msname": "MWA.ps",
        "dask": {"worker": {"replicas": 4, "foo": "bar"}},
        "skymodel_image": "test-model-%s-image.fits",
    }

    with pytest.raises(ValueError):
        SpectralLineImagingParams(**parameters)


def test_throw_validation_error_on_missing_model():
    """
    Assert if a validation error is thrown when model image is not passed
    """
    parameters = {
        "msname": "MWA.ps",
        "dask": {
            "worker": {
                "volume": {"name": "pvc", "path": "/mnt/data"},
            },
        },
    }

    with pytest.raises(ValueError):
        SpectralLineImagingParams(**parameters)
