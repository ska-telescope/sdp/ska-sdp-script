.. _spectral_line_imaging_configure:

Configuring the Script
======================

This section describes the configuration of the spectral line imaging script, covering the options available
in the JSON configuration string supplied to AssignResources, the required environment variables, the Helm
chart.

Configuration parameters
------------------------

The spectral line imaging script can be configured using the following Processing Block
parameters which can be added to the JSON configuration string.

Data from the Execution Blocks / Processing Blocks are stored in the Data
Product Storage. The data product storage is accessible through a kubernetes
Persistent Volume Claim (PVC), the name of the PVC is available in the
environment variable ``SDP_DATA_PVC_NAME``. The PVC is mounted into the
container using the mount path.

.. list-table:: "spectral line imaging" Processing Block configuration parameters with default values
   :header-rows: 1

   * - Name
     - Description
     - Default

   * - ``skymodel_image``
     - | Model continuum image or continuum cube in FITS format
     - ``""``

   * - ``additional_args``
     - | Additional arguments to be passed to the spectral line imaging pipeline
       | command
     - ``[]``

   * - ``dask``
     - | Dictionary containing values to be passed on to the Dask Helm chart
       | Refer to README of the :external+ska-sdp-helmdeploy-charts:doc:`Dask Helm chart <charts/dask>`
     - ``{}``

The basic set of mandatory command arguments used by the script consists of:

  - ``eb_id`` is the execution block ID passed to the script by the Processing Block
  - ``msdir`` is the directory where the Measurement Sets (MS) are found - this is
    constructed by the script from the execution block ID and the corresponding upstream
    processing block ID as

    .. code-block:: python

      msdir = f"/mnt/data/product/{eb_id}/ska-sdp/{upstream_pb_id}/"

  - ``msname`` is the name of the measurement set present in ``msdir``
  - ``results_dir`` is the directory to save final data products - this is
    constructed by the script from the execution block ID and the processing block ID as

    .. code-block:: python

      results_dir = f"/mnt/data/product/{eb_id}/ska-sdp/{pb_id}/"
