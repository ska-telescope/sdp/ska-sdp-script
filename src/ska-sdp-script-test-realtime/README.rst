Test Real-Time Script
=====================

The ``test-realtime`` script is designed to test the processing
controller logic concerning processing block dependencies.

The sequence of actions carried out by the script is:

- Claims processing block
- Sets processing block ``status`` to ``WAITING``
- Waits for ``resources_available`` to be ``True``

  - This is the signal from the processing controller that the script
    can run

- Sets processing block ``status`` to ``RUNNING``
- At this point a substantive script would make deployments to do the
  processing
- Sets processing block ``status`` to ``READY``
- Waits for execution block ``status`` to be set to ``FINISHED``

  - This is the signal from the Subarray device that the execution block
    is finished

- Sets processing block ``status`` to ``FINISHED``

The script makes no deployments.

Full description of processing block parameters of this script can be
found at the `bottom of this
page <https://developer.skao.int/projects/ska-sdp-script/en/latest/test-scripts/test-realtime.html#processing-block-parameters>`__.
