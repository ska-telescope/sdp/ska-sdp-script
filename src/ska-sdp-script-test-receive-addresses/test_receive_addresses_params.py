"""
Pydantic model for processing script parameters.
"""

from pydantic import Field
from pydantic.config import ConfigDict
from ska_sdp_scripting.processing_block import ParameterBaseModel


class TestReceiveAddressesParams(ParameterBaseModel):
    """
    test-receive-addresses script parameters
    """

    model_config = ConfigDict(title="test-receive-addresses")

    channels_per_port: int = Field(
        default=1,
        title="Number of channels per port",
        description="Number of channels per port",
    )

    max_ports_per_node: int = Field(
        default=None,
        title="Maximum number of ports required per Node",
        description="Maximum number of ports required per Node",
    )

    num_nodes: int = Field(
        default=1,
        title="Number of nodes required",
        description="The total number of nodes required",
    )

    port_start: int = Field(
        default=9000,
        title="Starting Port Value",
        description="The starting value for the port range",
    )

    time_to_ready: int = Field(
        default=0,
        title="Time to script being ready",
        description="Time the script sleeps, after which the subarray transitions"
        " to READY ObsState if Configure is executed",
    )
