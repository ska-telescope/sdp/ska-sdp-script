"""Module for testing network definitions."""

import contextlib
import itertools
import json
from ipaddress import IPv4Network
from unittest.mock import call, patch

import pytest
import pytest_mock
import ska_sdp_scripting
from common import yaml_load_test_data, yaml_validate
from conftest import populate_execution_block, populate_supernet_resource
from mock_processing_block import MockProcessingBlock
from models.vis_receive_script_params import VisReceiveParams
from subnet_allocation import (
    NETWORK_ATTACH_DEF_RESOURCE_TYPE,
    NETWORK_RESOURCE_NAME,
    NETWORK_RESOURCE_TYPE,
    SUPERNET_RESOURCE_TYPE,
    SubnetAllocation,
    SubnetAllocator,
    add_custom_network_definitions_and_ips,
    resource_aware_txn,
)
from vis_receive import main, setup_vis_receive_processing_block


def _load_network_attach_def(config_db):
    for txn in resource_aware_txn(config_db.txn()):
        return next(
            txn.resource.query_values(resource_type=NETWORK_ATTACH_DEF_RESOURCE_TYPE)
        )[1]


def _load_last_allocated_subnet(config_db):
    for txn in resource_aware_txn(config_db.txn()):
        return next(txn.resource.query_values(resource_type=SUPERNET_RESOURCE_TYPE))[1][
            "last-allocated-subnet"
        ]


def _load_network_allocations(config_db):
    for txn in resource_aware_txn(config_db.txn()):
        return {
            key: str(SubnetAllocation.from_config_db(value).network)
            for key, value in txn.resource_allocation.list_values(
                resource_type=NETWORK_RESOURCE_TYPE
            )
        }


@pytest.mark.parametrize(
    "num_nodes, expected_values_files",
    (
        (1, "network_definition_script_values.yaml"),
        (2, "network_definition_script_2_pods_values.yaml"),
    ),
)
def test_setup_network_definition_receive_script(
    mocker: pytest_mock.MockerFixture,
    num_nodes,
    expected_values_files,
    default_config_cidr24,
):
    """
    Tests that the network attachment definition is being picked and that IP address
    are being determined plus assigned.
    """
    mock_create_topic_with_partitions = mocker.patch(
        "vis_receive.create_topic_with_partitions"
    )
    pb = MockProcessingBlock(
        channels_per_port=13824 // 2,
        num_nodes=num_nodes,
        config=default_config_cidr24,
        use_network_definition=True,
    )

    network_attach_def = _load_network_attach_def(default_config_cidr24)

    def _assert_network_definition_remains_unchanged():
        assert network_attach_def == _load_network_attach_def(default_config_cidr24)

    with SubnetAllocator.from_pb(pb) as subnet_allocator:
        values, _, _host_port = setup_vis_receive_processing_block(pb, subnet_allocator)
        assert yaml_validate(values) == yaml_load_test_data(expected_values_files)
        _assert_network_definition_remains_unchanged()
        allocation_data = yaml_load_test_data("allocations.yaml")
        assert next(iter(allocation_data.values())) == _load_last_allocated_subnet(
            default_config_cidr24
        )
        assert allocation_data == _load_network_allocations(default_config_cidr24)

    _assert_network_definition_remains_unchanged()
    assert not _load_network_allocations(default_config_cidr24)
    assert mock_create_topic_with_partitions.mock_calls == [
        call(topic="metrics-stats-01", partitions=1)
    ]


@pytest.mark.parametrize(
    "supernet, cidr_bits, allocated_networks",
    (
        ("10.0.0.0/8", 24, ("10.0.0.0/24", "10.0.1.0/24")),
        ("10.0.0.0/23", 24, ("10.0.0.0/24", "10.0.1.0/24")),
        ("10.0.0.0/8", 16, ("10.0.0.0/16", "10.1.0.0/16")),
        ("192.168.0.0/24", 25, ("192.168.0.0/25", "192.168.0.128/25")),
    ),
)
def test_concurrent_network_allocations(
    config, supernet, cidr_bits, allocated_networks
):
    # pylint: disable=missing-function-docstring
    populate_execution_block(config)
    populate_supernet_resource(config, supernet=supernet, cidr_bits=cidr_bits)
    assert not _load_network_allocations(config)

    def network_allocation_entry(idx):
        return (
            f"{idx}:{NETWORK_RESOURCE_TYPE}:{NETWORK_RESOURCE_NAME}",
            allocated_networks[idx],
        )

    def assert_expected_allocations(indices, last_allocated_subnet_idx):
        expected_allocations = dict(network_allocation_entry(idx) for idx in indices)
        assert expected_allocations == _load_network_allocations(config)
        assert allocated_networks[
            last_allocated_subnet_idx
        ] == _load_last_allocated_subnet(config)

    with SubnetAllocator("0", config):
        assert_expected_allocations([0], 0)
        with SubnetAllocator("1", config):
            assert_expected_allocations([0, 1], 1)
        assert_expected_allocations([0], 1)
    assert_expected_allocations([], 1)


def test_allocations_are_sequential(config):
    """
    Subnet allocations always continue sequentially (and cycle automatically),
    regardless of subnets becoming deallocated.
    """

    # there are 4 subnets here
    supernet_cidr_bits = 26
    subnet_cidr_bits = 26
    supernet = f"192.168.0.0/{supernet_cidr_bits}"
    populate_execution_block(config)
    populate_supernet_resource(config, supernet=supernet, cidr_bits=subnet_cidr_bits)

    def trigger_allocation_and_deallocation(expected_allocated_subnet):
        with SubnetAllocator("pb_id", config):
            allocations = list(_load_network_allocations(config).values())
            assert len(allocations) == 1
            assert expected_allocated_subnet == allocations[0]

    # we get different allocations each time, and cycle automatically
    all_subnets = list(
        str(network)
        for network in IPv4Network(supernet).subnets(
            prefixlen_diff=(supernet_cidr_bits - subnet_cidr_bits)
        )
    )
    for subnet in itertools.chain(all_subnets, all_subnets):
        trigger_allocation_and_deallocation(subnet)
        assert subnet == _load_last_allocated_subnet(config)


def test_no_network_definition(config):
    """No allocation can be performed when a no network is defined in the config DB"""
    with pytest.raises(RuntimeError):
        with SubnetAllocator("0", config):
            pass


def test_subnets_are_exhausted(default_config_cidr31):
    # pylint: disable=missing-function-docstring
    config = default_config_cidr31
    with contextlib.ExitStack() as stack:
        stack.enter_context(SubnetAllocator("0", config))
        stack.enter_context(SubnetAllocator("1", config))
        # at this point there are no more subnets to grab, so this should fail
        with pytest.raises(RuntimeError):
            with SubnetAllocator("2", config):
                pass


def test_allocation_has_disappeared(default_config_cidr31):
    # pylint: disable=missing-function-docstring
    config = default_config_cidr31
    pb_id = "0"
    with SubnetAllocator(pb_id, config):
        allocation_key = f"{pb_id}:{NETWORK_RESOURCE_TYPE}:{NETWORK_RESOURCE_NAME}"
        assert _load_network_allocations(config) == {allocation_key: "192.168.0.0/31"}
        # remove and check that the test runs until the end
        for txn in resource_aware_txn(config.txn()):
            txn.resource_allocation(key=allocation_key).delete()
    assert not _load_network_allocations(config)


def test_allocation_has_changed_contents(default_config_cidr31):
    # pylint: disable=missing-function-docstring
    config = default_config_cidr31
    pb_id = "0"
    rougue_subnet = "10.0.0.0/8"
    allocation_key = f"{pb_id}:{NETWORK_RESOURCE_TYPE}:{NETWORK_RESOURCE_NAME}"
    with SubnetAllocator(pb_id, config):
        assert _load_network_allocations(config) == {allocation_key: "192.168.0.0/31"}
        # remove and allocate again with a different value
        for txn in resource_aware_txn(config.txn()):
            this_resource_allocation = txn.resource_allocation(key=allocation_key)
            this_resource_allocation.delete()
            this_resource_allocation.create(
                SubnetAllocation.from_network(rougue_subnet).to_config_db()
            )
    assert _load_network_allocations(config) == {allocation_key: rougue_subnet}


def test_dummy_test(mocker: pytest_mock.MockerFixture, default_config_cidr24):
    # pylint: disable=missing-function-docstring
    # We just run main here to confirm code executes without error
    mock_create_topic_with_partitions = mocker.patch(
        "vis_receive.create_topic_with_partitions"
    )

    def populated_mock_pb():
        return MockProcessingBlock(config=default_config_cidr24)

    with patch.object(ska_sdp_scripting, "ProcessingBlock", populated_mock_pb):
        main()

    assert mock_create_topic_with_partitions.mock_calls == [
        call(topic="metrics-stats-01", partitions=1)
    ]


@pytest.mark.parametrize(
    "all_pod_settings, expected_custom_ips, expected_all_pod_settings",
    [
        (tuple(), [], tuple()),
        (
            (VisReceiveParams.PodSettings(), VisReceiveParams.PodSettings()),
            ["10.20.0.2", "10.20.0.3"],
            (
                VisReceiveParams.PodSettings(
                    extraMetadata={
                        "annotations": {
                            "k8s.v1.cni.cncf.io/networks": json.dumps(
                                [
                                    {
                                        "namespace": "kube-system",
                                        "name": "nad-test",
                                        "ips": ["10.20.0.2/24"],
                                    }
                                ]
                            )
                        }
                    }
                ),
                VisReceiveParams.PodSettings(
                    extraMetadata={
                        "annotations": {
                            "k8s.v1.cni.cncf.io/networks": json.dumps(
                                [
                                    {
                                        "namespace": "kube-system",
                                        "name": "nad-test",
                                        "ips": ["10.20.0.3/24"],
                                    }
                                ]
                            )
                        }
                    }
                ),
            ),
        ),
    ],
)
def test_add_custom_network_definitions_and_ips(
    default_config_cidr24,
    all_pod_settings,
    expected_custom_ips,
    expected_all_pod_settings,
):
    """Test add_custom_network_definitions_and_ips."""
    pb = MockProcessingBlock(
        channels_per_port=13824 // 2,
        num_nodes=2,
        config=default_config_cidr24,
        use_network_definition=True,
    )

    with SubnetAllocator.from_pb(pb) as subnet_allocator:
        custom_ips, all_pod_settings = add_custom_network_definitions_and_ips(
            all_pod_settings, subnet_allocator
        )

    assert custom_ips == expected_custom_ips
    assert all_pod_settings == expected_all_pod_settings
