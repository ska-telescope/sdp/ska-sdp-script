"""
test-slurm script
"""

import logging
import os

import ska_ser_logging
from ska_sdp_scripting import ProcessingBlock
from test_slurm_params import TestSlurmParams

ska_ser_logging.configure_logging()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

PB_ID = os.getenv("SDP_PB_ID")


def main(pb):
    """Run processing script."""
    pb.validate_parameters(model=TestSlurmParams)

    # Get the parameters from the processing block
    parameters = pb.get_parameters()
    tasks = parameters.get("tasks", 20)
    nodes = parameters.get("nodes", 2)

    # Create work phase with the (fake) buffer request.
    work_phase = pb.create_phase("Work", [])

    with work_phase:
        eb_id = work_phase._eb_id
        LOG.info("Creating slurm deployment")
        slurm_deployment = work_phase.ee_deploy_slurm(
            deploy_name="test-slurm",
            slargs={
                "script": "#!/bin/bash\n\n echo Hello Slurm;",
                "tasks": tasks,
                "nodes": nodes,
                "current_working_directory": f"/product/{eb_id}/sdp/{PB_ID}",
            },
        )

        work_phase.wait_loop(slurm_deployment.is_finished)


if __name__ == "__main__":
    with ProcessingBlock() as processing_block:
        main(processing_block)
