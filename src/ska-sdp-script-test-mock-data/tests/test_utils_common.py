"""
Unittests for utils_common.py
"""

import csv
import datetime
import os
import tempfile
import time
from pathlib import PosixPath
from threading import Thread
from unittest.mock import patch

import numpy
import pytest
from generate_mock_data.utils_common import (
    _match_antennas_get_index,
    construct_pointing_data,
    create_data_flows,
    create_result_dir,
    get_pointing_scan_ids,
)
from utils import EB_ID, PB_ID, RECEPTORS, assert_json_data


def test_get_result_dir(monkeypatch):
    """Test generating results directory"""

    with tempfile.TemporaryDirectory() as temp_dir:

        monkeypatch.setattr("generate_mock_data.utils_common.MOUNT_POINT", temp_dir)

        scan_ids = [1, 2, 3, 4, 5]

        results_dir = create_result_dir(EB_ID, PB_ID, scan_ids)

        assert results_dir == f"{temp_dir}/product/{EB_ID}/ska-sdp/{PB_ID}/scan1-5"
        assert os.path.exists(results_dir)


def test_match_antennas_get_index_correct_match(column_names):
    """Matches column for the input receptors"""
    indices = _match_antennas_get_index(
        column_names, ["MKT000", "MKT001", "MKT002", "SKA001"]
    )
    assert (
        indices
        == numpy.array(
            [[4, 5, 6, 7], [8, 9, 10, 11], [12, 13, 14, 15], [20, 21, 22, 23]]
        )
    ).all()


def test_match_antennas_get_index_correct_no_match(column_names):
    """No column matches for the input receptors"""
    with pytest.raises(ValueError):
        _match_antennas_get_index(
            column_names, ["MKT062", "MKT063", "SKA033", "SKA034"]
        )


def test_construct_pointing_data(monkeypatch):
    """Unit test for construct_pointing_data"""

    with tempfile.TemporaryDirectory() as result_dir:
        test_csv = f"{result_dir}/test.csv"

        monkeypatch.setattr("generate_mock_data.utils_common.INPUT_CSV_FILE", test_csv)

        expected = {
            "source_name": "Test_source2",
            "track_duration": 16.0,
            "band_type": "Band 2",
            "scan_mode": "5-point",
            "receptors": RECEPTORS,
            "discrete_offset": numpy.array(
                [
                    [0.0, -1.0],
                    [1.0, 0.0],
                    [0.0, 1.0],
                    [-1.0, 0.0],
                    [0.0, 0.0],
                ]
            ),
            "ref_time": 200.0,
            "pointing_frame": "xel-el",
            "source_coords": (3.0, 4.0),
            "commanded_coords": numpy.ones([3, 2]) + 2,
            "pointing_offsets": numpy.ones([3, 2]),
            "frequency": numpy.array([1.355e09]),
        }

        with open(test_csv, "w", newline="", encoding="utf-8") as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=",")
            csvwriter.writerow(["Attribute: Data"])
            csvwriter.writerow([f"track_duration: {expected['track_duration']}"])
            csvwriter.writerow([f"band_type: {expected['band_type']}"])
            csvwriter.writerow([f"scan_mode: {expected['scan_mode']}"])
            csvwriter.writerow([f"pointing_frame: {expected['pointing_frame']}"])
            csvwriter.writerow([])
            csvwriter.writerow(
                [
                    "SourceName",
                    "SourceRA",
                    "SourceDEC",
                    "ReferenceTime",
                    "SKA001_CommandedAz",
                    "SKA001_CommandedEl",
                    "SKA001_xElOffset",
                    "SKA001_ElOffset",
                    "SKA002_CommandedAz",
                    "SKA002_CommandedEl",
                    "SKA002_xElOffset",
                    "SKA002_ElOffset",
                    "SKA003_CommandedAz",
                    "SKA003_CommandedEl",
                    "SKA003_xElOffset",
                    "SKA003_ElOffset",
                ]
            )
            csvwriter.writerow(
                [
                    "Test_source1",
                    "1",
                    "0",
                    "100",
                    "59",
                    "-1",
                    "0.01",
                    "0.02",
                    "60",
                    "-2",
                    "0.03",
                    "0.04",
                    "61",
                    "-3",
                    "0.02",
                    "0.001",
                ]
            )
            csvwriter.writerow(
                [
                    expected["source_name"],
                    expected["source_coords"][0],
                    expected["source_coords"][1],
                    expected["ref_time"],
                    expected["commanded_coords"][0, 0],
                    expected["commanded_coords"][0, 1],
                    expected["pointing_offsets"][0, 0],
                    expected["pointing_offsets"][0, 1],
                    expected["commanded_coords"][1, 0],
                    expected["commanded_coords"][1, 1],
                    expected["pointing_offsets"][1, 0],
                    expected["pointing_offsets"][1, 1],
                    expected["commanded_coords"][2, 0],
                    expected["commanded_coords"][2, 1],
                    expected["pointing_offsets"][2, 0],
                    expected["pointing_offsets"][2, 1],
                ]
            )

        result1 = construct_pointing_data(1, RECEPTORS)
        result2 = construct_pointing_data(2, RECEPTORS)

        assert_json_data(result1, expected)
        assert not result2


@pytest.mark.timeout(3)
def test_get_pointing_scan_ids(default_config):
    """
    After 1 second (in a thread) we add 5 finished pointing scans
    at that point the watcher iterates and finds the finished scan 5
    and returns the scan ids.
    """

    def update_after_delay():
        time.sleep(1)
        new_eb_state = {
            "scan_id": None,
            "scan_type": None,
            "scans": [
                {"scan_id": 2, "scan_type": "pointing", "status": "FINISHED"},
                {"scan_id": 3, "scan_type": "pointing", "status": "FINISHED"},
                {"scan_id": 4, "scan_type": "pointing", "status": "ABORTED"},
                {"scan_id": 5, "scan_type": "pointing", "status": "FINISHED"},
                {"scan_id": 6, "scan_type": "pointing", "status": "FINISHED"},
                {"scan_id": 7, "scan_type": "pointing", "status": "FINISHED"},
                {"scan_id": 8, "scan_type": "pointing", "status": "FINISHED"},
            ],
            "status": "ACTIVE",
        }
        for txn in default_config.txn():
            txn.execution_block.state(EB_ID).update(new_eb_state)

    # Set up thread to update key after delay
    thread = Thread(target=update_after_delay)
    start = datetime.datetime.now()
    thread.start()

    last_idx = -1
    scan_ids, last_idx = get_pointing_scan_ids(
        default_config, EB_ID, last_index=last_idx
    )

    # Make sure to wait for thread to finish
    thread.join()
    finish = datetime.datetime.now() - start

    assert last_idx == 5
    assert scan_ids == [2, 3, 5, 6, 7]

    # the update happens 1s after start
    assert finish.total_seconds() >= 1


@patch(
    "generate_mock_data.utils_common.SDP_DATA_PVC_NAME",
    "shared",
)
@patch("generate_mock_data.utils_common.KUBE_NAMESPACE_SDP", "test_namespace")
def test_create_data_flows(mock_pb):
    """
    Unit test for create_data_flows
    """
    parameters = {}
    ants = [
        "SKA001",
        "SKA002",
        "SKA003",
    ]
    # Create 3 DataFlow flows:
    #   One a DataQueue sink relating to a Kafka topic
    #   One a DataProduct sink relating to a file in a PVC is a Kubernetes
    #   namespace
    #   One TangoAttributeMap flow to decompose a Kafka topic
    #   (which uses the Kafka flow as the flow Source)
    create_data_flows(mock_pb, parameters, ants)

    assert mock_pb._flows[0].flow.sink.topics == "pointing_offset"
    assert str(mock_pb._flows[0].flow.sink.host) == "kafka://localhost:9092"
    assert mock_pb._flows[0].flow.sink.format == "msgpack_numpy"

    assert mock_pb._flows[1].flow.sink.data_dir.pvc_mount_path == PosixPath("/mnt/data")
    assert mock_pb._flows[1].flow.sink.data_dir.k8s_pvc_name == "shared"
    assert mock_pb._flows[1].flow.sink.data_dir.k8s_namespaces == ["test_namespace"]
    assert mock_pb._flows[1].flow.sink.paths[0] == PosixPath(
        "{common_prefix}pointing_offsets.hdf5"
    )

    assert len(mock_pb._flows[2].flow.sink.attributes) == 3
    assert mock_pb._flows[2].flow.sink.attributes[0][0].kind == "tango"
    assert (
        mock_pb._flows[2].flow.sink.attributes[0][0].attribute_url.attribute_name
        == "pointing_offset_SKA001"
    )
    assert len(mock_pb._flows[2].flow.sources) == 1
    assert mock_pb._flows[2].flow.sources[0].uri.kind == "data-queue"
