"""
Resource management for SDP Configuration Database.
"""

from ska_sdp_config.operations.entity_operations import CollectiveEntityOperations


class ResourceOperations(CollectiveEntityOperations):
    """Database operations related to resource management."""

    PREFIX = "/resource"
    KEY_PARTS = {
        "resource_type": "[a-zA-Z0-9-_]+",
        "resource_name": "[a-zA-Z0-9-_]+",
    }
