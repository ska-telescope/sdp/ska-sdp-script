"""vis-receive helm chart values model."""

# pylint: disable=invalid-name

from __future__ import annotations

from input_parameters import SignalDisplay
from pydantic import Field
from pydantic.dataclasses import dataclass
from ska_sdp_scripting.processing_block import ParameterBaseModel

MSWRITER_PROCESSOR = "mswriter"
RCAL_PROCESSOR = "rcal"
TIME_MSWRITER_PROCESSOR = "averagetime-mswriter"
BUILTIN_PROCESSORS = (
    MSWRITER_PROCESSOR,
    RCAL_PROCESSOR,
    TIME_MSWRITER_PROCESSOR,
)
DEFAULT_PROCESSORS = {MSWRITER_PROCESSOR: {}}


class VisReceiveParams(ParameterBaseModel):
    """
    Processing block parameters read by vis-receive script.

    These are provided by the processing block definition by assign_resources.
    """

    channels_per_port: int = 1
    processes_per_node: int = 1
    max_ports_per_node: int | None = None
    num_nodes: int | None = None
    port_start: int = 21000
    transport_protocol: str = "udp"
    use_network_definition: bool = True

    telstate: dict[str, str] | None = None
    extra_helm_values: dict = Field(default_factory=dict)
    pod_settings: list[PodSettings] = Field(default_factory=list)
    processors: dict = Field(default=DEFAULT_PROCESSORS)
    signal_display: SignalDisplay = Field(default_factory=SignalDisplay)

    @dataclass
    class NetworkMapping:
        """Network Mapping."""

        name: str | None = None
        namespace: str | None = None
        deviceID: str | None = None
        ip: str | None = None

        def __bool__(self):
            return bool(self.name or self.namespace or self.deviceID or self.ip)

    @dataclass
    class PodSettings:
        """Pod Settings."""

        networkMapping: VisReceiveParams.NetworkMapping | None = None
        extraMetadata: dict | None = None
        nodeSelector: dict | None = None
        receiverResources: dict | None = None
        securityContext: dict | None = None


@dataclass
class BeamChannelAddresses:
    """
    Network address configuration that assigns beam channel ranges to hosts
    and ports.
    """

    host: list[tuple[int, int]]
    """
    Sequence of (start_channel, host_idx) values.

    i.e. `[(0, 0), (200, 1)]`
    """
    port: list[tuple[int, int]]
    """
    Sequence of (start_channel, port) values.

    i.e. `[(0, 9000), (200, 10000)]`
    """


TargetID = str
"""Target ID i.e. `'target:a'`"""

BeamID = str
"""Beam ID i.e. `'vis0'`"""

ObservationAddressMapping = dict[TargetID, dict[BeamID, BeamChannelAddresses]]
"""Network address mapping for target with beam combinations."""

ObservationPortCountMapping = dict[TargetID, dict[BeamID, list[int]]]
"""Network port count mapping for target with beam combinations."""
