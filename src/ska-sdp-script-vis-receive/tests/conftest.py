"""Common pytest fixtures module."""

import pytest
import ska_sdp_config
import subnet_allocation
from common import yaml_load_test_data


@pytest.fixture(name="config")
def fixture_config():
    """Load in-memory configuration database client."""
    with ska_sdp_config.Config(backend="memory") as config:
        config.backend._data.clear()
        yield config


@pytest.fixture(name="execution_block_config")
def fixture_execution_block_config():
    """Load mock execution block config."""
    return yaml_load_test_data("mock_execution_block.yaml")


_NETWORK_ATTACH_DEF = {"name": "nad-test", "namespace": "kube-system"}
_SUPERNET_TEMPLATE = {"ip-supernet": "", "ip-subnet-cidr-bits": 0}


def populate_execution_block(
    config: ska_sdp_config.Config, execution_block_config: dict | None = None
):
    """Populate the configuration database with an execution block.

    Args:
        config (ska_sdp_config.Config): configuration database client
        execution_block_config (dict | None, optional): execution block kwargs.
            Defaults to reading from mock_execution_block.yaml.
    """
    if execution_block_config is None:
        execution_block_config = yaml_load_test_data("mock_execution_block.yaml")
    for txn in config.txn():
        eb_id = execution_block_config.get("eb_id")
        execution_block_config.update({"key": eb_id})
        eblock = ska_sdp_config.entity.ExecutionBlock(**execution_block_config)
        txn.execution_block.create(eblock)


def populate_supernet_resource(
    config: ska_sdp_config.Config, supernet: str, cidr_bits: int
):
    """Populate the configuration database with an supernet resources.

    Args:
        config (ska_sdp_config.Config): configuration database client.
        supernet (str): CIDR supernet address.
        cidr_bits (int): CIDR bit length.
    """
    supernet_resource = dict(_SUPERNET_TEMPLATE)
    supernet_resource["ip-supernet"] = supernet
    supernet_resource["ip-subnet-cidr-bits"] = cidr_bits
    for txn in subnet_allocation.resource_aware_txn(config.txn()):
        txn.resource(
            resource_type=subnet_allocation.SUPERNET_RESOURCE_TYPE,
            resource_name="vis-receive-supernet",
        ).create(supernet_resource)
        txn.resource(
            resource_type=subnet_allocation.NETWORK_ATTACH_DEF_RESOURCE_TYPE,
            resource_name="nad-test",
        ).create(_NETWORK_ATTACH_DEF)


@pytest.fixture(name="default_config_cidr24")
def fixture_default_populated_config_cidr24(
    config: ska_sdp_config.Config,
) -> ska_sdp_config.Config:
    """SDP Config with default values and populated execution block."""
    populate_execution_block(config)
    populate_supernet_resource(config, supernet="10.20.0.0/16", cidr_bits=24)
    return config


@pytest.fixture(name="default_config_cidr31")
def fixture_default_populated_config_cidr31(
    config: ska_sdp_config.Config,
) -> ska_sdp_config.Config:
    """SDP Config with default values and populated execution block."""
    populate_execution_block(config)
    populate_supernet_resource(config, supernet="192.168.0.0/30", cidr_bits=31)
    return config
