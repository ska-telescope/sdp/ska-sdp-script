"""
Pydantic model for spectral-line-imaging processing script
"""

from typing import Annotated

from pydantic import BaseModel, Field
from pydantic.config import ConfigDict
from ska_sdp_scripting.processing_block import ParameterBaseModel

PIPELINE_VERSION = "0.6.1"


class DaskWorkerParams(BaseModel, extra="forbid"):
    """Parameters for the dask worker"""

    replicas: int = Field(
        default=2, title="Replicas", description="Number of replicas of dask worker"
    )

    resources: dict = Field(
        default={}, title="Resources", description="Resources available to dask worker"
    )


class DaskParams(BaseModel, extra="forbid"):
    """Parameters to be passed to the dask helm chart"""

    worker: DaskWorkerParams = Field(
        default_factory=DaskWorkerParams,
        title="worker",
        description="Dask worker parameters",
    )


class SpectralLineImagingParams(ParameterBaseModel):
    """
    spectral line imaging pipeline script parameters
    """

    model_config = ConfigDict(title="spectral-line-imaging")

    version: Annotated[
        str,
        Field(
            pattern=r"^[a-zA-Z0-9_\.-]+$",
            default=PIPELINE_VERSION,
            title="Spectral line imaging pipeline Version",
            description="Version of the spectral line imaging pipeline used "
            "by the helm chart for deployment",
        ),
    ]

    msname: Annotated[
        str,
        Field(
            pattern=r"^[a-zA-Z0-9_\.-]+$",
            title="Measurement set name",
            description="Name of the measurement set to be processed",
        ),
    ]

    image: Annotated[
        str,
        Field(
            pattern=r"^[a-zA-Z0-9_:\./-]+$",
            default="artefact.skao.int/ska-sdp-spectral-line-imaging",
            title="OCI image",
            description="The OCI image used by the helm chart to launch the "
            "spectral line imaging pipeline",
        ),
    ]

    imagePullPolicy: Annotated[
        str,
        Field(
            pattern=r"^(Always|IfNotPresent|Never)$",
            default="IfNotPresent",
            title="Pull policy of the image",
            description="Kubernetes image pull policy used by the helm "
            "chart (Always, IfNotPresent, Never)",
        ),
    ]

    dask: DaskParams = Field(
        default_factory=DaskParams,
        title="Dask Helm chart arguments",
        description="Arguments to the dask helm chart",
    )

    skymodel_image: Annotated[
        str,
        Field(
            title="Continuum model image",
            description="Name of the continuum model image/cube (FITS)",
        ),
    ]

    additional_args: list[str] = Field(
        default=[],
        title="Additional arguments",
        description="Additional arguments to be passed to the spectral line"
        " imaging pipeline",
    )
