# flake8: noqa
# pylint: disable=anomalous-backslash-in-string
"""
Collection of templates for files that need
to be generated when a new script is created.
"""

RELEASE = """release=0.0.0
tag=<SCRIPT-NAME>-0.0.0
"""

README = """<SCRIPT-NAME-CAPS> Script
=========================

description...


"""

CHANGELOG = """Changelog
---------

Dev
^^^

changes...
"""

DOCKERFILE = """FROM artefact.skao.int/ska-build-python:0.1.1 as build

WORKDIR /build

ENV POETRY_VIRTUALENVS_CREATE=false

COPY pyproject.toml poetry.lock ./

RUN poetry install

FROM artefact.skao.int/ska-python:0.1.2

WORKDIR /app

COPY --from=build /usr/local/ /usr/local/

COPY <SCRIPT-PY>.py <SCRIPT-PY>_params.py ./
ENTRYPOINT ["python", "<SCRIPT-PY>.py"]
"""

POETRY = """[tool.poetry]
name = "ska-sdp-script-<SCRIPT-NAME>"
version = "0.0.0"
description = "SKA SDP <SCRIPT-NAME-CAPS> Script"
authors = ["SKA SDP Developers"]
license = "BSD-3-Clause"
repository = "https://gitlab.com/ska-telescope/sdp/ska-sdp-script/src/ska-sdp-script-<SCRIPT-NAME>"
documentation = "https://developer.skao.int/projects/ska-sdp-script/en/latest/scripts/<SCRIPT-NAME>.html"
package-mode = false

[[tool.poetry.source]]
name = "PyPI"
priority = "supplemental"

[[tool.poetry.source]]
name = "skao"
url = "https://artefact.skao.int/repository/pypi-internal/simple"
priority = "primary"

[tool.poetry.dependencies]
python = "^3.10"
ska-ser-logging = "^0.4.3"
ska-sdp-scripting = "^0.12.0"
"""

MAKEFILE = """include ../../.make/dependencies.mk

PROJECT_NAME = ska-sdp-script-<SCRIPT-NAME>
PROJECT_PATH = ska-telescope/sdp/ska-sdp-script/src/ska-sdp-script-<SCRIPT-NAME>
ARTEFACT_TYPE = oci
"""

SKART = """[dep.ska-ser-logging]
dep_type = "gitlab:python"
project = "ska-telescope/ska-ser-logging"
release.tag = "\\\d+\\\.\\\d+\\\.\\\d+"

[dep.ska-ser-logging.sink.poetry]
sink_type = "python:poetry"
release.version_only = true

[dep.ska-sdp-scripting]
dep_type = "gitlab:python"
project = "ska-telescope/sdp/ska-sdp-scripting"
release.tag = "\\\d+\\\.\\\d+\\\.\\\d+"

[dep.ska-sdp-scripting.sink.poetry]
sink_type = "python:poetry"
release.version_only = true
"""

RST = """
.. include:: ../../../src/ska-sdp-script-<SCRIPT-NAME>/README.rst

Processing block parameters
---------------------------

.. autopydantic_settings:: <SCRIPT-PY>_params.ScriptParams

.. include:: ../../../src/ska-sdp-script-<SCRIPT-NAME>/CHANGELOG.rst
"""
