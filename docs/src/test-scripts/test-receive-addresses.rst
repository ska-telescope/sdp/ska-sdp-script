.. include:: ../../../src/ska-sdp-script-test-receive-addresses/README.rst

Processing block parameters
---------------------------

.. autopydantic_settings:: test_receive_addresses_params.TestReceiveAddressesParams

.. include:: ../../../src/ska-sdp-script-test-receive-addresses/CHANGELOG.rst
