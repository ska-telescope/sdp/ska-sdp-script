"""
Pydantic model for processing script parameters.
"""

from pydantic import Field
from pydantic.config import ConfigDict
from ska_sdp_scripting.processing_block import ParameterBaseModel


class TestSlurmParams(ParameterBaseModel):
    """
    test-slurm script parameters
    """

    model_config = ConfigDict(title="test-slurm")

    tasks: int = Field(
        default=1,
        title="Slurm tasks",
        description="Number of slurm tasks",
    )

    nodes: int = Field(
        default=1, title="Slurm nodes", description="Number of slurm nodes"
    )
