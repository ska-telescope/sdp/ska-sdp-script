"""
Create a Mock version of a ProcessingBlock for use in testing
to avoid need for a working Configuration Database
"""

import os

from mock_phase import MockPhase
from overrides import override
from ska_sdp_scripting import BufferRequest, Phase, ProcessingBlock

TEST_DIR_PATH = os.path.dirname(os.path.abspath(__file__))


class MockProcessingBlock(ProcessingBlock):
    """
    Mocked version of ska_sdp_scripting.ProcessingBlock
    overriding functions only needed in testing
    """

    # pylint: disable=super-init-not-called
    def __init__(self, config, **kwargs):
        self._eb_id = "eb-test-20210630-00000"
        self._config = config
        self._pb_id = "pb-test-20210630-00000"
        self.parameters = kwargs
        self._dependencies = []
        self._flows = []

    def get_parameters(self):
        return self.parameters

    @override
    def create_phase(self, name: str, requests: list[BufferRequest]) -> Phase:
        kind = "processing"
        return MockPhase(
            name,
            requests,
            self._config,
            self._pb_id,
            self._eb_id,
            kind,
            self.parameters.get("queue_connector_configuration"),
            self._flows,
        )

    @override
    def receive_addresses(
        self,
        configured_host_port,
        chart_name=None,
        service_name=None,
        namespace=None,
        update_dns=True,
    ):
        # pylint: disable=too-many-arguments,too-many-positional-arguments
        return

    @override
    def get_dependencies(self) -> list:
        return self._dependencies or []

    def set_dependencies(self, new_dependency: list):
        """Set the list of processing block dependencies."""
        self._dependencies = new_dependency
        return self._dependencies or []
