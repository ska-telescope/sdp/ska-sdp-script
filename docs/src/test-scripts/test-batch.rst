.. _test_batch:

.. include:: ../../../src/ska-sdp-script-test-batch/README.rst

Processing block parameters
---------------------------

.. autopydantic_settings:: test_batch_params.TestBatchParams

.. include:: ../../../src/ska-sdp-script-test-batch/CHANGELOG.rst