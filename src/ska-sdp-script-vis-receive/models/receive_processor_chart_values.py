"""vis-receive processor helm chart values models."""

# pylint: disable=invalid-name

from __future__ import annotations

from pydantic.dataclasses import dataclass

from .k8s_defs import EnvVar


@dataclass
# pylint: disable-next=too-many-instance-attributes
class ReceiveProcessorValues:
    """
    Helm values definition for receive processors.

    Based on
    ska-sdp-helmdeploy-charts/charts/ska-sdp-helmdeploy-script/templates/values.yaml
    """

    name: str
    image: str
    version: str
    imagePullPolicy: str | None = None
    resources: dict | None = None

    env: list[EnvVar] | None = None
    command: list[str] | None = None
    args: list[str] | None = None

    readinessProbe: ReceiveProcessorValues.Probe | None = None

    @dataclass
    class Probe:
        """ReceiveProcessor probe values."""

        file: str | None = None
        intialDelaySeconds: int | None = None
        periodSeconds: int | None = None
