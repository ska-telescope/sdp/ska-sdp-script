"""
Script to test real-time processing.
"""

import logging

import ska_ser_logging
from ska_sdp_scripting import ProcessingBlock
from ska_sdp_scripting.utils import ProcessingBlockStatus
from test_realtime_params import TestRealtimeParams

ska_ser_logging.configure_logging()
LOG = logging.getLogger("test_realtime")
LOG.setLevel(logging.DEBUG)


def main(pb):
    """Run processing script."""
    pb.validate_parameters(model=TestRealtimeParams)

    # Get parameters from processing block.
    parameters = pb.get_parameters()
    length = parameters.get("length", 3600.0)

    # Make buffer requests - right now this doesn't do anything, but it gives an
    # example of how resource requests will be made
    in_buffer_res = pb.request_buffer(100.0e6, tags=["sdm"])
    out_buffer_res = pb.request_buffer(length * 6e15 / 3600, tags=["visibilities"])

    # simulate different failures if requested
    failed_pb_state = None
    if parameters.get("simulate_failed_engine_app"):
        failed_pb_state = {
            "error_messages": ["Processing application error in execution engine"],
        }
    elif parameters.get("simulate_failed_engine_start"):
        failed_pb_state = {
            "status": "FAILED",
            "error_messages": ["Execution engine image pull error"],
        }

    # Create work phase
    LOG.info("Creating work phase")
    work_phase = pb.create_phase("Work", [in_buffer_res, out_buffer_res])

    with work_phase:
        LOG.info("Pretending to deploy execution engine.")

        # Signal that the script is ready to do its processing
        work_phase.update_pb_state(status=ProcessingBlockStatus.READY)

        if failed_pb_state:
            for txn in work_phase._config.txn():
                pb_state = txn.processing_block.state(pb._pb_id).get()
                pb_state.update(failed_pb_state)
                txn.processing_block.state(pb._pb_id).update(pb_state)

        LOG.info("Done, now idling...")

        work_phase.wait_loop(work_phase.is_eb_finished)


if __name__ == "__main__":
    with ProcessingBlock() as processing_block:
        main(processing_block)
