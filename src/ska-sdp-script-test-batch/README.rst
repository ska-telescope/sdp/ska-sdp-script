Test Batch Script
=================

The ``test-batch`` script is designed to test the processing controller
logic concerning processing block dependencies.

The sequence of actions carried out by the script is:

- Claims processing block
- Reads value of ``duration`` parameter (type: float, units: seconds)
  from processing block
- Sets processing block ``status`` to ``'WAITING'``
- Waits for ``resources_available`` to be ``True``

  - This is the signal from the processing controller that the script
    can start

- Sets processing block ``status`` to ``'RUNNING'``
- Does some “processing” (i.e. sleeps) for the requested duration
- Sets processing block ``status`` to ``'FINISHED'``

The script makes no deployments.

Full description of processing block parameters of this script can be
found at the `bottom of this
page <https://developer.skao.int/projects/ska-sdp-script/en/latest/test-scripts/test-batch.html#processing-block-parameters>`__.
