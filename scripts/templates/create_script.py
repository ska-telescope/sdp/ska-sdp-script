"""Create source files for real-time or batch script."""

import argparse
import logging
import os
import sys

from file_templates import (
    CHANGELOG,
    DOCKERFILE,
    MAKEFILE,
    POETRY,
    README,
    RELEASE,
    RST,
    SKART,
)

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)
LOG.addHandler(logging.StreamHandler())

FILES_TO_GENERATE = {
    ".release": RELEASE,
    "README.rst": README,
    "CHANGELOG.rst": CHANGELOG,
    "Dockerfile": DOCKERFILE,
    "pyproject.toml": POETRY,
    "Makefile": MAKEFILE,
    "skart.toml": SKART,
}


def create_source_file(script_name, template_data, output_file):
    """
    Create the source file.

    :param template_data: Template of the source file
    :param script_name: Name of the script
    :param output_file: Output file
    """
    # Replace the target string
    script_name_caps = script_name.replace("-", " ").title()
    script_py = script_name.replace("-", "_")

    if template_data == RST:
        param_class = script_name.replace("-", " ").title().replace(" ", "")
        template_data = template_data.replace("ScriptParams", f"{param_class}Params")

    template_data = (
        template_data.replace("<SCRIPT-NAME>", script_name)
        .replace("<SCRIPT-NAME-CAPS>", script_name_caps)
        .replace("<SCRIPT-PY>", script_py)
    )

    # Write the file out again
    with open(output_file, "w", encoding="utf-8") as file:
        file.write(template_data)

    LOG.info("%s file created", output_file.split("/")[-1])


def create_python_file(script, template_file, output_file):
    """
    Create python files for script.

    :param script: Name of the script
    :param template_file: Template of the source file
    :param output_file: Output file
    """
    with open(template_file, "r", encoding="utf-8") as file:
        filedata = file.read()

    module = script.replace("-", "_")
    param_class = script.replace("-", " ").title().replace(" ", "")
    params_import = f"from {module}_params import {param_class}Params"

    # update the files with correct class names where needed
    filedata = (
        filedata.replace("from script_params import ScriptParams", params_import)
        .replace("ScriptParams", f"{param_class}Params")
        .replace("<SCRIPT-NAME>", script)
    )

    # Write the file out again
    with open(output_file, "w", encoding="utf-8") as file:
        file.write(filedata)

    LOG.info("%s file created", output_file.split("/")[-1])


# pylint: disable-next=too-many-locals
def main(args):
    """
    Generate directory and files for new script
    """
    # Initialise variables
    script = args.name
    script_dir = f"ska-sdp-script-{script}"

    template_dir = os.path.dirname(__file__)
    parent_dir = os.path.dirname(template_dir)
    source_path = os.path.join(parent_dir, "..", "src", script_dir)
    tmdata_base_path = "tmdata/ska-sdp/scripts"
    docs_base_path = "docs/src/scripts"

    python_file_prefix = script.replace("-", "_")

    script_template = os.path.join(template_dir, "template_" + args.kind + ".py")
    params_template = os.path.join(template_dir, "template_params.py")
    output_script_file = os.path.join(source_path, f"{python_file_prefix}.py")
    output_params_file = os.path.join(source_path, f"{python_file_prefix}_params.py")

    LOG.info("*** Start generating source files ***")

    # Create the script directory
    try:
        os.mkdir(source_path)
        LOG.info("Directory %s created", script_dir)
    except FileExistsError:
        LOG.error("Script already exists!")
        sys.exit()

    # Create the schema directory in tmdata
    # if above worked, this should also work
    tmdata_path = os.path.join(parent_dir, "..", tmdata_base_path, script)
    os.mkdir(tmdata_path)
    LOG.info("Directory %s/%s created", tmdata_base_path, script)

    # Generate the files
    create_python_file(script, script_template, output_script_file)
    create_python_file(script, params_template, output_params_file)

    for file, template in FILES_TO_GENERATE.items():
        create_source_file(script, template, os.path.join(source_path, file))

    # Add RST to documentation
    docs_path = os.path.join(parent_dir, "..", docs_base_path, f"{script}.rst")
    create_source_file(script, RST, docs_path)
    LOG.info("%s/%s.rst created", docs_base_path, script)

    LOG.info("*** Finished generating source files ***")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Create source files for real-time or batch script."
    )
    parser.add_argument("kind", type=str, help="Kind of script (realtime or batch)")
    parser.add_argument(
        "name", type=str, help=" Name of the script to be created, e.g. test-pipeline"
    )
    main(parser.parse_args())
