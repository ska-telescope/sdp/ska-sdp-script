"""
Export JSON schema from a Pydantic model.

Only scripts in ALLOWED_SCRIPT_NAMES are accepted.
"""

import glob
import importlib
import json
import logging
import sys
from pathlib import Path

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)

FILE_DIR = Path(__file__).parent
TMDATA_BASE_DIR = f"{FILE_DIR}/../tmdata/ska-sdp/scripts"
ALLOWED_SCRIPT_NAMES = [
    "vis-receive",
    "pointing-offset",
    "spectral-line-imaging",
    "test-batch",
    "test-dask",
    "test-realtime",
    "test-mock-data",
    "test-receive-addresses",
    "test-slurm",
]


def read_json(filename):
    """
    Read json data from file.

    :param filename: Path to file that will be read
    :return: dict containing schema data
    """
    with open(filename, encoding="utf-8") as jfile:
        return json.load(jfile)


def get_pydantic_model(script_name):
    """
    This function loads the given script's directory
    as a python module and retrieves the Pydantic model class

    :param script_name: name of the processing script
    :return: pydantic model class
    """
    script_source = f"{FILE_DIR}/../src/ska-sdp-script-{script_name}"
    if script_name == "test-mock-data":
        script_source = f"{script_source}/generate_mock_data"
    sys.path.append(script_source)

    module = __import__(f"{script_name.replace('-', '_')}_params")
    importlib.reload(module)

    class_name = script_name.replace("-", " ").title().replace(" ", "")
    class_name = f"{class_name}Params"
    cls = getattr(module, class_name)

    return cls


def get_latest_json_schema(script_name):
    """Find latest JSON schema in directory"""
    schema_dir = f"{TMDATA_BASE_DIR}/{script_name}/"
    jfiles = glob.glob(f"{schema_dir}*.json")
    # by sorting, we guarantee that the largest version number ends up at the
    # end of the list. If there are no schemas present yet, return None.
    return sorted(jfiles)[-1] if jfiles else None


def get_next_schema_version_nr(name, latest_schema_filename=None):
    """
    Find latest JSON schema filename in the script directory, get the version
    number from the filename and increment it by 1.

    :param name: name of the processing script
    :return: version string
    """

    if latest_schema_filename is None:
        latest_schema_filename = get_latest_json_schema(name)

    if latest_schema_filename is None:
        latest_version = 0
    else:
        latest_version = int(Path(latest_schema_filename).stem.rsplit("-", 1)[1])

    return str(latest_version + 1)


def generate_schema(cls):
    """
    Dump the Pydantic model into a JSON schema

    :param cls: Pydantic model class for given schema
    """
    return cls.model_json_schema(mode="serialization")


def export_json_schema(cls, filename):
    """
    Export the processing block parameters to json schema
    And output it to json file.

    :param cls: Pydantic model class for given schema
    :param filename: name of file where to export the schema to
    """
    schema = generate_schema(cls)
    json_output = json.dumps(schema, indent=2)
    with open(filename, "w", encoding="UTF-8") as outfile:
        outfile.write(json_output)
    return schema


def update(name, schema_version=None):
    """
    Export JSON schema from Pydentic model

    :param name: name of the processing script
    :param schema_version: what version number to export the schema with
    """
    if name not in ALLOWED_SCRIPT_NAMES:
        raise ValueError(f"Script can be one of {ALLOWED_SCRIPT_NAMES}")

    auto_version = schema_version is None
    latest_schema = get_latest_json_schema(name)

    if latest_schema:
        latest_version = latest_schema.removesuffix(".json").split("-")[-1]
        if not auto_version and (schema_version <= latest_version):
            LOG.warning(
                "You are creating a schema with version that already exists in tmdata."
            )

    if auto_version:
        schema_version = get_next_schema_version_nr(name, latest_schema)

    cls = get_pydantic_model(name)
    filename = f"{TMDATA_BASE_DIR}/{name}/{name}-params-{schema_version}.json"
    export_json_schema(cls, filename)


def update_all():
    """
    Update all schemas and bump the version numbers by 1.
    """
    for name in ALLOWED_SCRIPT_NAMES:
        LOG.info("Updating: %s schema", name)
        update(name)


def main(name=None, version=None):
    """
    Export the parameter schema for a processing script.

    :param name: Name of the processing script
    :param version: Integer version number of the schema. If None, the default,
                    the latest available version will be incremented by 1.
    """
    if name is None:
        LOG.info("Bumping all schemas to next version.")
        update_all()

    else:
        update(name, version)


if __name__ == "__main__":
    main(*sys.argv[1:])
