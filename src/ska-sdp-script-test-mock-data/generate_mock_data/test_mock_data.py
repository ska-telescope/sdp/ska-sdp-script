"""
Script to generate mock data.
"""

import logging
import os

import ska_ser_logging
from ska_sdp_scripting import ProcessingBlock
from ska_sdp_scripting.utils import ProcessingBlockStatus
from test_mock_data_params import (
    DEFAULT_KAFKA_TOPIC,
    KAFKA_HOST,
    MOUNT_POINT,
    SDP_DATA_PVC_NAME,
    TestMockDataParams,
)
from utils_common import create_data_flows

ska_ser_logging.configure_logging()
LOG = logging.getLogger("test_mock_data")
LOG.setLevel(logging.INFO)


def get_image(pb):
    """
    Get image processing block was deployed with.
    This image is also used to deploy the engine.

    :param pb: Processing Block object
    """
    for txn in pb._config.txn():
        proc_block = txn.processing_block.get(pb._pb_id)
        script = txn.script.get(proc_block.script)
        return script.image


def get_receptors(pb):
    """
    Get name of receptors from execution block
    """
    for txn in pb._config.txn():
        eb = txn.execution_block.get(pb._eb_id)
        try:
            resources = eb.resources
            ants = resources.get("receptors")
            LOG.info("Using antennas: %s", ants)
        except (KeyError, AttributeError, TypeError):
            LOG.warning("Antennas not provided, use default 4 antennas.")
            nants = 4
            ants = [f"SKA{(i + 1):03d}" for i in range(nants)]

        return ants


def set_up_params(parameters):
    """
    Update processing block parameters in place.

    :param parameters: processing block parameters
    """
    sdp_config_host = os.getenv("SDP_CONFIG_HOST", "127.0.0.1")
    sdp_config_port = int(os.getenv("SDP_CONFIG_PORT", "2379"))

    parameters["dataProductStorage"] = {
        "name": SDP_DATA_PVC_NAME,
        "mountPath": MOUNT_POINT,
    }

    envs = parameters.get("env", [])
    default_envs = [
        {"name": name, "value": value}
        for name, value in {
            "SDP_CONFIG_HOST": sdp_config_host,
            "SDP_CONFIG_PORT": str(sdp_config_port),
        }.items()
        if value is not None
    ]
    parameters["env"] = envs + default_envs


def _pointing_scenario(parameters, eb_id, pb_id, receptors):
    """
    Add parameters related to the use-case, where
    we write an HDF output file.

    :param parameters: processing block parameters
    :param eb_id: execution block ID
    :param pb_id: processing block ID
    :param receptors: list of antenna/receptor strings
    """
    kafka_topic = parameters.get("kafka_topic", DEFAULT_KAFKA_TOPIC)
    option = parameters.get("pointing_option", "both")
    parameters["args"] = [
        "pointing_engine.py",
        eb_id,
        pb_id,
        ",".join(receptors),
        KAFKA_HOST,
        kafka_topic,
        option,
    ]

    LOG.debug("Passing args: %s", parameters["args"])


def _ms_scenario(parameters, eb_id, pb_id):
    """
    Add parameters related to the use-case, where
    we write measurement sets.

    :param parameters: processing block parameters
    :param eb_id: execution block ID
    :param pb_id: processing block ID
    """
    # The input data is a list of Measurements Sets
    msfiles = parameters.get("input_data", [])

    parameters["args"] = [
        "copy_files_engine.py",
        eb_id,
        pb_id,
        ",".join(msfiles),
    ]

    LOG.debug("Passing args: %s", parameters["args"])


def main(pb):
    """Run processing script."""
    pb.validate_parameters(model=TestMockDataParams)

    # Get parameters from processing block.
    parameters = pb.get_parameters()

    # generic parameter updates, which will be valid for any use-case
    set_up_params(parameters)
    parameters["image"] = get_image(pb)

    scenario = parameters.get("scenario", None)

    LOG.info("Starting %s scenario", scenario)
    match scenario:
        case "pointing":
            # Get receptors
            receptors = get_receptors(pb)

            pointing_option = parameters.get("pointing_option", "both")

            LOG.info("Pointing option selected: %s", pointing_option)
            if pointing_option in ["both", "send-to-kafka"]:
                LOG.info("Creating data flows.")
                create_data_flows(pb, parameters, receptors)

            # Generate HDF5 file and metadata or send to kafka or both
            _pointing_scenario(parameters, pb._eb_id, pb._pb_id, receptors)

        case "measurement-set":
            _ms_scenario(parameters, pb._eb_id, pb._pb_id)

        case _:
            LOG.warning("No scenario selected! No execution engine will be deployed.")

    # Create work phase
    LOG.info("Creating work phase")
    work_phase = pb.create_phase("Work", [])

    with work_phase:
        # Only deploy engine if valid scenario has been entered
        if scenario:
            # Deploy execution engine
            work_phase.ee_deploy_helm("mock-data", parameters)

        # Signal that the script is ready to do its processing
        work_phase.update_pb_state(status=ProcessingBlockStatus.READY)

        LOG.info("Done, now idling...")

        work_phase.wait_loop(work_phase.is_eb_finished)


if __name__ == "__main__":
    with ProcessingBlock() as processing_block:
        main(processing_block)
