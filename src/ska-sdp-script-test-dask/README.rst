Test Dask Script
================

The ``test-dask`` script is designed to test deploying two instances of
a Dask execution engine and executing a simple function on each one.

The sequence of actions carried out by the script is:

- Claims processing block
- Sets processing block ``status`` to ``'WAITING'``
- Waits for ``resources_available`` to be ``True``

  - This is the signal from the processing controller that the script
    can start

- Sets processing block ``status`` to ``'RUNNING'``
- Deploys two Dask execution engines in parallel
- Does some simple operations. Constructs a graph to add two numbers
  together and computes the result by calling the ‘compute’ method.
- Sets processing block ``status`` to ``'FINISHED'``

Full description of processing block parameters of this script can be
found at the `bottom of this
page <https://developer.skao.int/projects/ska-sdp-script/en/latest/test-scripts/test-dask.html#processing-block-parameters>`__.
