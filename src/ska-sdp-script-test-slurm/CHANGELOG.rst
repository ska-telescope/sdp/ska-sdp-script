Changelog
---------

Development
^^^^^^^^^^^

0.0.0
^^^^^

- Initial version of the test slurm script
  (`MR223 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/223>`__)
