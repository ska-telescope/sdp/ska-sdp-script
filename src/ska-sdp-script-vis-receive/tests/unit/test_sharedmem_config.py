"""Tests for vis-receive sharedmem configuration."""

import ska_sdp_config
from mock_processing_block import MockProcessingBlock
from ska_sdp_config.entity.flow import Flow
from vis_receive import (
    update_receive_processor_configurations,
    update_receiver_configuration,
)


def test_receiver_flow_config(default_config_cidr24: ska_sdp_config.Config):
    """Unit test for SPEAD receive flow configuration."""
    pb = MockProcessingBlock(config=default_config_cidr24)
    update_receiver_configuration(pb)

    phase = pb.create_phase("test", [])
    spead_flow: Flow = phase.flows[0].flow
    assert spead_flow.key == Flow.Key(
        pb_id=pb._pb_id, kind="spead", name="raw-visibility"
    )
    sharedmem_flow: Flow = phase.flows[1].flow
    assert sharedmem_flow.key == Flow.Key(
        pb_id=pb._pb_id, kind="sharedmem", name="raw-visibility"
    )
    assert sharedmem_flow.sources[0].uri == spead_flow.key


def test_processor_flow_config(default_config_cidr24: ska_sdp_config.Config):
    """Unit test for processor flow configuration."""
    pb = MockProcessingBlock(config=default_config_cidr24)
    parameters = pb.get_parameters()
    parameters["processors"] = {"mswriter": {}}

    update_receive_processor_configurations(
        pb,
        pvc_name="test-pvc",
        pvc_mount_path="/mnt/data",
    )

    phase = pb.create_phase("test", [])
    flow: Flow = phase.flows[0].flow
    assert flow.key == Flow.Key(
        pb_id=pb._pb_id, kind="data-product", name="vis-receive-mswriter-processor"
    )
    assert (
        str(flow.sink.data_dir)
        == "/mnt/data/product/eb-test-20210630-00000/ska-sdp/pb-test-20210630-00000"
    )
