Pointing offset script
======================

This script deploys the SKA `pointing offset
pipeline <https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/>`__,
which calculates pointing offsets from “pointing”-type scan data.

For a full description visit the `online
documentation <https://developer.skao.int/projects/ska-sdp-script/en/latest/scripts/pointing-offset.html>`__.
