Test Mock Data Script
=====================

The ``test-mock-data`` script is designed to provide SDP data products
in the form of data files on storage or data on Tango attributes of the
QueueConnector device, without the need to execute vis-receive or any of
the pipelines generating the data. The detailed design of the script can 
be found on 
`this Confluence page <https://confluence.skatelescope.org/display/SE/Design+of+Test+Mock+Data+Script>`__.

It executes the different scenarios by deploying various execution engines
that carry out the required processing.

It implements the following scenarios (list to grow as development
progresses):

1. ``"pointing"``: Write pointing-offset results to HDF and/or the relevant Queue Connector tango attributes
2. ``"measurement-set"``: Copy user-provided MS file(s) to output directory

A full description of the processing block parameters of this script can
be found in the :ref:`Processing block parameters <test_mock_data_params>` section.

Pointing offset
---------------

There are three available options that can be chosen for this scenario: 

-  ``"write-hdf"``: HDF files that follow a pointing data template are
   written to disk at the standard output directory. Corresponding metadata files are also added.
-  ``"send-to-kafka"``: Pointing offset results are sent to
   Kafka and the QueueConnector device is configured to
   read these data and display them on its dish-specific pointing
   attributes.
-  ``"both"``: Runs both ``write-hdf`` and ``send-to-kafka`` options.

The output pointing HDF files follow the data structure of the
`data product of the pointing offset calibration pipeline <https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/data_product.html#hdf5-main-data-product>`_.

An internal CSV file
and HDF template are used to obtain the pointing offsets. The CSV file allows
for a set of fifty different sources. The pointing engine listens for **any**
finished scans, and takes pointing offsets from the
CSV file every time a completed 5-point observation has been
observed. If more observations are completed than contained in the CSV
file, it will loop back to start at the beginning again.

Note that the configuration
information for each dish in the HDF output is identical. This is because the template
HDF file contains only one dish, and the coordinates etc. are copied for all
other dishes - i.e. all dishes are placed at the same physical
coordinates.

Based on the selected option, the script also configures data flow objects to be created
in the SDP configuration database, where a ``DataProduct`` flow is used for HDF files and
``DataQueue`` flow used to send the data via Kafka. The queue connector is also 
configured using a ``TangoAttributeMap`` flow object so that it picks up the offsets that were sent to
Kafka.

Write Measurement Sets data
---------------------------

Measurement Set (MS) files for each scan are written to disk at the
standard output directory as defined by
`ADR-55 <https://confluence.skatelescope.org/display/SWSI/ADR-55+Definition+of+metadata+for+data+management+at+AA0.5>`_.
Input MS file paths must be provided on
the shared PVC for each scan and these are copied to the output. A
corresponding metadata file is also generated.

The measurement set engine listens for **any** new scan,
and when the scan starts, copies the next MS to the standard output
directory. The output MS is renamed to match the observed scan id.

Testing
-------

The script uses the following environment variables.

.. list-table:: Environment variables used by the test-mock-data script.
   :header-rows: 1

   * - Name
     - Description
     - Default
   * - ``SDP_CONFIG_HOST``
     - Host address of the Configuration DB
     - ``127.0.0.1``
   * - ``SDP_CONFIG_PORT``
     - Port of the Configuration DB
     - ``2379``
   * - ``SDP_KAFKA_HOST``
     - Kafka server (host)
     - ``localhost:9092``
   * - ``SDP_DATA_PVC_NAME``
     - PVC name
     - ``None``
   * - ``SDP_HELM_NAMESPACE``
     - K8s namespace used for data product flow
     - ``None``
   * - ``WATCHER_TIMEOUT``
     - Timeout used when waiting for scans
     - 60 s


:external+ska-sdp-integration:doc:`Deploy SDP <installation/standalone>`
and make sure the :external+ska-sdp-integration:doc:`iTango <operation/itango>`
console pod is also running.

After entering the iTango pod, obtain a handle to a subarray device and
turn it on:

.. code-block:: python

   d = DeviceProxy('test-sdp/subarray/01')
   d.On()

If you are not sure what devices are available, list them with
``lsdev``.

The customised processing block parameters that can be included in the
configuration string can be found in the 
:ref:`Processing block parameters <test_mock_data_params>`
section and summarised in the following table.

.. list-table:: Processing block parameters
   :header-rows: 1

   * - Name
     - Description
     - Default
   * - ``scenario``
     - Name of scenario to run
     - ``None``
   * - ``input_data``
     - List containing path(s) of MS to copy
     - ``[]``
   * - ``kafka_topic``
     - Kafka topic name
     - ``pointing_offset``
   * - ``pointing_option``
     - Flag to choose optional behaviour for the pointing scenario
     - ``both``


``scenario`` needs to be set to ``pointing`` or ``measurement-set`` for the different scenarios
to be run. If no scenario is specified, the script sets up the EB and PB correctly but does not deploy
any execution engine.

``input_data`` is a list where each element is the path to the
Measurement Set to be written for the ``measurement-set`` scenario.
These paths must be located on a PVC accessible to the test-mock-data engine.
Note that the path should start at the root of the storage -
i.e. it should not include the mount point. The input data can
contain any number of Measurement Sets.

``pointing_option`` sets the options for the ``pointing`` scenario. It must be set to
``write-hdf``, ``send-to-kafka`` or ``both`` to determine the
operations performed by the scenario.

An example for the measurement set scenario with 5 scans is:

.. code-block:: python

         "parameters": {
           "scenario": "measurement-set",  
           "input_data": ["product/eb-orcatest-20240814-94773/ska-sdp/pb-orcatestvr-20240814-94773/output.scan-1.ms", 
                          "product/eb-orcatest-20240814-94773/ska-sdp/pb-orcatestvr-20240814-94773/output.scan-2.ms",
                          "product/eb-orcatest-20240814-94773/ska-sdp/pb-orcatestvr-20240814-94773/output.scan-3.ms",
                          "product/eb-orcatest-20240814-94773/ska-sdp/pb-orcatestvr-20240814-94773/output.scan-4.ms",
                          "product/eb-orcatest-20240814-94773/ska-sdp/pb-orcatestvr-20240814-94773/output.scan-5.ms"]
         }

Start the execution block with the ``AssignResources`` command:

.. code-block:: python

   d.AssignResources(config)

``config`` is a full AssignResources configuration string for SDP.
See `telescope model example <https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/sdp/ska-sdp-assignres.html#sdp-assign-resources-1-0>`_.

The dishes that will appear in the output data for the ``pointing`` scenario are set from
the ``receptors`` config parameter in ``execution_block.resources`` section of the
configuration string.

The script will request the start of the engine pod called
``mock-data``. This pod watches for the scans that are commanded on the
subarray. The ``measurement-set`` scenario writes the relevant
measurement set files when each scan is started, and the ``pointing``
scenario waits for the end of the 5th scan in the pointing observation
before writing files.

If the offsets were sent to the Queue Connector, you can then access the
data in itango3 by running the following code (replace the dish ID as
required):

.. code-block:: python

   q = DeviceProxy("test-sdp/queueconnector/01")
   q.pointing_offset_SKA001

To remove the deployment, you need to release all the resources and
return the subarray to an ``EMPTY`` state (in itango):

.. code-block:: python

   d.end()
   d.releaseAllResources()
