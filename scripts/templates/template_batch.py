"""
<SCRIPT-NAME> script
"""

import logging
import time

import ska_ser_logging
from script_params import ScriptParams
from ska_sdp_scripting import ProcessingBlock

ska_ser_logging.configure_logging()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)


def main(pb):
    """Run processing script."""
    pb.validate_parameters(model=ScriptParams)

    # Get the parameters from the processing block
    parameters = pb.get_parameters()
    duration = parameters.get("duration", 60.0)

    # Create work phase with the (fake) buffer request.
    work_phase = pb.create_phase("Work", [])

    # Define the function to be executed by the execution engine. In a real
    # pipeline this would be defined elsewhere and imported here.

    def some_processing(dur):
        """Do some processing for the required duration"""
        time.sleep(dur)

    with work_phase:
        deploy = work_phase.ee_deploy_test(
            "<SCRIPT-NAME>", func=some_processing, f_args=(duration,)
        )

        work_phase.wait_loop(deploy.is_finished)


if __name__ == "__main__":
    with ProcessingBlock() as processing_block:
        main(processing_block)
