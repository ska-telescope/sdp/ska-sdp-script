Changelog
=========

Development
-----------

- Update Dockerfile to use SKA python base images

0.2.0
-----

- Remove dependency on config yaml, configuration is done through
  ``--set`` passed in ``additional_args`` instead
- Add parameter ``skymodel_image`` for passing sky model fits.

0.1.0
-----

- Initial version of spectral-line-imaging processing script
