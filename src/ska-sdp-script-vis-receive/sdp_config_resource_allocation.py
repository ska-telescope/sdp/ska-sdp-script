"""
Resource allocation management for SDP Configuration Database.
"""

from ska_sdp_config.operations.entity_operations import CollectiveEntityOperations


class ResourceAllocationOperations(CollectiveEntityOperations):
    """Database operations related to resource allocation management."""

    PREFIX = "/allocation"
    KEY_PARTS = {
        "pb_id": "[a-zA-Z0-9-_]+",
        "resource_type": "[a-zA-Z0-9-_]+",
        "resource_name": "[a-zA-Z0-9-_]+",
    }
