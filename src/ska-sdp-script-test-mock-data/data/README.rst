pointing_offset.csv
===================

Reference pointing data that can be used to generate pointing offset
data products.

Information
-----------

Generate CSV file for testing global pointing model fitting (`ORC-2163 <https://jira.skatelescope.org/browse/ORC-2163>`__)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Number of observed sources: 50 Telescope: SKA1-MID with 197 dishes

Dish names
^^^^^^^^^^

The names of the dishes were extracted from
`ska-telmodel-data <https://gitlab.com/ska-telescope/ska-telmodel-data>`__
using
`ska-sdp-datamodels <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/configuration/config_create.py?ref_type=heads#L327>`__
consistent with
`ADR-32 <https://confluence.skatelescope.org/display/SWSI/ADR-32+Format+for+the+Dish+Identifiers>`__.
This ensures the MeerKAT and SKA dishes are assigned the most recent
names and positions.

What calibrators were used
^^^^^^^^^^^^^^^^^^^^^^^^^^

The gain calibrators at
`L <https://github.com/ska-sa/katsdpscripts/blob/master/RTS/sources_pnt_L.csv>`__
and
`Ku <https://github.com/ska-sa/katsdpscripts/blob/master/RTS/sources_pnt_Ku.csv>`__-
bands were combined and the top 50 sources (name and position) were
extracted. This approach provides realistic directions on the sky for
which the commanded Azimuth and Elevation angles can be extracted at a
reference time with ``katpoint``, ``AstroPy``, or ``PyEphem``. Since the
source positions are what matter most, calibrators at different bands
(say S-band) could be used as well. The reference times are the middle
timestamps of the on-source scan (i.e. the scan at the centre of the
observation) for each reference pointing observation.

How were the Pointing Offsets derived
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For a global pointing model fit, pointing offsets in cross-elevation and
elevation for each dish per reference pointing observation are required.
For this work, we would therefore need 2 values (cross-elevation and
elevation offset) for each of the 197 dishes for the 50 sources. To
generate realistic values, we run the pointing pipeline with different
command line options on a real reference pointing observation with the
MeerKAT array (with 61 dishes). These values were then scaled by varying
factors to depict the fitted offsets per dish per source. In instances
were invalid fits were obtained, NaNs were stored as the pointing
offsets. For more information on what sets of criteria need to be met
for a fit to be declared valid, refer to the pointing offset pipeline
`documentation <https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/pointing_offsets/fitting_qa.html>`__.

template.hdf5
=============

Template HDF5 file used to generate pointing offset data products
