Changelog
---------

1.0.0
^^^^^
- Update dependencies, documentation and bug fixes
  (`MR246 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/246>`__)
- Remove code to configure queue connector before v5
  (`MR240 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/240>`__)
- Add function to create data flows in pointing scenario
  (`MR238 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/238>`__)
- Combine "kafka" and "hdf" scenarios into a single "pointing" scenario
  (`MR235 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/235>`__) 
- Remove using specific scan IDs 
  (`MR232 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/232>`__) 
- Remove option that allows users to provide input data for the 
  pointing-offset-hdf and pointing-offset-queue-connector scenarios 
  (`MR232 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/232>`__) 
- Update Dockerfile to use SKA Python base image
  (`MR211 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/211>`__)
- Update pointing-offset-queue-connector to wait until scans are
  run/completed
  (`MR206 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/206>`__)
- Update behaviour when no input_data is specified to use internal data
  (`MR206 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/206>`__)

0.1.0
^^^^^

[!WARNING] This version only works with SDP 0.24.0 but if the
QueueConnector device is set to be version 4.1.0

- Add the pointing data CSV file to the docker image
  (`MR202 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/202>`__)
- Update ska-sdp-scripting to 0.12.0
  (`MR202 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/202>`__)
- Add functionality to check which antennas to simulate and update all
  relevant parameters in the template HDF5 to match number of antennas
  (`MR195 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/195>`__)
- Update file writing to occur when scans are run/completed
  (`MR196 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/196>`__)
- Add functionality to the mock-test-data processing script to generate
  data files
  (`MR187 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/187>`__)
- Processing script reports internal errors in pb state
  (`MR185 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/185>`__)
- Pydantic model included in documentation
  (`MR189 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/189>`__)
- JSON parameter schema added to tmdata
  (`MR186 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/186>`__)
- Validate processing block parameters using scripting library 0.10.0
  (`MR180 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/180>`__)
- Added processing block parameter JSON schema and Pydantic model
  (`MR180 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/180>`__)
- Update script to write basic metadata yaml file
  (`MR181 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/181>`__)
- Update script to execute required functions to produce data in an
  execution engine
  (`MR179 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/179>`__)
- Initial version of the script, which sends mock pointing offset data
  to Kafka and configures the QueueConnector to display this data in
  tango attributes.
  (`MR175 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/175>`__)
