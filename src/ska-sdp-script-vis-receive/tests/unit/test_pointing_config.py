"""Tests for vis-receive pointing configuration."""

from mock_processing_block import MockProcessingBlock
from pydantic_core import Url
from ska_sdp_config.entity.flow import DataQueue, Flow, FlowSource
from vis_receive import update_pointing_data_queue_configuration


def test_pointing_data_queue_config(default_config_cidr24):
    """
    Unit test for update_pointing_data_queue_configuration.
    """

    def _assert_dict_value(result, expected):
        """
        Assert values in two dicts are the same
        """
        for key, value in expected.items():
            if isinstance(value, dict):
                _assert_dict_value(result[key], value)
            else:
                assert result[key] == value

    pb = MockProcessingBlock(config=default_config_cidr24)

    parameters = pb.get_parameters()

    # Add telstate information, and update accordingly
    parameters["telstate"] = {
        "target_fqdn": "tango://ska_mid/tm_leaf_node/{dish_id}/target",
        "source_offset_fqdn": "tango://ska_mid/tm_leaf_node/{dish_id}/source_offset",
        "direction_fqdn": "tango://{dish_id}/elt/master/direction",
    }

    update_pointing_data_queue_configuration(pb)

    # Check flows
    phase = pb.create_phase("test", [])
    assert len(phase.flows) == 3
    for flow in phase.flows:
        assert len(flow.flow.sources) == 4
    last_flow = phase.flows[-1]

    assert last_flow.flow == Flow(
        key=Flow.Key(
            pb_id="pb-test-20210630-00000", name="vis-receive-actual-pointings"
        ),
        sink=DataQueue(topics="actual-pointings", host="localhost:9092", format="npy"),
        sources=[
            FlowSource(
                uri=Url("tango://SKA001/elt/master/direction"),
                function="ska-sdp-lmc-queue-connector:exchange",
                parameters=None,
            ),
            FlowSource(
                uri=Url("tango://SKA002/elt/master/direction"),
                function="ska-sdp-lmc-queue-connector:exchange",
                parameters=None,
            ),
            FlowSource(
                uri=Url("tango://SKA003/elt/master/direction"),
                function="ska-sdp-lmc-queue-connector:exchange",
                parameters=None,
            ),
            FlowSource(
                uri=Url("tango://SKA004/elt/master/direction"),
                function="ska-sdp-lmc-queue-connector:exchange",
                parameters=None,
            ),
        ],
        data_model="PointingTable",
    )


def test_pointing_data_queue_noupdate(default_config_cidr24):
    """
    Unit test for update_queue_connector_configuration
    In case no updates have been done
    """
    pb = MockProcessingBlock(config=default_config_cidr24)

    update_pointing_data_queue_configuration(pb)

    phase = pb.create_phase("test", [])
    with phase:
        assert len(phase.flows) == 0
