Test Slurm Script
=================

The ``test-slurm`` script is set up to deploy a single Slurm execution
engine and then give it a job to complete.

The script proceeds as follows:

- Claims processing block
- Sets values for ``tasks`` and ``nodes`` parameters (type: integer)
  from processing block
- Sets processing block ``status`` to ``'WAITING'``
- Waits for ``resources_available`` to be ``True``

  - This is the signal from the processing controller that the script
    can start

- Sets processing block ``status`` to ``'RUNNING'``
- Deploys a single slurm execution engine with the parameters ``script``
  (in this case, a simple slurm script which prints ‘Hello Slurm’),
  ``tasks``, ``nodes`` and ``current_working_directory``
- Waits for the processing block ``status`` to be ``'FINISHED'``
