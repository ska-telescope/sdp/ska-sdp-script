"""
This script adds the list of available processing scripts to the documentation in table
form. The list of available scripts is read from the file
`tmdata/ska-sdp/scripts/scripts.yaml` and written
into two text files in simple rst format for real-time and batch scripts.
"""

from pathlib import Path
from collections import defaultdict

import yaml


# ---------------------------------------------------------------------------- #
# Paths
DOCS_SRC_PATH = Path(__file__).parent.parent
SCRIPTS_PATH = 'tmdata/ska-sdp/scripts'
SCHEMAS_PATH = (DOCS_SRC_PATH / "../.." / SCRIPTS_PATH).absolute()
REPO_URL = "https://gitlab.com/ska-telescope/sdp/ska-sdp-script/"
LINK_TEMPLATE = f"`{{}} <{REPO_URL}-/blob/master/{SCRIPTS_PATH}/{{}}>`__"

# columns
FIELDS = ("name", "image", "sdp_version", "schema")


def read_yaml(filename):
    """Read data from a yaml file."""
    with Path(filename).open("r", encoding="utf-8") as file:
        return yaml.safe_load(file)  # Loader=yaml.FullLoader


def read_scripts_yaml(filename):
    """
    Read content of ``scripts.yaml``. Return a dict keyed on the "kind" of
    script with values that are lists of dicts containing metadata info for
    each script of that kind.
    """
    tables = defaultdict(list)
    for info in read_yaml(filename)["scripts"]:
        tables[info["kind"]].append(info)
    return tables


def _gen_simple_rst_table_lines(data):
    """Generate table lines for simple rst table."""

    # compute column widths. create format strings for data lines and borders
    widths = list(map(max, zip(*(map(len, line) for line in data))))
    fmt_cell = "{{: <{}}}"
    fmt_data_line = " ".join(map(fmt_cell.format, widths))
    fmt_hline = " ".join(map(fmt_cell.replace(" ", "=").format, widths))

    # top border and column headers
    yield (hline := fmt_hline.format(*([""] * len(FIELDS))))
    yield fmt_data_line.format(*FIELDS)
    yield hline

    # data lines: sort newest versions first
    for cells in sorted(data, key=_sort_row):
        yield fmt_data_line.format(*cells)
    # bottom border
    yield hline


def _sort_row(cells: list):
    # Sorting function for the rows of the table. Sorting is done by the
    # following rules:
    #   Test scripts are listed before regular non-test scripts.
    #   Scripts are listed alphabetically within (test / non-test) groups.
    #   Scripts versions are listed in descending order for each script.
    name, image, *_ = cells
    major, minor, patch, *_ = parse_image_version(image)
    return not name.startswith('test'), name, (-major, -minor, -patch)


def parse_image_version(image_name: str):
    """
    Parse image version as a tuple from image name string.

    This function parses the image version from the image name and returns it as
    a tuple. The first three items in the tuple are the major, minor, and patch
    version numbers as integers. If this is a development version, there will be
    a final item in the tuple, which is a string containing the short hash of
    the specific commit.
    """
    major, minor, patch = image_name.split(':', 1)[1].split('.', 2)
    patch, *dev = patch.split('-')
    return (int(major), int(minor), int(patch), *dev)


def write_simple_rst_table(
    data: list[tuple],
    filename: str | None = None,
    title: str = "",
    indent: int = 2,
):
    """
    Write data to text file in simple rst format.

    :param data: List of rows.
    :param filename: Path to file.
    :param title: Optional title for table.
    :param indent: Prefered indentation level, 2 by default.
    """

    table = f"\n{'': >{indent}}".join(
        (f".. table:: {title}\n", *_gen_simple_rst_table_lines(data))
    )
    if filename is not None:
        Path(filename).write_text(table, encoding="utf-8")

    return table


def write_table(table_data: list[dict], filename: str | Path, title: str = ""):
    """
    Write script metadata to table in rst format.

    :param table_data: list of dict containing relevant data for available
        scripts.
    :param filename: Path to file.
    :param title: Optional title for table.
    """

    # convert the schema info to a link
    for info in table_data:
        if schema := info.get("schema", ""):
            schema = LINK_TEMPLATE.format(schema, schema)
        info["schema"] = schema

    # get block data
    data = [list(map(entry.get, FIELDS)) for entry in table_data]
    return write_simple_rst_table(data, filename, title)


def main():
    """
    Create list of available scripts by parsing
    `mdata/ska-sdp/scripts/scripts.yaml` and writing the info therein into two
    separate text files, one for real-time processing scripts, and one for batch
    processing scripts.
    """
    #
    titles = {
        "realtime": "Real-time processing scripts",
        "batch": "Batch processing scripts",
    }
    filename = SCHEMAS_PATH / "scripts.yaml"
    for kind, table_data in read_scripts_yaml(filename).items():
        out = DOCS_SRC_PATH / f"scripts-table-{kind}"
        write_table(table_data, out, titles[kind])


if __name__ == "__main__":
    main()
