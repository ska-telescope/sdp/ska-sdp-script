"""
Common fixtures for testing
"""

import numpy
import pytest
from astropy.coordinates import EarthLocation, SkyCoord
from ska_sdp_config import Config, ProcessingBlock, entity
from ska_sdp_config.entity import Script
from ska_sdp_datamodels.calibration import PointingTable
from ska_sdp_datamodels.configuration.config_create import Configuration
from ska_sdp_datamodels.science_data_model import ReceptorFrame
from ska_sdp_scripting import ProcessingBlock as scripting_pb
from utils import EB_ID, PB_ID, RECEPTORS, TEST_SCRIPT

PREFIX = "/__test_mock_data"


@pytest.fixture(name="config")
def config_fxt():
    """Config db object"""

    with Config(global_prefix=PREFIX, backend="etcd3") as config:
        config.backend.delete(PREFIX, must_exist=False, recursive=True)
        yield config
        config.backend.delete(PREFIX, must_exist=False, recursive=True)


@pytest.fixture(name="default_config")
def default_config_fxt(config):
    """
    Default config using simple execution block string
    and empty config dictionary string
    """

    execution_block = {
        "key": EB_ID,
        "subarray_id": "01",
        "resources": {
            "receptors": RECEPTORS,
            "receive_nodes": 1,
        },
    }
    eb_state = {
        "scan_type": None,
        "scan_id": None,
        "scans": [],
        "status": "ACTIVE",
    }
    script_key = Script.Key(
        kind="realtime",
        name=TEST_SCRIPT["name"],
        version=TEST_SCRIPT["version"],
    )
    processing_block = ProcessingBlock(key=PB_ID, eb_id=EB_ID, script=script_key)
    script = Script(key=script_key, image=TEST_SCRIPT["image"])

    for txn in config.txn():
        eblock = entity.ExecutionBlock(**execution_block)
        txn.execution_block.create(eblock)
        txn.execution_block.state(EB_ID).create(eb_state)
        txn.processing_block.create(processing_block)
        txn.script.create(script)

    return config


@pytest.fixture(name="mock_pb")
def mock_pb_fixt(default_config):
    """Mock PB fixture"""
    return MockProcessingBlock(config=default_config)


class MockProcessingBlock(scripting_pb):
    """
    Mocked version of ska_sdp_scripting.ProcessingBlock
    overriding functions only needed in testing
    """

    # pylint: disable-next=super-init-not-called
    def __init__(self, config):
        self._eb_id = EB_ID
        self._config = config
        self._pb_id = PB_ID
        self.queue = []
        self._flows = []
        self.product = []


@pytest.fixture(name="column_names")
def csv_column_names():
    """CSV (for global pointing model fitting) column names fixture"""
    return [
        [
            "SourceName",
            "SourceRA",
            "SourceDEC",
            "ReferenceTime",
            "MKT000_CommandedAz",
            "MKT000_CommandedEl",
            "MKT000_xElOffset",
            "MKT000_ElOffset",
            "MKT001_CommandedAz",
            "MKT001_CommandedEl",
            "MKT001_xElOffset",
            "MKT001_ElOffset",
            "MKT002_CommandedAz",
            "MKT002_CommandedEl",
            "MKT002_xElOffset",
            "MKT002_ElOffset",
            "MKT003_CommandedAz",
            "MKT003_CommandedEl",
            "MKT003_xElOffset",
            "MKT003_ElOffset",
            "SKA001_CommandedAz",
            "SKA001_CommandedEl",
            "SKA001_xElOffset",
            "SKA001_ElOffset",
            "SKA002_CommandedAz",
            "SKA002_CommandedEl",
            "SKA002_xElOffset",
            "SKA002_ElOffset",
            "SKA003_CommandedAz",
            "SKA003_CommandedEl",
            "SKA003_xElOffset",
            "SKA003_ElOffset",
            "SKA004_CommandedAz",
            "SKA004_CommandedEl",
            "SKA004_xElOffset",
            "SKA004_ElOffset",
        ]
    ]


@pytest.fixture(name="template_pointingtable")
def template_pointingtable():
    """Fixture for the template PointingTable"""

    return PointingTable.constructor(
        pointing=numpy.array([[[[[-9.622378184959146e-05, 1.5363332557249976e-05]]]]]),
        time=numpy.array([5179445072.497151]),
        weight=numpy.array([[[[[260536994.65962976, 326008096.73621637]]]]]),
        frequency=numpy.array([1336858119.419643]),
        receptor_frame=ReceptorFrame("stokesI"),
        band_type=None,
        scan_mode=None,
        pointingcentre=SkyCoord(294.854, -63.7127, unit="deg"),
        configuration=Configuration.constructor(
            name="",
            location=EarthLocation(
                5109271.497354163, 2006808.8930278125, -3239130.7361407224, "m"
            ),
            names=numpy.array(["m000"]),
            xyz=numpy.array([[5109271.49735416, 2006808.89302781, -3239130.73614072]]),
            mount=numpy.array(["ALT-AZ"]),
            frame="",
            receptor_frame=ReceptorFrame("linear"),
            diameter=numpy.array([13.5]),
            offset=numpy.array([[0.0, 0.0, 0.0]]),
            stations=numpy.array(["0"]),
            vp_type=numpy.array([""]),
        ),
    )
