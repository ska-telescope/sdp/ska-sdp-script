"""
Primarily MockPhase class module for SDP script, vis_receive
at this stage, testing.
"""

from typing import Callable

from ska_sdp_config import Config
from ska_sdp_config.config import Transaction
from ska_sdp_scripting import Phase
from ska_sdp_scripting.data_flow import DataFlow
from ska_sdp_scripting.utils import ProcessingBlockStatus


class MockPhase(Phase):
    """Mock script phase."""

    # pylint: disable=too-many-positional-arguments,too-many-instance-attributes
    # pylint: disable=super-init-not-called,too-many-arguments
    def __init__(
        self,
        name: str,
        list_requests: list,
        config: Config,
        pb_id: str,
        eb_id: str,
        script_kind: str,
        qc_config: str,
        flow_data: list[DataFlow],
    ):
        self.name = name
        self.list_requests = list_requests
        self.config = config
        self.pb_id = pb_id
        self.eb_id = eb_id
        self.script_kind = script_kind
        self.qc_config = qc_config
        self.flows = flow_data

    def __enter__(self):
        return self

    def ee_deploy_helm(
        self,
        deploy_name: str,
        values: dict | None = None,
        version: str = "1.0.0",
    ):
        return None

    def is_eb_finished(self, txn: Transaction) -> bool:
        return True

    def update_pb_state(
        self, status: ProcessingBlockStatus = ProcessingBlockStatus.UNSET
    ):
        return None

    def wait_loop(self, func: Callable[[Transaction], bool], time_to_ready: int = 0):
        return None

    def __exit__(self, exc_type, exc_val, exc_tb):
        return None
