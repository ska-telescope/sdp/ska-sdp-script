"""
Variables and util functions for testing
"""

import os

import numpy
from astropy.coordinates import EarthLocation, SkyCoord
from generate_mock_data.pointing_engine import POINTING_SCANS
from ska_sdp_datamodels.configuration.config_create import Configuration
from ska_sdp_datamodels.science_data_model import ReceptorFrame

RECEPTORS = ["SKA001", "SKA002", "SKA003"]
PB_ID = "pb-test-20210630-00000"
EB_ID = "eb-test-20210630-00000"
SCRIPT_IMAGE = "test-docker-image"

KAFKA_HOST = os.getenv("SDP_KAFKA_HOST", "localhost:9092")

TEST_SCRIPT = {
    "image": SCRIPT_IMAGE,
    "name": "my-test",
    "version": "100.0.0",
}

EXPECTED_METADATA = {
    "config": {
        "cmdline": None,
        "commit": None,
        "image": SCRIPT_IMAGE,
        "processing_block": PB_ID,
        "processing_script": "my-test",
        "version": "100.0.0",
    },
    "context": {},
    "interface": "http://schema.skao.int/ska-data-product-meta/0.1",
    "execution_block": EB_ID,
    "obscore": {
        "facility_name": "SKA-Observatory",
    },
    "files": [],
}
N_ANT = len(RECEPTORS)
SOURCE_DATA = {
    "source_name": "test_source",
    "track_duration": 70.0,
    "band_type": "Band 2",
    "scan_mode": f"{POINTING_SCANS}-point",
    "receptors": RECEPTORS,
    "discrete_offset": numpy.array(
        [
            [0.0, -1.0],
            [1.0, 0.0],
            [0.0, 1.0],
            [-1.0, 0.0],
            [0.0, 0.0],
        ]
    ),
    "ref_time": numpy.array([370.0]),
    "pointing_frame": "xel-el",
    "source_coords": (0.0, 0.0),
    "pointing_offsets": numpy.zeros((1, N_ANT, 1, 1, 2)),
    "commanded_coords": numpy.zeros((1, N_ANT, 1, 1, 2)),
    "frequency": numpy.array([1.355e09]),
}


class MockData:  # pylint: disable=too-few-public-methods
    """Mock class for pointing table data"""

    def __init__(self, data):
        self.data = data


class MockReceptor:  # pylint: disable=too-few-public-methods
    """Mock receptor class"""

    def __init__(self):
        self.type = "stokesI"
        self.nrec = 1


# pylint: disable=too-few-public-methods,too-many-instance-attributes
class MockPointingTable:
    """Mock pointing table"""

    def __init__(self, n_ant):
        self.scan_mode = "5-point"
        self.attrs = {
            "track_duration": 60.0,
            "discrete_offset": numpy.array(
                [
                    [0.0, -1.0],
                    [1.0, 0.0],
                    [0.0, 1.0],
                    [-1.0, 0.0],
                    [0.0, 0.0],
                ]
            ),
            "pointingcentre": SkyCoord(294.8542, -63.7126, unit="deg"),
            "receptor_frame": ReceptorFrame("stokesI"),
            "configuration": Configuration.constructor(
                name="",
                location=EarthLocation(
                    5109271.497354163, 2006808.8930278125, -3239130.7361407224, "m"
                ),
                names=numpy.array([f"SKA00{i + 1}" for i in range(n_ant)]),
                xyz=numpy.array(
                    [[5109271.49735416, 2006808.89302781, -3239130.73614072]] * n_ant
                ),
                mount=numpy.array(["ALT-AZ"] * n_ant),
                frame="",
                receptor_frame=ReceptorFrame("linear"),
                diameter=numpy.array([13.5] * n_ant),
                offset=numpy.array([[0.0, 0.0, 0.0]] * n_ant),
                stations=numpy.array([f"SKA00{i + 1}" for i in range(n_ant)]),
                vp_type=numpy.array([""] * n_ant),
            ),
        }
        self.time = MockData(numpy.array([270.0]))
        self.pointing = MockData(numpy.ones([1, n_ant, 1, 1, 2]))
        self.weight = MockData(numpy.ones([1, n_ant, 1, 1, 2]))
        self.expected_width = MockData(numpy.ones([1, n_ant, 1, 1, 2]))
        self.fitted_width = MockData(numpy.ones([1, n_ant, 1, 1, 2]))
        self.fitted_width_std = MockData(numpy.ones([1, n_ant, 1, 1, 2]))
        self.fitted_height = MockData(numpy.ones([1, n_ant, 1, 1]))
        self.fitted_height_std = MockData(numpy.ones([1, n_ant, 1, 1]))
        self.commanded_pointing = numpy.array([1, n_ant, 1, 1, 2])
        self.frequency = MockData(numpy.array([1350e6]))
        self.coords = {"antenna": numpy.arange(n_ant)}
        self.receptor_frame = MockReceptor()


# Helper functions
def assert_json_data(expected_result, result):
    """Assert keys and values of data loaded from json."""
    for key, value in expected_result.items():
        if isinstance(value, dict):
            for sub_key, sub_value in value.items():
                assert (
                    result[key][sub_key] == sub_value
                ), f"result: {result[key][sub_key]}, expected: {sub_value}"
        elif isinstance(value, numpy.ndarray):
            assert (result[key] == value).all()
        else:
            assert result[key] == value, f"result: {result[key]}, expected: {value}"
