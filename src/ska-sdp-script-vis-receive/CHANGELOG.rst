Changelog
=========

Development
-----------

5.0.0
-----

- Bumped ``ska-sdp-scripting`` to ``^1.0.0``
  to support SDP v1.
- Bumped default image versions for receiver, processors, integration and rcal
  to ``6.0.0``, ``3.0.0``, ``1.0.0`` and ``3.0.0`` respectively,
  all of which support SDP v1.
- Added pod settings custom IP reading unit tests.
- Added writing ``SharedMem`` flow configuration for receiver.
- Added writing ``DataProduct`` flow configuration for mswriter processor.
- Changed vis-receive to validate script inputs and chart value outputs using pydantic models.
- Sub-networks assigned to vis-receive deployments are now allocated
  starting from the sub-network that comes after the last allocated one.
  This reduces the chances of a ``vis-receive`` deployment
  that takes a long time to terminate (or gets stuck in error)
  from clashing with newly-minted deployments.
- Removed custom SDP Config DB operations code,
  replaced with operations classes from ``ska-sdp-config``.
- Update Dockerfile to use SKA python base image.

4.5.0
-----

- Signal Metrics will now create the topics if needed, and update the amount of
  partitions as needed.
- Signal Metrics adds new parameters:
  + ``window_count``: The amount of extra windows to allow.
  + ``rounding_sensitivity``: The precision to round to.
- Upgrade Signal Metrics Generator to latest version (0.24.0)
- Updated to using flows to configure the queue connector device for pointings and signal display.

4.4.1
-----

- Updated the patch version of the averagetime-mswriter image to 0.3.1
  This picks up a minor change in how the data is accumulated
  and averaged.

4.4.0
-----

- Signal Display Metrics now configures down-scaled processing by default.
- Signal Display Metrics configuration is now validated using a pydantic model.
- Signal Display Metrics processor is updated to the latest version. It is now defaulted to 0.23.0
- Added a new processor that integrates the visibilities in time before writing out
  the Measurement Sets. This processor is called ``averagetime-mswriter``.

4.3.0
-----

- Use poetry for managing dependencies
- Add new Signal Metrics Display configuration
- **BREAKING** the previous Signal Metrics processors should no longer be used,
  only use the new configuration.
- Bumped the default version of the mswriter processor to 2.3.0
- Bumped the default version of the receiver to 5.2.2
- Use ska-sdp-scripting 0.9.0, which uses ska-sdp-config 0.9.0
  with new pydantic-based interface.
- Bumped the default version of the RCAL processor to 2.0.1.
  Also to pick up changes to the configuration database.


4.2.1
-----

- Use ska-sdp-scripting 0.7.1, which removes cancelled deployments.

4.2.0
-----

- Moved receive_addresses code to scripting library (part of
  ska-sdp-scripting 0.7.0)
- Use ska-sdp-scripting 0.7.0, which uses ska-sdp-config 0.6.0
  with new pydantic-based interface. It also addresses a bug in
  pb.receive_addresses.
- **BREAKING**
  This and newer versions are not compatible with SDP version < 0.21.0


4.1.0
-----

- mswriter-processor version updated to 2.0.2
- New function added to configure queue connector for dish pointing
  in case vis-receive is deployed together with pointing-offset script
- Upgraded the QA Metrics Processor to ``0.20.0``

4.0.0
-----

- New Signal Display Metrics processors added, using prefix ``signal-display-metrics-*``
  Four have been added ``-all``, ``-basic``, ``-phase``, and ``-amplitude``.
  The first can be used for small datasets/testing, but the other 3 should be
  used in almost all other cases.
- **BREAKING** the QA Metrics processor (``qa-metric-generator``) has been
  removed and replaced with ``signal-display-metrics-*`` processors.

3.0.0
-----

- **BREAKING**
  Bump latest version of ``mswriter`` processor to ``2.0.0``,
  ``rcal`` processor to ``2.0.0``,
  and ``receive-modules`` to ``5.0.0``.
  This new combination of components
  implements the required logic
  to close the output Measurement Sets at EndScan time,
  as well as updating the SDP Config DB
  with the list of Measurement Sets that have been written.
  This change is breaking
  because this new version of the receiver
  will not work against processors
  based on versions of the ``receive-processors`` package
  lower than ``2.0.0``.

2.3.0
-----

- Bump latest version of ``rcal`` processor
  to ``1.0.0``.
  This version of the ``rcal`` processor
  can deal with the latest versions of the receiver
  that implement data aggregation across time and frequency.

2.2.1
-----

- Use ska-sdp-scripting 0.6.3, which implements configuring the
  QueueConnector device in Phase enter and exit methods

2.2.0
-----

- The ``processors`` PB parameter can now be a dictionary instead of a list,
  allowing for partial built-in overrides to be given.
  Passing a list is still supported, but deprecated.
- Bump default ``mswriter`` processor version to ``1.1.1``.
- Add initial ``qa-metrics-generator`` and ``rcal`` processor definitions.

2.1.0
-----

- Updated to new LMC Queue Connector 3.0.1 configuration database layout and schema.
- Added waiting for LMC Queue Connector to reach ON state before
  transitioning PB to ready.
- Updated to scripting library ``~= 0.6.2``.

2.0.0
-----

- Changed how the network configuration is read
  from the SDP configuration database.
  Instead of reading it from ``/network-definition``,
  it is now read from ``/resource/<resource_type>:<resource_name>``.
- Changed how networks are allocated within SDP.
  Instead of updating a central ``/network-definintion`` value
  in the SDP configuration database,
  we now append/remove entries of the form ``<pb_id>:<resource_type>:<resource_name>``
  for each allocation under the ``/allocation`` prefix,
  and leave the ``/network-definition`` value unedited.
- **BREAKING** Extra PB parameters
  are not forwarded anymore
  as Helm values for the |vis-recv-chart|.
  Use the new ``extra_helm_values`` PB parameter instead.
- Added top-level ``extra_helm_values`` PB parameter
  (defaults to ``{}``)
  where users can specify any arbitrary extra set
  of Helm values that should be directly forwarded
  to the |vis-recv-chart|.
- Added top-level ``port_start`` PB parameter (defaults to ``21000``),
  which was previously hidden within the implicit set
  of Helm values that the vis-receive chart received.
- Added convenience top-level ``transport_protocol`` PB parameter
  (defaults to ``"udp"``),
  which previously had to be given via the extra Helm values
  forwarded to the vis-receive Chart.

1.5.0
-----

- Updated default major versions of the receiver and mswriter processor images
  to ``4`` and ``1`` respectively.
- The ``SDP_KAFKA_HOST`` environment variable
  pointing to the SDP Kafka instance
  is now exposed to all pods.

1.4.0
-----

- Added ``use_network_definition`` as a PB parameter. When ``True`` this
  will use the ``/network-definition`` stored in SDP etcd to override
  the ``extraMetada`` field of ``pod_settings`` to use the host network
  port defined via a "network attachment definition" in Kubernetes
  along with an allocated IP address with its cidr. The used subnet
  range will be added to the allocted-subnets of the
  ``/network-definition`` and update the SDP etcd entry. On completion
  the allocated subnet will be released.
- Update to scripting library ``~= 0.6.0``.
- Stop forcing a backend for the ska-sdp-config library
  in the pods launched by this script.

1.3.0
-----

- Added writing to LMC Queue Connector database configuration.

1.2.0
-----

- Update version of built-in ``mswriter`` processor to ``0.5.1``
  and of the receiver to ``3.10.1``.

1.1.1
-----

- Update to latest scripting library (0.5.2)

1.1.0
-----

- Update to latest scripting library (0.5.0). This required
  the update of how phase.wait_loop is used in the script.

1.0.1
-----

- Fix issue with custom IPs causing the script to crash
  due to a wrong variable name being referenced
  while updating the PB's receive addresses.

1.0.0
-----

- Complete rewrite to use the new ``vis-receive`` Helm Chart,
  which is itself an almost complete rewrite of the ``receive`` Helm Chart.

0.8.1
-----

- Update to latest scripting library (0.4.2)

0.8.0
-----

- Read name of PVC to be used by the receiver
  from the ``SDP_DATA_PVC_NAME`` environment variable
  as communicated by the SDP Processing Controller.
- Communicate more environment variables
  to extra containers.
  This is required to use the new SDP metadata package.

0.7.0
-----

- Update to latest receiver image (3.6.0).
- Add verbosity PB parameter.
- Add PB parameters to map a native network device, assign an IP to it,
  and indicate a node selector for the receiver pod.

0.6.0
-----

- Update to latest receiver image (3.3.0).
- Update to latest scripting library (0.4.1)
- Point the receiver to the correct SDP Configuration Database and Execution Block
  so it can read Scan metadata associated to each incoming payload
  and the list of antennas making up the subarray.
- Made Docker image slimmer by removing unnecessary software installations.

0.5.1
-----

- Update to latest receiver image (2.1.0).
- Use new readinessProbe available on the receiver chart.
- Removed ``transmission.*`` option handling as they are not needed anymore and added confusion.

0.5.0
-----

- Port to use SDP scripting library (formerly known as the workflow library).

0.4.0
-----

- Removed reception.num_ports configuration value, replaced it with reception.channels_per_stream,
  which is what the receiver program uses.
- Improved default values used to launch receiver chart.

0.3.8
-----

- Refactored code and added workflow pytest code

0.3.7
-----

- Updated to add flexibility for the command parameter and updated default parameters

0.3.6
-----

- Ported to work with the correct version of the workflow library (0.2.6)

0.3.5
-----

- Capable to deploy a receive process, plasma store, and a number of other processes to
  consume the data from the plasma store.

0.3.4
-----

- Use dependencies from the central artefact repository and publish the
  workflow image there.

0.3.3
-----

- Ported to use the latest version of workflow library (0.2.4). Capable to deploy multiple receive processes.
  Ports published in the receive addresses match with the actual ports of the receive process(es)

0.3.2
-----

- use python:3.9-slim as the base docker image
