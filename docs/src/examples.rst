.. _examples:

Examples
========

Simple examples of real-time and batch scripts to help developing new ones.

.. toctree::
  :maxdepth: 1

  examples/realtime-example
  examples/batch-example
