"""
Pointing offset script
"""

import logging
import os

from packaging import version
from pointing_offset_params import (
    DEFAULT_ENCODING,
    DEFAULT_KAFKA_TOPIC,
    DEFAULT_PIPELINE_VERSION,
    PointingOffsetParams,
)
from ska_sdp_config.entity.common import PVCPath
from ska_sdp_config.entity.flow import (
    DataProduct,
    DataQueue,
    Flow,
    FlowSource,
    TangoAttribute,
    TangoAttributeMap,
    TangoAttributeUrl,
)
from ska_sdp_scripting import ProcessingBlock
from ska_sdp_scripting.utils import ProcessingBlockStatus
from ska_ser_logging import configure_logging

configure_logging()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

MIN_PIPELINE_VERSION = "1.0.0"

KAFKA_HOST = os.getenv("SDP_KAFKA_HOST", "localhost:9092")
PB_ID = os.getenv("SDP_PB_ID")
SDP_CONFIG_HOST = os.getenv("SDP_CONFIG_HOST", "127.0.0.1")
SDP_DATA_PVC_NAME = os.getenv("SDP_DATA_PVC_NAME")
KUBE_NAMESPACE_SDP = os.getenv("SDP_HELM_NAMESPACE")


def _get_antenna_names(pb):
    """
    Extract antenna names from processing block

    :param pb: processing block
    """
    for txn in pb._config.txn():
        eb = txn.execution_block.get(pb._eb_id)
        try:
            resources = eb.resources
            ants = resources.get("receptors")
            LOG.info("Using antennas: %s", ants)
        except (KeyError, AttributeError, TypeError):
            LOG.error(
                "No valid list of antennas provided, Cannot set up QueueConnector."
            )
    return ants


def get_receive_pb_id(pb, pointing_pb_id):
    """
    Find the PB id for the receive script that is started
    with the pointing pipeline. We need this to determine
    in which directory the data are saved by the mswriter.
    Receive PB needs to be defined in dependencies for pointing.
    """
    dependencies = pb.get_dependencies()
    try:
        receive_pb_id = dependencies[0].pb_id
    except IndexError:
        # dependencies list is empty
        receive_pb_id = pointing_pb_id

    return receive_pb_id


def valid_pipeline_version(given_version, min_version=MIN_PIPELINE_VERSION):
    """
    Method for checking the pipeline version is within the allowable range.
    """
    # allow for version not being set
    if given_version is None:
        return False

    # allow for development versions
    if "-dev" in given_version:
        given_version = given_version.split("-dev")[0]

    return version.parse(min_version) <= version.parse(given_version)


def update_pipeline_version(parameters):
    """
    Method for ensuring the pipeline parameter 'version' is valid.
    This updates the "version" parameter in pb.
    """
    current_version = parameters.get("version")

    if current_version is None:
        parameters["version"] = DEFAULT_PIPELINE_VERSION
        LOG.warning(
            "No pipeline version was provided, setting to default %s",
            DEFAULT_PIPELINE_VERSION,
        )

    elif not valid_pipeline_version(current_version):
        parameters["version"] = DEFAULT_PIPELINE_VERSION
        LOG.error(
            "You have tried to use version %s of the pointing "
            "offset calibration pipeline, which is not allowed. "
            "To ensure compatibility the version has been reset to %s",
            current_version,
            DEFAULT_PIPELINE_VERSION,
        )


def set_up_envs(parameters):
    """
    Set up the environment variables needed for
    the pointing pipeline
    This updates the "env" parameter in pb.
    """
    parameters["dataProductStorage"] = {
        "name": SDP_DATA_PVC_NAME,
        "mountPath": "/mnt/data",
    }

    sdp_config_port = int(os.getenv("SDP_CONFIG_PORT", "2379"))
    kafka_topic = parameters.get("kafka_topic", DEFAULT_KAFKA_TOPIC)

    # Extract telmodel_sources
    telescope_model = parameters.get("telescope_model", {})
    telmodel_sources = telescope_model.get("telmodel_sources", [])

    envs = parameters.get("env", [])
    default_envs = [
        {"name": name, "value": value}
        for name, value in {
            "SDP_PB_ID": PB_ID,
            "SDP_PROCESSING_SCRIPT": "True",
            "SDP_CONFIG_HOST": SDP_CONFIG_HOST,
            "SDP_CONFIG_PORT": str(sdp_config_port),
            "SDP_KAFKA_TOPIC": kafka_topic,
            "SDP_KAFKA_SERVER": KAFKA_HOST,
            "SKA_TELMODEL_SOURCES": ",".join(telmodel_sources),
        }.items()
        if value is not None
    ]
    parameters["env"] = envs + default_envs


def create_data_flows(pb, parameters, ants=None):
    """
    Create DataQueue and DataProduct objects in the configuration database

    :param pb: Processing block
    :param parameters: Parameters from the processing block
    :param ants: Antenna names from the execution block
    """
    # pylint: disable=too-many-locals

    eb_id = pb._eb_id
    encoding_type = parameters.get("encoding", DEFAULT_ENCODING)
    kube_namespaces = parameters.get("kube_namespaces", [KUBE_NAMESPACE_SDP])
    data_product_storage = parameters.get("dataProductStorage", {})
    mount_path = data_product_storage.get("mountPath", "/mnt/data")
    kafka_topic = parameters.get("kafka_topic", DEFAULT_KAFKA_TOPIC)
    flow_name = "pointing-offset"

    # source of the data is the MS written by vis-receive
    # (here Flow.Key points to that data flow object)
    vis_receive_id = get_receive_pb_id(pb, PB_ID)

    # the following has to match what vis-receive uses;
    # for now, hard-coded to current implementation
    vis_receive_data_flow_name = "vis-receive-mswriter-processor"

    if vis_receive_id == PB_ID:
        # In this case, we are running the pointing on its own, with
        # the understanding that the MS data for the pipeline can be
        # found in the processing block directory of the pointing script.
        # So we need to create the data flow pointing to it and configure
        # the pointing pipeline accordingly.
        ms_data_product = DataProduct(
            data_dir=PVCPath(
                k8s_namespaces=kube_namespaces,
                k8s_pvc_name=SDP_DATA_PVC_NAME,
                pvc_mount_path=mount_path,
                pvc_subpath=f"product/{eb_id}/ska-sdp/{PB_ID}",
            ),
            paths=[""],  # data path doesn't matter for this
        )
        pb.create_data_flow(
            name=vis_receive_data_flow_name,
            data_model="Visibility",
            sink=ms_data_product,
        )

    ms_data_source = FlowSource(
        uri=Flow.Key(
            pb_id=vis_receive_id,
            name=vis_receive_data_flow_name,
            kind="data-product",
        ),
        function="ska-sdp-wflow-pointing-offset:pointing-offset",
    )

    queue = DataQueue(
        topics=kafka_topic,
        host=KAFKA_HOST,
        format=encoding_type,
    )
    product = DataProduct(
        data_dir=PVCPath(
            k8s_namespaces=kube_namespaces,
            k8s_pvc_name=SDP_DATA_PVC_NAME,
            pvc_mount_path=mount_path,
            pvc_subpath=f"product/{eb_id}/ska-sdp/{PB_ID}",
        ),
        paths=[
            "{common_prefix}pointing_offsets.hdf5",
        ],
    )

    tango_product = TangoAttributeMap(
        attributes=[
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(
                        # only attribute name is used by QC, but we need to provide
                        # a full tango attribute url, so we default to mid-sdp. This
                        # is not used by the QC, so it doesn't matter what it's set to
                        f"tango://mid-sdp/queueconnector/01/pointing_offset_{antenna}"
                    ),
                    dtype="DevDouble",
                    max_dim_x=3,
                    default_value=float("NaN"),
                ),
                TangoAttributeMap.DataQuery(
                    select=f"[?[0]=='{antenna}'][[1], [2], [4]][]"
                ),
            )
            for antenna in ants
        ]
    )
    data_models = {"msgpack_numpy": "PointingNumpyArray"}
    data_model = data_models.get(encoding_type)

    kafka_flow = pb.create_data_flow(name=flow_name, data_model=data_model, sink=queue)
    kafka_flow.add_source(ms_data_source)
    pb.create_data_flow(name=flow_name, data_model=data_model, sink=product).add_source(
        ms_data_source
    )

    # Flow to create Tango attributes from one Kafka topic
    pb.create_data_flow(
        name="tango-flow", data_model=data_model, sink=tango_product
    ).add_source(kafka_flow, function="ska-sdp-lmc-queue-connector:exchange")

    LOG.info("Created data queue and data product flows in the configuration database")


def update_parameters_for_execution(parameters, eb_id):
    """
    Update parameters (mainly command line arguments)
     for the pointing offset execution engine.

    :param parameters: processing block parameters
    :param eb_id: Execution block ID
    """

    num_of_scans = parameters.get("num_scans", 5)
    # Here are default command line
    additional_args = parameters.get(
        "additional_args",
        ["--use_source_offset_column"],
    )

    # Extract rfi_key information
    if telescope_model := parameters.get("telescope_model", {}):
        rfi_key = telescope_model.get("static_rfi_key", "")
        static_rfi_key = ["--rfi_file", f"{rfi_key}"]
        additional_args.extend(static_rfi_key)
    elif "--apply_mask" in additional_args:
        LOG.warning("Because no telescope model is provided, No RFI mask is applied.")
        additional_args.remove("--apply_mask")

    default_command_args = [
        "compute",
        "--eb_id",
        f"{eb_id}",
        "--num_scans",
        f"{num_of_scans}",
        *additional_args,
    ]
    command_args = parameters.get("args", default_command_args)
    parameters["args"] = command_args

    LOG.info(
        "Pointing-offset pipeline will run with the following args: %s",
        command_args,
    )


def find_receive_address(work_phase, pb, attribute_name=None):
    """
    Find the correct receive addresses for the processing block
    to update later.
    """
    eb_id = work_phase._eb_id
    for txn in work_phase._config.txn():
        eb = txn.execution_block.get(eb_id)
        subarray_id = eb.subarray_id
        system = txn.system.get()
        system_component = system.components[
            f"lmc-queueconnector-{subarray_id:02}"
        ]  # Retrieve the SystemComponent object

        try:
            device_name = system_component.devicename
            LOG.info("Use address: %s", device_name)
        except KeyError:
            LOG.info("Use default prefix for QueueConnector device")
            device_name = f"test-sdp/queueconnector/{subarray_id:02}"

    scan_types = pb.get_scan_types()
    tango_attribute = attribute_name or "pointing_offset"
    recv_address = {
        scan_type["scan_type_id"]: {
            beam_id: {
                "pointing_cal": f"tango://{device_name}/{tango_attribute}_{{dish_id}}"
            }
            for beam_id, beam_val in scan_type["beams"].items()
            if beam_val["channels_id"] == "vis_channels"
        }
        for scan_type in scan_types
    }

    return recv_address


def main(pb):
    """Execute processing"""
    pb.validate_parameters(model=PointingOffsetParams)

    # Get and update initial processing block parameters
    parameters = pb.get_parameters()
    update_pipeline_version(parameters)
    set_up_envs(parameters)
    # Antenna names
    ants = _get_antenna_names(pb)

    LOG.info("Updating configuration for the queue connector.")

    # Create data flows
    create_data_flows(pb, parameters, ants)

    # Create work phase
    LOG.info("Creating work phase.")
    work_phase = pb.create_phase("Work", [])

    with work_phase:
        eb_id = work_phase._eb_id
        update_parameters_for_execution(parameters, eb_id)
        LOG.info("Deploying pointing-offset execution engine.")

        work_phase.ee_deploy_helm("pointing-offset", parameters)

        LOG.info("Update receive addresses.")
        recv_address = find_receive_address(
            work_phase, pb, parameters.get("tango_attribute")
        )
        pb.receive_addresses(configured_host_port=recv_address, update_dns=False)

        # Signal that the script is ready to do its processing
        work_phase.update_pb_state(status=ProcessingBlockStatus.READY)

        LOG.info("Done, now idling...")

        work_phase.wait_loop(work_phase.is_eb_finished)


if __name__ == "__main__":
    with ProcessingBlock() as processing_block:
        main(processing_block)
