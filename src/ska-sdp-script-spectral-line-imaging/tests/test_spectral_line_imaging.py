"""
Unit tests for the spectral line imaging script
"""

from unittest.mock import patch

from spectral_line_imaging import setup_envs, update_parameters_for_execution


# Helper functions
def assert_dict_value(result, expected):
    """
    Assert values in two dicts are the same
    """
    for key, value in expected.items():
        if isinstance(value, dict):
            assert_dict_value(result[key], value)
        else:
            assert result[key] == value


def get_expected_parameters(skymodel_image_name, additional_args=None):
    """
    Return expected parameters for assertion
    """
    return {
        "pipeline": {
            "enabled": True,
            "name": "sip",
            "command": "spectral-line-imaging-pipeline",
            "args": [
                "run",
                "--input",
                "/mnt/data/product/eb_id/ska-sdp/upstream_pb_id/test.ps",
                "--output",
                "/mnt/data/product/eb_id/ska-sdp/pb_id",
                "--set",
                "parameters.read_model.image",
                "/mnt/data/product/eb_id/ska-sdp/upstream_pb_id/" + skymodel_image_name,
            ]
            + additional_args,
        }
    }


@patch("spectral_line_imaging.SDP_DATA_PVC_NAME", "pvc")
@patch("spectral_line_imaging.PIPELINE_VERSION", "TEST")
def test_setup_envs():
    """
    Test to assert if container image, data product storage information
    is updated in parameters
    """
    parameters = {}

    actual_parameters = setup_envs(parameters)

    expected_parameters = {
        "worker": {"volume": {"name": "pvc", "path": "/mnt/data"}},
        "image": "artefact.skao.int/ska-sdp-spectral-line-imaging:TEST",
    }

    assert_dict_value(actual_parameters, expected_parameters)


@patch("spectral_line_imaging.SDP_DATA_PVC_NAME", "pvc")
@patch("spectral_line_imaging.PIPELINE_VERSION", "TEST")
def test_setup_envs_helm_chart_update():
    """
    Test to assert if dask helm chart parameters can be updated
    """
    parameters = {"dask": {"worker": {"replicas": 4}}}

    actual_parameters = setup_envs(parameters)

    expected_parameters = {
        "worker": {"volume": {"name": "pvc", "path": "/mnt/data"}, "replicas": 4},
        "image": "artefact.skao.int/ska-sdp-spectral-line-imaging:TEST",
    }

    assert_dict_value(actual_parameters, expected_parameters)


def test_execution_parameters():
    """
    Test to assert if parameters are passed to properly to the helm chart
    """
    parameters = {
        "msname": "test.ps",
        "worker": {"volume": {"name": "pvc", "path": "/mnt/data"}},
        "skymodel_image": "test-model-%s-image.fits",
    }

    update_parameters_for_execution(parameters, "eb_id", "upstream_pb_id", "pb_id")

    assert_dict_value(
        parameters, get_expected_parameters("test-model-%s-image.fits", [])
    )


def test_config_override_with_additional_params():
    """
    Test to assert if additional parameters are passed to the
    pipeline
    """
    additional_args = ["--set", "pipeline.vis_stokes_conversion", "false"]
    parameters = {
        "msname": "test.ps",
        "worker": {
            "volume": {"name": "pvc", "path": "/mnt/data"},
        },
        "skymodel_image": "test-model-%s-image.fits",
        "additional_args": additional_args,
    }

    update_parameters_for_execution(parameters, "eb_id", "upstream_pb_id", "pb_id")

    assert_dict_value(
        parameters, get_expected_parameters("test-model-%s-image.fits", additional_args)
    )
