"""
Fixtures for testing the pointing-offset script.
"""

import pytest
from ska_sdp_config import Config, entity


@pytest.fixture(name="config")
def config_fxt():
    """Config db object"""
    with Config(backend="memory") as config:
        config.backend._data.clear()
        yield config


@pytest.fixture(name="default_config")
def default_config_fxt(config):
    """
    Default config using simple execution block string
    and empty config dictionary string
    """
    execution_block = {
        "key": "eb-test-20210630-00000",
        "subarray_id": "01",
        "resources": {
            "receptors": ["SKA001", "SKA002", "SKA003"],
            "receive_nodes": 1,
        },
    }
    test_config = entity.system.System(
        version="1.2.3",
        components={
            "lmc-queueconnector-01": entity.system.SystemComponent(
                image="artefact.skao.int/ska-sdp-lmc",
                version="4.5.6",
                devicename="test/device/0",
            ),
        },
        dependencies={},
    )

    for txn in config.txn():
        eblock = entity.ExecutionBlock(**execution_block)
        txn.execution_block.create(eblock)
        txn.system.create(test_config)

    return config
