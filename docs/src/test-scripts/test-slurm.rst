.. _test_slurm:

.. include:: ../../../src/ska-sdp-script-test-slurm/README.rst

Processing block parameters
---------------------------

.. autopydantic_settings:: test_slurm_params.TestSlurmParams

.. include:: ../../../src/ska-sdp-script-test-slurm/CHANGELOG.rst
