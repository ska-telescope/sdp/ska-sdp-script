SDP Scripts
===========

.. toctree::
  :maxdepth: 1

  script-development
  examples
  scripts
  test-scripts


.. _available_scripts:

Available scripts
-----------------

The following table gives the list of available real-time processing scripts,
the version of the SDP in which they are supported, as well as the
corresponding parameter schema version.

.. include:: scripts-table-realtime


In addition, the following batch processing scripts are available.

.. include:: scripts-table-batch
