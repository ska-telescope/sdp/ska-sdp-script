Changelog
=========

1.0.0
-----

- Update documentation and example_configuration_string.json
  (`MR256 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/256>`__)
- Fix configuring the Pointing Pipeline via vis-receive MS data flow
  (`MR250 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/250>`__)
- Remove code to configure queue connector before v5
  (`MR240 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/240>`__)
- Update Dockerfile to use SKA python base image
  (`MR211 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/211>`__)

0.8.0
-----

- Minimum allowed pipeline version is set to 0.9.0
  (`MR202 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/202>`__)
- Remove checking for maximum pipeline version
  (`MR202 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/202>`__)
- Update ska-sdp-scripting to 0.12.0
  (`MR202 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/202>`__)
- Update to configure QueueConnector Tango attributes using a data flow
  (`MR164 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/164>`__)
- Update to configure sources in data flow entries
  (`MR197 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/197>`__)
- Processing script reports internal errors in pb state
  (`MR185 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/185>`__)
- Pydantic model included in documentation
  (`MR189 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/189>`__)
- JSON parameter schema added to tmdata
  (`MR186 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/186>`__)
- Validate processing block parameters using scripting library 0.10.0
  (`MR180 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/180>`__)
- Added processing block parameter JSON schema and Pydantic model
  (`MR174 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/174>`__,
  `MR177 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/177>`__)
- Simplify processing block parameters and minor refactoring
  (`MR172 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/172>`__)

0.7.0
-----

- Update pointing offset docs
  (`MR167 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/167>`__)
- Update to scripting library 0.9.0 and use ska-sdp-python base image
  (`MR166 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/166>`__)
- Add sdp version requirements
  (`MR160 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/160>`__)
- Set up skart for dependency updates of processing scripts
  (`MR156 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/156>`__)
- Use poetry for dependency management
  (`MR155 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/155>`__)
- Set pointing offset pipeline allowable version range
  (`MR154 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/154>`__)
- Use pydantic execution block
  (`MR151 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/151>`__)
- Documentation updates
  (`MR142 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/142>`__)
- Pointing script generates data flow entries
  (`MR141 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/141>`__)

0.6.1
-----

Minimum requirements
^^^^^^^^^^^^^^^^^^^^

- Same as 0.6.0

Changes
^^^^^^^

- Use ska-sdp-scripting 0.7.1, which removes cancelled deployments
  (`MR140 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/140>`__)

0.6.0
-----

.. _minimum-requirements-1:

Minimum requirements
^^^^^^^^^^^^^^^^^^^^

- Pointing offset calibration pipeline (ska-sdp-wflow-pointing-offset):
  `0.5.0 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/tree/0.5.0>`__
- SDP (ska-sdp-integration):
  `0.21.0 <https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/releases/0.21.0>`__
- Queue Connector (ska-sdp-lmc-queue-connector):
  `4.1.0 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc-queue-connector/-/tree/4.1.0>`__
- vis-receive script:
  `4.2.0 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/blob/master/docs/src/scripts/vis-receive/changelog.rst>`__

.. _changes-1:

Changes
^^^^^^^

- Use ska-sdp-scripting 0.7.0, which uses ska-sdp-config 0.6.0 with new
  pydantic-based interface
  (`MR134 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/134>`__)

0.5.0
-----

.. _minimum-requirements-2:

Minimum requirements
^^^^^^^^^^^^^^^^^^^^

- Pointing offset calibration pipeline (ska-sdp-wflow-pointing-offset):
  `0.5.0 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/tree/0.5.0>`__
- SDP (ska-sdp-integration):
  `0.19.0 <https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/releases#anchor-0190>`__
- Queue Connector (ska-sdp-lmc-queue-connector):
  `4.1.0 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc-queue-connector/-/tree/4.1.0>`__
  Note: only SDP 0.21.0+ runs version 4.1.0 of the Queue Connector by
  default; previous versions will need to manually set it
- vis-receive script:
  `4.1.0 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/blob/master/docs/src/scripts/vis-receive/changelog.rst>`__

.. _changes-2:

Changes
^^^^^^^

- Documentation updates
  (`MR129 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/129>`__)
- Allow customization of the pointing offset tango attribute and update
  receiveAddresses
  (`MR128 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/128>`__)
- Update the QueueConnector exchange for pointing offsets to provide
  data on an attribute per dish
  (`MR125 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/125>`__,
  `MR129 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/129>`__)

0.4.0
-----

- Set minimum suggested pipeline version to 0.4.0
  (`MR121 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/121>`__)
- Load telescope model data path and file information from script
  parameters
  (`MR118 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/118>`__,
  `MR120 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/120>`__)
- Update documentation
  (`MR121 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/121>`__,
  `MR116 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/116>`__,
  `MR115 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/115>`__)
- Refactored the pointing script into functions and added unit tests
  (`MR112 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/112>`__)
- Delete unused arguments and set the results directory
  (`MR113 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/113>`__,
  `MR111 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/111>`__)

0.3.1
-----

- Use ska-sdp-scripting 0.6.3, which implements configuring the
  QueueConnector device in Phase enter and exit methods
  (`MR106 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/106>`__)

0.3.0
-----

- Update example JSON string and documentation
  (`MR98 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/98>`__,
  `MR99 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/99>`__,
  `MR101 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/101>`__,
  `MR100 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/100>`__)
- Update receive addresses with pointing FQDN
  (`MR95 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/95>`__)
- Use script dependencies to obtain pb_id of the vis-receive script
  (`MR94 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/94>`__)
- Script no longer configures the QueueConnector device directly (done
  via
  vis-receive)(`MR87 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/87>`__)

0.2.0
-----

- Update documentation to include usage of mock dishes and CBF emulator
  (`MR83 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/83>`__)
- Update to scripting library v0.6.1
  (`MR84 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/84>`__)

0.1.0
-----

- Configure the QueueConnector device to store pointing offsets and send
  Kafka information to pipeline via environment variables
  (`MR82 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/82>`__)
- Load additional arguments, if any, to CLI args from parameters
  (`MR81 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/81>`__)
- Load PVC information from environment variable
  (`MR81 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/81>`__)

0.0.2
-----

- Use the vis-receive script’s PB ID (if exists) for determining the MS
  directory
  (`MR80 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/80>`__)

0.0.1
-----

- Initial version of the pointing script
  (`MR74 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/74>`__)
