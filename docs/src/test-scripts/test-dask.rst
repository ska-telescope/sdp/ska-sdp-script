.. include:: ../../../src/ska-sdp-script-test-dask/README.rst

Processing block parameters
---------------------------

.. autopydantic_settings:: test_dask_params.TestDaskParams

.. include:: ../../../src/ska-sdp-script-test-dask/CHANGELOG.rst