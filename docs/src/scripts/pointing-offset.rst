Pointing Offset Script
======================

This script deploys the SKA :external+ska-sdp-wflow-pointing-offset:doc:`pointing offset pipeline <index>`,
which calculates pointing offsets from "pointing"-type scan data. The pipeline is deployed by SDP using the
:external+ska-sdp-helmdeploy-charts:doc:`pointing-offset helm chart <charts/pointing-offset>`.

To set up SDP on your local machine using Minikube (or a remote cluster), follow the instructions at
:external+ska-sdp-integration:doc:`Installing SDP <installation/standalone>`.
If you want to test the script, we suggest you deploy SDP together with itango.

The script is configured using a JSON configuration string, and implemented by running the SDP AssignResources command on the subarray device.
The options for the configuration string, and the necessary environment variables are described in :ref:`pointing_configure`.

A detailed procedure to test the script is described in :ref:`pointing_test` and also demonstrated in a Jupyter notebook in the
:external+ska-sdp-notebooks:doc:`SDP Notebooks repository <index>`.

.. toctree::
   :maxdepth: 1

   pointing-offset/configuration
   pointing-offset/testing
   pointing-offset/changelog
