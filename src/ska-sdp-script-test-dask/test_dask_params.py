"""
Pydantic model for processing script parameters.
"""

from pydantic import Field
from pydantic.config import ConfigDict
from ska_sdp_scripting.processing_block import ParameterBaseModel


class TestDaskParams(ParameterBaseModel):
    """
    test-dask script parameters
    """

    model_config = ConfigDict(title="test-dask")

    n_workers: int = Field(
        default=2,
        title="Number of workers",
        description="Number of workers for Dask processing",
    )
