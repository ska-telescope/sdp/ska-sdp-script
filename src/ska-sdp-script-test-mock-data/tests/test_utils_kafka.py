"""
Unittests for utils_kafka.py
"""

import numpy
import pytest
from generate_mock_data.utils_kafka import (
    convert_to_structured,
    send_data_via_kafka_pointing,
)
from ska_sdp_dataqueues import DataQueueConsumer
from ska_sdp_dataqueues.schemas import PointingNumpyArray
from utils import KAFKA_HOST, RECEPTORS


@pytest.fixture(name="pointing_structured")
def pointing_structured_fixture():
    """
    Provide data for testing the
    structured numpy array schema
    """
    data = {
        "antenna_name": "SKA001",
        "last_scan_index": 9.0,
        "xel_offset": 3.5,
        "xel_offset_std": 0.1,
        "el_offset": 2.8,
        "el_offset_std": 0.1,
        "expected_width_h": 1.1,
        "expected_width_v": 1.1,
        "fitted_width_h": 1.3,
        "fitted_width_h_std": 0.1,
        "fitted_width_v": 1.2,
        "fitted_width_v_std": 0.1,
        "fitted_height": 100.0,
        "fitted_height_std": 1.2,
    }

    return data


@pytest.fixture(name="pointing_raw")
def pointing_raw_fixture():
    """Generate a pointing numpy array"""

    nants = len(RECEPTORS)
    ants = numpy.array(RECEPTORS).reshape(nants, 1)
    values = [numpy.arange(0, 120, 10) / 10.0] * nants
    data = numpy.column_stack((ants, values))

    return data


@pytest.mark.test_with_kafka
async def test_send_numpy_to_kafka(pointing_structured):
    """
    Unit test for send_data_via_kafka_pointing
    """
    encoding = "msgpack_numpy"
    kafka_topic = "test-pointing"

    pointing_data_numpy = PointingNumpyArray(**pointing_structured).to_numpy()
    send_data_via_kafka_pointing(KAFKA_HOST, kafka_topic, pointing_data_numpy)

    # Read the data back
    kafka_consumer = DataQueueConsumer(
        KAFKA_HOST, topics=[kafka_topic], encoding=encoding
    )
    async with kafka_consumer:
        async for _, message in kafka_consumer:
            # Check the if the message is returned correctly
            numpy.testing.assert_equal(pointing_data_numpy, message)
            break


def test_convert_to_structured(pointing_structured):
    """Unit test for convert raw to structured data"""

    data = numpy.array([[3.5, 0.1, 2.8, 0.1, 1.1, 1.1, 1.3, 0.1, 1.2, 0.1, 100.0, 1.2]])
    data = numpy.column_stack((["SKA001"], data))

    expected_data_numpy = PointingNumpyArray(**pointing_structured).to_numpy()

    structured_data = convert_to_structured(data, 9)

    numpy.testing.assert_equal(structured_data, expected_data_numpy)
