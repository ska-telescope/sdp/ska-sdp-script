"""Test Visibility Receive models."""

from common import yaml_load_test_data
from models.vis_receive_script_params import VisReceiveParams
from pydantic import TypeAdapter


def test_default_vis_receive_params():
    """Test default VisReceiveParameters are model compliant."""
    assert isinstance(
        TypeAdapter(VisReceiveParams).validate_python(
            yaml_load_test_data("default_pb_params.yaml")
        ),
        VisReceiveParams,
    )
