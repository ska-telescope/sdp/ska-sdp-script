"""
Script to test generation of receive addresses.

The purpose of this script is to test the mechanism for generating SDP
receive addresses from the channel link map contained in the EB. The script
picks it up from there, uses it to generate the receive addresses for each scan
type and writes them to the processing block state. The subarray publishes this
address map on the appropriate attribute to complete the transition following
AssignResources.

This script does not generate any deployments.
"""

import logging

import ska_ser_logging
from ska_sdp_scripting import ProcessingBlock
from ska_sdp_scripting.utils import ProcessingBlockStatus
from test_receive_addresses_params import TestReceiveAddressesParams

ska_ser_logging.configure_logging()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)


def main(pb):
    """Run processing script."""
    pb.validate_parameters(model=TestReceiveAddressesParams)
    pb_parameters = pb.get_parameters()

    # Port and receive process configuration
    channels_per_port = pb_parameters.get("channels_per_port", 1)
    max_ports_per_node = pb_parameters.get("max_ports_per_node")
    requested_num_nodes = pb_parameters.get("num_nodes")
    recv_address_options = {}
    if max_ports_per_node is not None:
        max_channels_per_node = max_ports_per_node * channels_per_port
        recv_address_options["max_ports_per_host"] = max_channels_per_node
    else:
        if requested_num_nodes is None:
            requested_num_nodes = 1
        recv_address_options["num_hosts"] = requested_num_nodes

    # Determine hosts and port ranges for per scan type and beam
    port_start = pb_parameters.get("port_start", 9000)
    scan_types = pb.get_scan_types()
    host_port, _ = pb.config_host_port_channel_map(
        scan_types, port_start, channels_per_port, **recv_address_options
    )

    # Create work phase
    LOG.info("Create work phase")
    work_phase = pb.create_phase("Work", [])

    with work_phase:
        # Deploy a fake execution engine
        work_phase.ee_deploy_test("test-receive")

        # Add receive addresses to PB state
        pb.receive_addresses(
            configured_host_port=host_port,
            chart_name="test-receive",
        )

        # Signal that the script is ready to do its processing
        work_phase.update_pb_state(status=ProcessingBlockStatus.READY)

        # ... Do some processing here ...

        LOG.info("Done, now idling...")

        time_to_ready = pb.get_parameters().get("time_to_ready", 0)
        LOG.info("Setting time_to_ready to %s", time_to_ready)
        work_phase.wait_loop(work_phase.is_eb_finished, time_to_ready=time_to_ready)


if __name__ == "__main__":
    with ProcessingBlock() as processing_block:
        main(processing_block)
