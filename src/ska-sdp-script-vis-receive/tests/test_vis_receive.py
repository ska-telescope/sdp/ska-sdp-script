"""Test Visibility Receive script"""

from unittest.mock import call

import pytest
import pytest_mock
import ska_sdp_config
from common import yaml_load_test_data, yaml_validate
from mock_processing_block import MockProcessingBlock
from models.receive_processor_chart_values import ReceiveProcessorValues
from pydantic import TypeAdapter, ValidationError
from subnet_allocation import SubnetAllocator
from vis_receive import (
    BUILTIN_PROCESSORS,
    _deep_merge_dicts,
    _override_receive_addresses,
    read_receiver_chart_values,
    setup_vis_receive_processing_block,
)


@pytest.mark.parametrize(
    "source,target,expected",
    [
        (
            {"a": {"b": 1, "c": 3}},
            {"a": {"b": 2, "d": 4}},
            {"a": {"b": 1, "c": 3, "d": 4}},
        ),
    ],
)
def test_deep_merge_dicts(source: dict, target: dict, expected: dict):
    """test deep_merge_dicts function."""
    assert expected == _deep_merge_dicts(source, target)
    assert expected == target


@pytest.mark.parametrize("name", BUILTIN_PROCESSORS)
def test_builtin_processors(name: str):
    """Checks that built-in processors always yields non-empty chart values"""
    values = TypeAdapter(ReceiveProcessorValues).dump_python(
        read_receiver_chart_values(name, {}), exclude_none=True
    )
    assert len(values) > 0


@pytest.mark.parametrize("name", BUILTIN_PROCESSORS)
def test_builtin_processor_overrides(name: str):
    """Checks that built-in processors values can be overridden"""
    test_version = "my-version.0.1"
    original_values = read_receiver_chart_values(name, {})
    assert original_values.version != test_version
    overriden_values = read_receiver_chart_values(name, {"version": test_version})
    assert overriden_values.version == test_version


_EXTERNAL_PROCESSOR_NAME = "my-external-processor"
_EXTERNAL_PROCESSOR_DESCRIPTION = {
    "name": _EXTERNAL_PROCESSOR_NAME,
    "image": "my-image",
    "version": "version",
}


@pytest.mark.parametrize(
    "name, values",
    ((_EXTERNAL_PROCESSOR_NAME, _EXTERNAL_PROCESSOR_DESCRIPTION),),
)
def test_external_processor(name: str, values: dict):
    """Checks that external processors values are taken as-is"""
    assert name not in BUILTIN_PROCESSORS
    calculated_values = TypeAdapter(ReceiveProcessorValues).dump_python(
        read_receiver_chart_values(name, values), exclude_none=True
    )
    assert values == calculated_values


@pytest.mark.parametrize(
    "name, values",
    (
        (_EXTERNAL_PROCESSOR_NAME, {}),
        (_EXTERNAL_PROCESSOR_NAME, {"a": "b"}),
        (BUILTIN_PROCESSORS[0][:-1], {}),
        (BUILTIN_PROCESSORS[0][:-1], {"a": "b"}),
    ),
)
def test_external_processor_exceptions(name: str, values: dict):
    """Test that external processors values are validated."""
    assert name not in BUILTIN_PROCESSORS
    with pytest.raises(ValidationError):
        _ = read_receiver_chart_values(name, values)


@pytest.mark.parametrize("pb_params_file", (None, "default_pb_params.yaml"))
def test_setup_default_receive_script(
    mocker: pytest_mock.MockerFixture,
    pb_params_file: str,
    default_config_cidr24: ska_sdp_config.Config,
):
    """
    Tests that the default set of helm values calculated by the vis-receive
    script with the default set of PB parameters (except for
    "channels_per_port") is what we expect it to be. This is done both
    implicitly (without providing any PB parameters) and explicitly (providing
    a set of PB parameters that match the default ones).
    """
    mock_create_topic_with_partitions = mocker.patch(
        "vis_receive.create_topic_with_partitions"
    )
    if pb_params_file:
        pb_params = yaml_load_test_data(pb_params_file)
    else:
        pb_params = {}
    pb_params["channels_per_port"] = 13824 // 2
    pb_params["use_network_definition"] = False
    pb = MockProcessingBlock(**pb_params, config=default_config_cidr24)
    with SubnetAllocator.from_pb(pb) as subnet_allocator:
        values, custom_ip, _host_port = setup_vis_receive_processing_block(
            pb, subnet_allocator
        )
    assert not any(custom_ip)
    assert yaml_validate(values) == yaml_load_test_data("default_script_values.yaml")
    assert mock_create_topic_with_partitions.mock_calls == [
        call(topic="metrics-stats-01", partitions=1)
    ]


@pytest.mark.parametrize(
    "custom_ips",
    [
        ["10.20.0.2", "10.20.0.3"],
    ],
)
@pytest.mark.parametrize(
    "initial_state,expected_state",
    [
        (
            {"other": 0, "receive_addresses": {}},
            {"other": 0, "receive_addresses": {}},
        ),
        (
            {"receive_addresses": {"target:a": {"vis0": {"host": [(0, 0)]}}}},
            {"receive_addresses": {"target:a": {"vis0": {"host": [[0, "10.20.0.2"]]}}}},
        ),
        (
            {"receive_addresses": {"target:a": {"vis0": {"host": [(0, 1)]}}}},
            {"receive_addresses": {"target:a": {"vis0": {"host": [[0, "10.20.0.2"]]}}}},
        ),
        (
            {"receive_addresses": {"target:a": {"vis0": {"host": [(0, 0), (20, 1)]}}}},
            {
                "receive_addresses": {
                    "target:a": {
                        "vis0": {"host": [[0, "10.20.0.2"], [20, "10.20.0.3"]]}
                    }
                }
            },
        ),
        (
            {
                "receive_addresses": {
                    "target:a": {
                        "vis0": {"host": [(0, 0), (100, 1)]},
                        "calibration": {"host": [(0, 1)]},
                    }
                }
            },
            {
                "receive_addresses": {
                    "target:a": {
                        "vis0": {"host": [[0, "10.20.0.2"], [100, "10.20.0.3"]]},
                        "calibration": {"host": [[0, "10.20.0.2"]]},
                    }
                }
            },
        ),
    ],
)
def test_override_receive_addresses(
    custom_ips: list[str],
    default_config_cidr24: ska_sdp_config.Config,
    initial_state: dict,
    expected_state: dict,
):
    """Unit test for overriding processing block receive_addresses state."""
    mock_pb = MockProcessingBlock(
        channels_per_port=13824 // 2,
        num_nodes=2,
        use_network_definition=True,
        config=default_config_cidr24,
    )
    for txn in mock_pb._config.txn():
        state = txn.processing_block.state(mock_pb._pb_id).create(initial_state)
    _override_receive_addresses(mock_pb, custom_ips)
    for txn in mock_pb._config.txn():
        state = txn.processing_block.state(mock_pb._pb_id).get()
        assert state == expected_state
