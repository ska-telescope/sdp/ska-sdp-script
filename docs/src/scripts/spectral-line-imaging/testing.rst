.. _spectral_line_test:

Testing in SDP deployed on DP Cluster
=====================================

1. Data for Testing
-------------------

**Prerequisites** SDP System will have to be running in the kubernetes cluster, to run this script.
Steps to install SDP System are available in
:external+ska-sdp-integration:doc:`installation/standalone`.

**Assumption**: The current script assumes a MSv4 measurement set, model
images, config yaml is present in the same folder as the processing block
of this script.

Spin up a shell pod and transfer the MSv4 measurement set and the model
images. The `<eb-id>` and `<pb-id>` specified here should be the same as the
`<eb-id>` and `<pb-id>` in the configuration string.

.. code-block:: console

    kubectl -n <control-system-namespace> scale sts/shell --replicas=1
    kubectl -n <control-system-namespace> cp MWA.ps shell-0:/mnt/data/product/<eb-id>/ska-sdp/<pb-id>
    kubectl -n <control-system-namespace> cp model-I-image.fits shell-0:/mnt/data/product/<eb-id>/ska-sdp/<pb-id>
    kubectl -n <control-system-namespace> cp model-Q-image.fits shell-0:/mnt/data/product/<eb-id>/ska-sdp/<pb-id>
    kubectl -n <control-system-namespace> cp model-U-image.fits shell-0:/mnt/data/product/<eb-id>/ska-sdp/<pb-id>
    kubectl -n <control-system-namespace> cp model-V-image.fits shell-0:/mnt/data/product/<eb-id>/ska-sdp/<pb-id>

Remove the shell pod after transferring the data.

.. code-block:: console

    kubectl -n <control-system-namespace> scale sts/shell --replicas=0

2. Run AssignResources on the subarray device
---------------------------------------------

Exec into the ITango pod (with ``kubectl`` or ``k9s``):

.. code-block:: console

    kubectl exec -it ska-tango-base-itango-console -n <control-system-namespace> -- itango3

Connect to the device:

.. code-block:: python

    d = DeviceProxy('test-sdp/subarray/01')
    d.On()

Load the configuration string into a variable:

.. code-block:: python

    config_str = {
      "interface": "https://schema.skao.int/ska-sdp-assignres/0.4",
      "resources": {
        "receptors": ["SKA001", "SKA036", "SKA063", "SKA100"],
        "receive_nodes": 1
      },
      "execution_block": {
        "eb_id": "eb-test-20210630-00000",
        "context": {},
        "max_length": 21600.0,
        "channels": [
          {
            "channels_id": "vis_channels",
            "spectral_windows": [
              {
                "spectral_window_id": "fsp_1_channels",
                "count": 8,
                "start": 0,
                "stride": 1,
                "freq_min": 1.2925e9,
                "freq_max": 1.4125e9,
                "link_map": [
                  [0, 0],
                  [200, 1],
                  [744, 2],
                  [944, 3]
                ]
              }
            ]
          }
        ],
        "polarisations": [
          {
            "polarisations_id": "all",
            "corr_type": ["XX", "XY", "YX", "YY"]
          }
        ],
        "fields": [
          {
            "field_id": "field_a",
            "phase_dir": {
              "ra": [2.711325],
              "dec": [-0.01328889],
              "reference_time": "...",
              "reference_frame": "ICRF3"
            },
            "pointing_fqdn": "low-tmc/telstate/0/pointing"
          },
          {
            "field_id": "field_b",
            "phase_dir": {
              "ra": [294.8629],
              "dec": [-63.44029],
              "reference_time": "...",
              "reference_frame": "ICRF3"
            },
            "pointing_fqdn": "low-tmc/telstate/0/pointing"
          }
        ],
        "beams": [
          {
            "beam_id": "vis0",
            "function": "visibilities"
          }
        ],
        "scan_types": []
      },
      "processing_blocks": [
        {
          "pb_id": "pb-test-20211111-00000",
          "script": {
            "kind": "batch",
            "name": "spectral-line-imaging",
            "version": "0.2.0"
          },
          "parameters": {
            "msname": "MWA.ps",
            "skymodel_image": "model-%s-image.fits",
            "additional_args": []
          },
          "dependencies": []
        }
      ]
    }

.. code-block:: python

    import json
    config = json.dumps(config_str)

Run AssignResources:

.. code-block:: python

    d.assignResources(config)

This will start the processing block of the spectral line imaging script.
A Dask Helm chart is deployed with the image of the pipeline. This Helm chart
will spawn a Dask scheduler, Dask workers and a kubernetes job which runs the
pipeline with the arguments provided. The kubernetes job is removed when
the pipeline is finished.

To remove the deployment, you need to end the observation
by returning the subarray to an ``EMPTY`` state (in ITango):

3. Cleaning up
--------------

.. code-block:: python

    d.end()
    d.releaseAllResources()
