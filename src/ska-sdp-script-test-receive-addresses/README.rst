Test Receive Addresses Script
=============================

Introduction
------------

The purpose of this script is to test the mechanism for generating SDP
receive addresses from the channel link map for each scan type in the
execution block. The script picks it up from there, uses it to generate
the receive addresses for each scan type and writes them to the
processing block state. The addresses are published on the
receiveAddresses attribute by the SDP subarray.

Full description of processing block parameters of this script can be
found at the `Processing block
parameters <https://developer.skao.int/projects/ska-sdp-script/en/latest/test-scripts/test-receive-addresses.html#processing-block-parameters>`__
section.

Testing
-------

`Deploy
SDP <https://developer.skao.int/projects/ska-sdp-integration/en/latest/running/standalone.html>`__
and make sure the `iTango
console <https://developer.skao.int/projects/ska-sdp-integration/en/latest/running/standalone.html#accessing-the-tango-interface>`__
pod is also running.

After entering the iTango pod, obtain a handle to a subarray device and
turn it on:

.. code-block:: python

   d = DeviceProxy('test-sdp/subarray/01')
   d.On()

If you are not sure what devices are available, list them with
``lsdev``. Here is the configuration string for the execution block:

.. code-block:: python

   config = '''
   {
     "interface": "https://schema.skao.int/ska-sdp-assignres/0.4",
     "resources": {
       "receptors": ["SKA001", "SKA002", "SKA003", "SKA004"]
     },
     "execution_block": {
       "eb_id": "eb-test-20220916-00000",
       "context": {},
       "max_length": 3600.0,
       "beams": [
         {"beam_id": "vis0", "function": "visibilities"}
       ],
       "scan_types": [
         {
           "scan_type_id": ".default",
           "beams": {
             "vis0": {
               "channels_id": "vis_channels",
               "polarisations_id": "all"
             }
           }
         },
         {
           "scan_type_id": "target:a",
           "derive_from": ".default",
           "beams": {
             "vis0": {
               "field_id": "field_a"
             }
           }
         },
         {
           "scan_type_id": "calibration:b",
           "derive_from": ".default",
           "beams": {
             "vis0": {
               "field_id": "field_b"
             }
           }
         }
       ],
       "channels": [
         {
           "channels_id": "vis_channels",
           "spectral_windows": [
             {
               "spectral_window_id":"fsp_1_channels",
               "count":4,
               "start":0,
               "stride":2,
               "freq_min":350000000.0,
               "freq_max":368000000.0,
               "link_map":[[0, 0], [200, 1], [744, 2], [944, 3]]
             }
           ]
         }
       ],
       "polarisations": [
         {
           "polarisations_id": "all",
           "corr_type": ["XX", "XY", "YX", "YY"]
         }
       ],
       "fields": [
         {
           "field_id": "field_a",
           "phase_dir": {
             "ra": [123.0],
             "dec": [-60.0],
             "reference_frame": "ICRF3",
             "reference_time": "..."
           }
         },
         {
           "field_id": "field_b",
           "phase_dir": {
             "ra": [123.0],
             "dec": [-60.0],
             "reference_frame": "ICRF3",
             "reference_time": "..."
           }
         }
       ]
     },
     "processing_blocks": [
       {
         "pb_id": "pb-test-20220916-00000",
         "script": {"kind": "realtime", "name": "test-receive-addresses", "version": "0.5.0"},
         "sbi_ids": ["sbi-test-20220916-00000"],
         "parameters": {}
       }
     ]
   }
   '''

Note that each script may come in multiple versions. Always use the
latest version, unless you know a specific version that suits your
needs. (The Changelog at the end of this page may help you to decide.)

Start the execution block with the ``AssignResources`` command:

.. code-block:: python

   d.AssignResources(config)

You can connect to the configuration database by running the following
command:

.. code-block:: console

   kubectl -n <namespace> exec -it ska-sdp-console-0 -- bash

and from there to see the full list of entries, run

.. code-block:: console

   ska-sdp list -a

To check if the receive addresses are updated in the processing block
state correctly, run the following command:

.. code-block:: console

   ska-sdp get pb pb-test-20220916-00000/state

and the output should look like this:

.. code-block:: python

   /pb/pb-test-20220916-00000/state = {
     "deployments": {
       "proc-pb-test-20220916-00000-test-receive": "RUNNING"
     },
     "last_updated": "2022-09-16 15:34:27",
     "receive_addresses": {
       "calibration:b": {
         "vis0": {
           "function": "visibilities",
           "host": [
             [
               0,
               "proc-pb-test-20220916-00000-test-receive-0.receive.test-sdp.svc.cluster.local"
             ]
           ],
           "port": [
             [
               0,
               9000,
               1
             ]
           ]
         }
       },
       "target:a": {
         "vis0": {
           "function": "visibilities",
           "host": [
             [
               0,
               "proc-pb-test-20220916-00000-test-receive-0.receive.test-sdp.svc.cluster.local"
             ]
           ],
           "port": [
             [
               0,
               9000,
               1
             ]
           ]
         }
       }
     },
     "resources_available": true,
     "status": "READY"
   }

This should now update the receiveAddresses attribute with receive
addresses map and that can be verified by running ``d.receiveAddresses``
and the output should look like this:

.. code-block:: python

   Out[4]: '{"calibration:b": {"vis0": {"function": "visibilities", "host": [[0, "proc-pb-test-20220916-00000-test-receive-0.receive.test-sdp.svc.cluster.local"]], "port": [[0, 9000, 1]]}}, "target:a": {"vis0": {"function": "visibilities", "host": [[0, "proc-pb-test-20220916-00000-test-receive-0.receive.test-sdp.svc.cluster.local"]], "port": [[0, 9000, 1]]}}}'
