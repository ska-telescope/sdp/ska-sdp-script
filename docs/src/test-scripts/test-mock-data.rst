.. include:: ../../../src/ska-sdp-script-test-mock-data/README.rst

.. _test_mock_data_params:

Processing block parameters
---------------------------

.. autopydantic_settings:: generate_mock_data.test_mock_data_params.TestMockDataParams

.. include:: ../../../src/ska-sdp-script-test-mock-data/CHANGELOG.rst
