"""Input parameter configuration."""

from enum import Enum

from pydantic import BaseModel, Field


class SignalDisplayMetrics(str, Enum):
    """List of metrics that can be requested."""

    ALL = "all"
    STATS = "stats"
    SPECTRUM = "spectrum"
    LAG_PLOT = "lagplot"
    AMPLITUDE = "amplitude"
    PHASE = "phase"
    BAND_AVERAGED_X_CORR = "bandaveragedxcorr"
    UV_COVERAGE = "uvcoverage"

    def __str__(self):
        return self.value


class SignalDisplay(BaseModel):
    """Configuration for the Signal Display Metrics processor."""

    metrics: list[SignalDisplayMetrics] = Field(
        default=[SignalDisplayMetrics.STATS],
        description="List of metrics to generate, or 'all'",
    )
    version: str = Field(
        default=None, description="The version of the processor to use"
    )
    image: str = Field(default=None, description="The image of the processor to use")
    nchan_avg: int = Field(
        default=5,
        description="The number of channels to average together in the data",
        gte=1,
    )
    window_count: int = Field(
        default=5,
        description="The number of additional windows for the configured metrics",
        gte=0,
        lte=10,
    )
    rounding_sensitivity: int = Field(
        default=5,
        description="The amount of significant digits to round the metrics to",
        gt=0,
    )
