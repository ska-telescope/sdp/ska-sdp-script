"""
Example receive script
Ported CBF-SDP receive script
Deploys and update receive addresses attribute with DNS-based IP address
"""

# pylint: disable=invalid-name

import copy
import json
import logging
import os
import pathlib
import sys
import warnings
from dataclasses import fields

import ska_sdp_scripting
import ska_ser_logging
import yaml
from input_parameters import SignalDisplayMetrics
from kafka_operations import create_topic_with_partitions
from models.k8s_defs import EnvVar
from models.receive_processor_chart_values import ReceiveProcessorValues
from models.vis_receive_chart_values import VisReceiveValues
from models.vis_receive_script_params import (
    ObservationAddressMapping,
    ObservationPortCountMapping,
    VisReceiveParams,
)
from pydantic import TypeAdapter
from ska_sdp_config.entity.flow import (
    DataProduct,
    DataQueue,
    FlowSource,
    PVCPath,
    SharedMem,
    SpeadStream,
    TangoAttribute,
    TangoAttributeMap,
    TangoAttributeUrl,
)
from ska_sdp_scripting.data_flow import DataFlow
from ska_sdp_scripting.utils import ProcessingBlockStatus
from subnet_allocation import SubnetAllocator, add_custom_network_definitions_and_ips
from yaml.loader import FullLoader

PROCESSORS_PATH = f"{os.path.dirname(os.path.abspath(__file__))}/processors"
MSWRITER_PROCESSOR = "mswriter"
RCAL_PROCESSOR = "rcal"
TIME_MSWRITER_PROCESSOR = "averagetime-mswriter"

BUILTIN_PROCESSORS = (
    MSWRITER_PROCESSOR,
    RCAL_PROCESSOR,
    TIME_MSWRITER_PROCESSOR,
)
DEFAULT_PROCESSORS = {MSWRITER_PROCESSOR: {}}
KAFKA_HOST = os.getenv("SDP_KAFKA_HOST", "localhost:9092")
PB_ID: str | None = os.getenv("SDP_PB_ID")

# Initialise logging
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

# Signal Display Configurations
SIGNAL_DISPLAY_METRICS = "signal-display-metrics-basic"
SIGNAL_DISPLAY_METRIC_COMBINATIONS = [
    (
        SignalDisplayMetrics.STATS,
        SignalDisplayMetrics.SPECTRUM,
        SignalDisplayMetrics.LAG_PLOT,
        SignalDisplayMetrics.BAND_AVERAGED_X_CORR,
        SignalDisplayMetrics.UV_COVERAGE,
    ),
    (SignalDisplayMetrics.AMPLITUDE,),
    (SignalDisplayMetrics.PHASE,),
]


def _deep_merge_dicts(source: dict, target: dict):
    for name, value in source.items():
        if isinstance(value, dict):
            if name not in target:
                target[name] = {}
            target[name] = _deep_merge_dicts(source[name], target[name])
        else:
            target[name] = source[name]
    return target


def _get_chart_pod_settings(
    pb_pod_settings: VisReceiveParams.PodSettings,
) -> tuple[str | None, VisReceiveParams.PodSettings]:
    """
    Converts processing block pod settings to chart pod settings.

    Returns:
        tuple[str | None, dict]: custom_ip, settings
    """
    custom_ip: str | None = None
    settings = VisReceiveParams.PodSettings()

    if network_mapping := pb_pod_settings.networkMapping:
        network_config = {}
        for field in fields(VisReceiveParams.NetworkMapping):
            if attr_value := getattr(network_mapping, field.name):
                network_config[field.name] = attr_value
        if ip := network_mapping.ip:
            network_mapping.ip = None
            network_config["ips"] = [ip]
            # IP can have subnet mask
            custom_ip = ip.split("/")[0]
        extraMetadata = {
            "annotations": {"k8s.v1.cni.cncf.io/networks": json.dumps([network_config])}
        }
        settings.extraMetadata = extraMetadata

    if nodeSelector := pb_pod_settings.nodeSelector:
        settings.nodeSelector = nodeSelector

    if receiverResources := pb_pod_settings.receiverResources:
        settings.receiverResources = receiverResources

    if securityContext := pb_pod_settings.securityContext:
        settings.securityContext = securityContext

    return custom_ip, settings


def _update_signal_display_tango_flows(
    pb: ska_sdp_scripting.ProcessingBlock,
    subarray_id: str,
    metric_stats_dataqueue_flow: DataFlow,
):
    tango_device = f"tango://mid-sdp/queueconnector/{subarray_id}"
    sink = TangoAttributeMap(
        attributes=[
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(f"{tango_device}/receiver_state"),
                    dtype="DevString",
                    default_value="unknown",
                ),
                TangoAttributeMap.DataQuery(
                    when="type=='visibility_receive'", select="state"
                ),
            ),
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(f"{tango_device}/last_update"),
                    dtype="DevDouble",
                    default_value="0.0",
                ),
                TangoAttributeMap.DataQuery(
                    when="type=='visibility_receive'", select="time"
                ),
            ),
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(
                        f"{tango_device}/processing_block_id"
                    ),
                    dtype="DevString",
                    default_value="",
                ),
                TangoAttributeMap.DataQuery(
                    when="type=='visibility_receive'", select="processing_block_id"
                ),
            ),
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(
                        f"{tango_device}/execution_block_id"
                    ),
                    dtype="DevString",
                    default_value="",
                ),
                TangoAttributeMap.DataQuery(
                    when="type=='visibility_receive'", select="execution_block_id"
                ),
            ),
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(f"{tango_device}/subarray_id"),
                    dtype="DevString",
                    default_value="-1",
                ),
                TangoAttributeMap.DataQuery(
                    when="type=='visibility_receive'", select="subarray_id"
                ),
            ),
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(f"{tango_device}/scan_id"),
                    dtype="DevLong64",
                    default_value="-1",
                ),
                TangoAttributeMap.DataQuery(
                    when="type=='visibility_receive'", select="scan_id"
                ),
            ),
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(
                        f"{tango_device}/payloads_received"
                    ),
                    dtype="DevULong64",
                    default_value="0",
                ),
                TangoAttributeMap.DataQuery(
                    when="type=='visibility_receive'", select="payloads_received"
                ),
            ),
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(
                        f"{tango_device}/time_slices_received"
                    ),
                    dtype="DevULong64",
                    default_value="0",
                ),
                TangoAttributeMap.DataQuery(
                    when="type=='visibility_receive'", select="time_slices"
                ),
            ),
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(
                        f"{tango_device}/time_since_last_payload"
                    ),
                    dtype="DevDouble",
                    default_value="0.0",
                ),
                TangoAttributeMap.DataQuery(
                    when="type=='visibility_receive'",
                    select="time_since_last_payload",
                ),
            ),
        ]
    )
    pb.create_data_flow(
        name=f"metrics-stats-vis-receive-{subarray_id}",
        data_model="Metrics",
        sink=sink,
        sources=[
            FlowSource(
                uri=metric_stats_dataqueue_flow.flow.key,
                function="ska-sdp-lmc-queue-connector:exchange",
            )
        ],
    )


# pylint: disable=too-many-locals,too-many-branches
def update_signal_display_configuration(pb: ska_sdp_scripting.ProcessingBlock):
    """Update Signal Display related configuration.

    This function uses the given ProcessingBlock's parameters to create other
    configuration options, and writes the update configuration back to the
    given ProcessingBlock directly using the dictionary access.

    Flows and pb processor params are updated.

    pb (ProcessingBlock): the in-out processing block.
    """

    pb_parameters = TypeAdapter(VisReceiveParams).validate_python(pb.get_parameters())
    signal_display_config = pb_parameters.signal_display

    for txn in pb._config.txn():
        subarray_id = txn.execution_block.get(pb._eb_id).subarray_id

    default_config = read_receiver_chart_values(SIGNAL_DISPLAY_METRICS, {})
    metrics = signal_display_config.metrics
    if signal_display_config.version is None:
        signal_display_config.version = default_config.version
    if signal_display_config.image is None:
        signal_display_config.image = default_config.image

    if SignalDisplayMetrics.ALL in metrics:
        metrics = [
            metric for inner in SIGNAL_DISPLAY_METRIC_COMBINATIONS for metric in inner
        ]
    elif SignalDisplayMetrics.STATS not in metrics:
        metrics.append(SignalDisplayMetrics.STATS)
    elif len(metrics) == 0:
        metrics.append(SignalDisplayMetrics.STATS)

    if len(pb_parameters.processors) == 0:
        pb_parameters.processors = copy.deepcopy(DEFAULT_PROCESSORS)

    for index, comb in enumerate(SIGNAL_DISPLAY_METRIC_COMBINATIONS):
        check = [c in metrics for c in comb]
        if len(check) > 0 and any(check):
            pod = copy.deepcopy(default_config)
            pod.name = f"signal-display-metrics-{index}"
            pod.version = signal_display_config.version
            pod.image = signal_display_config.image
            pod.command[-1] = ",".join([metric for metric in comb if metric in metrics])
            pb_parameters.processors[f"signal-display-metrics-{index}"] = TypeAdapter(
                ReceiveProcessorValues
            ).dump_python(pod, exclude_none=True)

    metric_stats_flow = None
    for metric in metrics:
        # add the default window to the amount of windows, and use as partitions
        topic_name = f"metrics-{metric}-{subarray_id}"

        partitions = signal_display_config.window_count + 1
        if metric == "stats":
            partitions = 1

        create_topic_with_partitions(
            topic=topic_name,
            partitions=partitions,
        )
        queue = DataQueue(
            topics=topic_name,
            host=KAFKA_HOST,
            format="json" if metric == SignalDisplayMetrics.STATS else "msgpack_numpy",
        )

        # this flow needs to be changed to point to the incoming flow record
        # (i.e. the Plasma flow)
        source = FlowSource(
            uri=f"metrics://signal-display/{subarray_id}/{metric}",
            function=(
                "SignalDisplayMetrics.Stats"
                if metric == "stats"
                else "SignalDisplayMetrics"
            ),
            parameters={
                "metric_type": metric,
                "nchan_avg": signal_display_config.nchan_avg,
                "additional_windows": signal_display_config.window_count,
                "rounding_sensitivity": signal_display_config.rounding_sensitivity,
            },
        )
        flow = pb.create_data_flow(
            name=topic_name,
            data_model="Metrics",
            sink=queue,
            sources=[source],
        )
        if metric == SignalDisplayMetrics.STATS:
            metric_stats_flow = flow

    if metric_stats_flow is None:
        log.warning("Stats metric not found, skipping tango metric flows.")
    else:
        _update_signal_display_tango_flows(pb, subarray_id, metric_stats_flow)

    # pb_parameters gets mutated
    pb.get_parameters().update({"processors": pb_parameters.processors})

    pb.create_data_flow(
        name=f"metrics-receiverstats-{subarray_id}",
        data_model="Spead2Stats",
        sink=DataQueue(
            topics=f"metrics-receiverstats-{subarray_id}",
            host=KAFKA_HOST,
            format="json",
        ),
        sources=[
            FlowSource(
                uri=f"metrics://signal-display/{subarray_id}/receiver-stats",
                function="signal_metrics.stats_receiver",
                parameters={},
            )
        ],
    )


def read_receiver_chart_values(
    filestem: str, extra_values: dict
) -> ReceiveProcessorValues:
    """Read a receive processor helm chart values document from the processors
    directory.

    Args:
        filestem (str): stem part of the YAML filename.
        extra_values (dict): additional values before validation.

    Returns:
        ReceiveProcessorValues: receive processor chart values.
    """
    description_file = pathlib.Path(PROCESSORS_PATH) / f"{filestem}.yaml"
    if description_file.exists():
        with description_file.open() as f:
            values = yaml.load(f, Loader=FullLoader)
    else:
        values = {}
    values = _deep_merge_dicts(extra_values, values)

    if len(values) == 0:
        log.warning("Given processor name, has no configuration: %s", filestem)
    return TypeAdapter(ReceiveProcessorValues).validate_python(values)


def _log_as_yaml(x: dict):
    for line in yaml.safe_dump(x, explicit_start=True, explicit_end=True).splitlines():
        log.info("%s", line, stacklevel=2)
    log.info("", stacklevel=2)


def _update_pointing_dataqueue_flows(
    pb: ska_sdp_scripting.ProcessingBlock,
    telstate: dict[str, str],
    receptors: list[str],
) -> list[DataFlow]:
    """Update processing block with queue connector flows for receive.

    Args:
        pb (ProcessingBlock): processing block to update
        telstate (dict[str, str]): processing block telstate
        receptors (list[str]): receptor names to use as sources

    Returns:
        list[DataFlow]: added dataflow configs
    """
    # mapping of telstate fqdn template keys to output data queue topic
    fqdn_template_map = {
        "target_fqdn": "commanded-pointings",
        "source_offset_fqdn": "source-offsets",
        "direction_fqdn": "actual-pointings",
    }

    # NOTE: "PointingTable" maps to a special QC structured array type:
    # np.dtype([
    #     ("antenna_name", "<U6"),
    #     ("ts", "datetime64[ns]"),
    #     ("az", "float64"),
    #     ("el", "float64"),
    # ])
    return [
        pb.create_data_flow(
            name=f"vis-receive-{topic}",
            data_model="PointingTable",
            sink=DataQueue(
                topics=topic,
                host=KAFKA_HOST,
                format="npy",
            ),
            sources=[
                FlowSource(
                    uri=TangoAttributeUrl(
                        telstate[fqdn_key].format(dish_id=receptor_id)
                    ),
                    function="ska-sdp-lmc-queue-connector:exchange",
                )
                for receptor_id in receptors
            ],
        )
        for fqdn_key, topic in fqdn_template_map.items()
    ]


def update_pointing_data_queue_configuration(pb: ska_sdp_scripting.ProcessingBlock):
    """Create and update queue connector exchanges using the telstate information.

    This creates exchanges for:
    * Commanded pointings
    * Actual pointings
    * Source offsets

    Args:
        pb (ska_sdp_scripting.ProcessingBlock): _description_
    """
    # Get device and attribute names from telstate
    pb_parameters = TypeAdapter(VisReceiveParams).validate_python(pb.get_parameters())
    telstate = pb_parameters.telstate

    for txn in pb._config.txn():
        # Get list of receptors, if the update is going ahead
        # If unavailable, this will raise an error
        resources = txn.execution_block.get(pb._eb_id).resources
        receptors = resources.get("receptors")
        log.info("The receptors are %s", receptors)

    if telstate is None:
        log.warning(
            "No valid telescope state information found."
            "Skipping pointing configuration"
        )
    else:
        log.info("Creating pointing data queue flows.")
        flows = _update_pointing_dataqueue_flows(pb, telstate, receptors)
        log.info("Successfully created pointing data queue flows %s", flows)


def update_receiver_configuration(pb: ska_sdp_scripting.ProcessingBlock):
    """Update the processing block receiver configuration flow.

    Args:
        pb (ska_sdp_scripting.ProcessingBlock): the processing block to update.

    Returns:
        DataFlow: raw visibility shared-memory flow.
    """
    spead_flow = pb.create_data_flow(
        "raw-visibility",
        "Visibility",
        SpeadStream(channel_map={}, receiver_version=""),
        sources=[],
    )
    pb.create_data_flow(
        "raw-visibility",
        "Visibility",
        SharedMem(impl="plasma", host_path="/plasma/socket"),
        sources=[
            FlowSource(
                uri=spead_flow.flow.key,
                function="ska-sdp-realtime-receive:receiver",
            )
        ],
    )


def update_receive_processor_configurations(
    pb: ska_sdp_scripting.ProcessingBlock,
    pvc_name: str,
    pvc_mount_path: str,
):
    """Update the processing block processor configuration flows.

    Args:
        pb (ska_sdp_scripting.ProcessingBlock): the processing block to update.
        pvc_name (str): the PVC name to mount for each processor container.
        pvc_mount_path (str): the PVC mounted location in processor containters.
    """
    pb_parameters = TypeAdapter(VisReceiveParams).validate_python(pb.get_parameters())
    for processor_values in (
        read_receiver_chart_values(name, extra_values)
        for name, extra_values in pb_parameters.processors.items()
    ):
        if processor_values.name == "mswriter-processor":
            pb.create_data_flow(
                name=f"vis-receive-{processor_values.name}",
                data_model="Visibility",
                sink=DataProduct(
                    data_dir=PVCPath(
                        k8s_namespaces=[],
                        k8s_pvc_name=pvc_name,
                        pvc_mount_path=pathlib.Path(pvc_mount_path),
                        pvc_subpath=pathlib.Path(
                            f"product/{pb._eb_id}/ska-sdp/{pb._pb_id}"
                        ),
                    ),
                    paths=[],
                ),
            )


# pylint: disable=too-many-statements
def setup_vis_receive_processing_block(
    pb: ska_sdp_scripting.ProcessingBlock, subnet_allocator: SubnetAllocator
) -> tuple[dict, list[str], ObservationAddressMapping]:
    """Setup the processing block and generate values for the visibility receive chart.

    Args:
        pb (ska_sdp_scripting.ProcessingBlock): processing block
        subnet_allocator (SubnetAllocator): allocator for subnet

    Returns:
        tuple[dict, list, list]: values, custom_ips, calculated_recv_addresses
    """
    log.info("Input Processing Block parameters:")
    _log_as_yaml(pb.get_parameters())

    # Update processing block configuration
    update_signal_display_configuration(pb)
    update_pointing_data_queue_configuration(pb)

    # Generate params
    execution_block_id = pb._eb_id
    pb_parameters = pb.get_parameters()
    for txn in pb._config.txn():
        subarray_id = txn.execution_block.get(pb._eb_id).subarray_id

    log.info("Processing Block parameters:")
    _log_as_yaml(pb_parameters)

    pb_parameters = TypeAdapter(VisReceiveParams).validate_python(pb.get_parameters())

    # Multiplicity settings. Node multiplicity is given either by indicating
    # the number of nodes to use (num_nodes), or the maximum number of ports to
    # open per node (max_ports_per_node)
    channels_per_port = pb_parameters.channels_per_port
    processes_per_node = pb_parameters.processes_per_node
    max_ports_per_node = pb_parameters.max_ports_per_node
    requested_num_nodes = pb_parameters.num_nodes
    recv_address_options = {}
    if max_ports_per_node is not None:
        if requested_num_nodes is not None:
            log.warning(
                "Ignoring num_nodes=%d parameter in preference for "
                "max_ports_per_node",
                requested_num_nodes,
            )
        max_channels_per_node = max_ports_per_node * channels_per_port
        recv_address_options["max_ports_per_host"] = max_channels_per_node
    else:
        if requested_num_nodes is None:
            requested_num_nodes = 1
            log.info("Defaulting num_nodes=1 for vis-receive deployment")
        max_channels_per_node = None
        recv_address_options["num_hosts"] = requested_num_nodes

    # Determine hosts and port ranges for per scan type and beam
    port_start = pb_parameters.port_start
    scan_types = pb.get_scan_types()
    hpcm = pb.config_host_port_channel_map(
        scan_types, port_start, channels_per_port, **recv_address_options
    )
    calculated_recv_addresses = TypeAdapter(ObservationAddressMapping).validate_python(
        hpcm[0]
    )
    host_port_count = TypeAdapter(ObservationPortCountMapping).validate_python(hpcm[1])
    num_nodes = max(
        len(count) for beams in host_port_count.values() for count in beams.values()
    )
    # Currently simplifying the situation so all hosts are configured to listen
    # on the same number of ports
    ports_per_node = max(
        max(count) for beams in host_port_count.values() for count in beams.values()
    )

    log.info("Receiver multiplicity will be as follows:")
    log.info("  Given parameters:")
    log.info("    Channels per port:      %d", channels_per_port)
    log.info("    Max ports per node:     %s", max_ports_per_node)
    log.info("    Number of nodes:        %s", requested_num_nodes)
    log.info("    Processes per node:     %d", processes_per_node)
    log.info("  Calculated:")
    log.info("    Number of nodes:        %d", num_nodes)
    log.info("    Max channels per node:  %s", max_channels_per_node)
    log.info("    Ports per node:         %d", ports_per_node)

    # Native per-pod settings extended/trimmed to num_nodes elements
    pb_pod_settings = pb_parameters.pod_settings
    missing_pod_settings = num_nodes - len(pb_pod_settings)
    pb_pod_settings += [VisReceiveParams.PodSettings()] * max(missing_pod_settings, 0)
    del pb_pod_settings[num_nodes:]
    if missing_pod_settings != 0:
        log.info(
            "Given pod settings were %s to match number of pods %d",
            (
                f"extended by {missing_pod_settings}"
                if missing_pod_settings > 0
                else f"trimmed by {-missing_pod_settings}"
            ),
            num_nodes,
        )

    custom_ips: list[str]
    pod_settings: list[VisReceiveParams.PodSettings]
    custom_ips, pod_settings = zip(
        *[_get_chart_pod_settings(pod_settings) for pod_settings in pb_pod_settings]
    )

    if subnet_allocator.allocated_subnet:
        custom_ips, pod_settings = add_custom_network_definitions_and_ips(
            pod_settings,
            subnet_allocator,
        )
    else:
        log.info(
            "Parameter use_network_definition False or does not exist, "
            "so not adding/using any network attachment definition."
        )

    sdp_config_host = os.getenv("SDP_CONFIG_HOST", "127.0.0.1")
    sdp_config_port = int(os.getenv("SDP_CONFIG_PORT", "2379"))
    data_pvc_name = os.getenv("SDP_DATA_PVC_NAME", "data-product-pvc")
    data_pvc_mount_path = "/data"
    stats_receiver_config = f"{KAFKA_HOST}:metrics-receiverstats-{subarray_id}"

    update_receive_processor_configurations(
        pb,
        pvc_name=data_pvc_name,
        pvc_mount_path=data_pvc_mount_path,
    )
    values = VisReceiveValues(
        script="vis-receive",
        podSettings=pod_settings,
        dataProductStorage=VisReceiveValues.DataProductStorage(
            name=data_pvc_name,
            mountPath=data_pvc_mount_path,
        ),
        env=[
            EnvVar(name=name, value=value)
            for name, value in {
                "SDP_PB_ID": PB_ID,
                "SDP_KAFKA_HOST": KAFKA_HOST,
                "SDP_CONFIG_HOST": sdp_config_host,
                "SDP_CONFIG_PORT": str(sdp_config_port),
                "http_proxy": os.getenv("http_proxy"),
                "https_proxy": os.getenv("https_proxy"),
                "no_proxy": os.getenv("no_proxy"),
            }.items()
            if value is not None
        ],
        receiver=VisReceiveValues.Receiver(
            version="6.0.0",
            streams=ports_per_node,
            instances=processes_per_node,
            portStart=port_start,
            options=VisReceiveValues.Options(
                reception=VisReceiveValues.Reception(
                    transport_protocol=pb_parameters.transport_protocol,
                    stats_receiver_kafka_config=stats_receiver_config,
                ),
                telescope_model=VisReceiveValues.TelescopeModel(
                    execution_block_id=execution_block_id,
                    telmodel_key="instrument/ska1_low/layout/low-layout.json",
                ),
                scan_provider=VisReceiveValues.ScanProvider(
                    execution_block_id=execution_block_id,
                ),
                sdp_config_db=VisReceiveValues.SdpConfigDb(
                    host=sdp_config_host,
                    port=sdp_config_port,
                ),
            ),
        ),
        processors=[
            read_receiver_chart_values(name, description)
            for name, description in pb_parameters.processors.items()
        ],
    )

    values_dict = TypeAdapter(VisReceiveValues).dump_python(
        values, exclude_none=True, by_alias=True
    )
    # Override with other helm values verbatim
    values_dict = _deep_merge_dicts(pb_parameters.extra_helm_values, values_dict)

    log.info("Calculated Helm values:")
    _log_as_yaml(values_dict)

    return values_dict, custom_ips, calculated_recv_addresses


def _override_receive_addresses(
    pb: ska_sdp_scripting.ProcessingBlock, custom_ips: list[str]
):
    for txn in pb._config.txn():
        state = txn.processing_block.state(pb._pb_id).get()
        receive_addresses = state["receive_addresses"]
        for beams in receive_addresses.values():
            for values in beams.values():
                host_channel_map = values["host"]
                assert len(custom_ips) >= len(host_channel_map)
                for i, host in enumerate(host_channel_map):
                    if custom_ips[i]:
                        host[1] = custom_ips[i]
        txn.processing_block.state(pb._pb_id).update(state)


def setup_and_run_execution_block(
    pb: ska_sdp_scripting.ProcessingBlock, subnet_allocator: SubnetAllocator
):
    """Setup and run an execution block.

    Configuration is taken from a processing block and subnet allocator
    and executes via the helm deployer.

    Args:
        pb (ska_sdp_scripting.ProcessingBlock): processing block
        subnet_allocator (SubnetAllocator): k8s subnet allocator
    """
    log.info("Setting default values")

    values, custom_ips, host_port = setup_vis_receive_processing_block(
        pb, subnet_allocator
    )

    # Create work phase
    log.info("Create work phase")
    work_phase: ska_sdp_scripting.phase.Phase = pb.create_phase("Work", [])

    with work_phase:
        # A "dry run" produces no deployment of the vis-receive chart,
        # useful for debugging of the output Helm values, and the generated
        # receive addresses.
        dry_run = pb.get_parameters().get("dry_run", False)

        # Deploy visibility receive
        deploy_name = "vis-receive"
        if dry_run:
            chart_name = f"proc-{pb._pb_id}-{deploy_name}"
        else:
            chart_name = None
            work_phase.ee_deploy_helm(deploy_name, values)

        # Generate DNS names and add receive addresses to pb.
        pb.receive_addresses(
            chart_name=chart_name,
            configured_host_port=TypeAdapter(ObservationAddressMapping).dump_python(
                host_port, exclude_none=True
            ),
            service_name=f"proc-{pb._pb_id}-vis-receive",
        )

        # Override pb state receive addresses with custom IP if given
        if any(custom_ips):
            _override_receive_addresses(pb, custom_ips)
        work_phase.update_pb_state(status=ProcessingBlockStatus.READY)

        log.info("Done, now idling...")

        work_phase.wait_loop(work_phase.is_eb_finished)


def main():
    """Script entrypoint."""
    if not sys.warnoptions:
        warnings.simplefilter("once", DeprecationWarning)
    ska_ser_logging.configure_logging()

    # Claim processing block
    with ska_sdp_scripting.ProcessingBlock() as pb:
        with SubnetAllocator.from_pb(pb) as subnet_allocator:
            setup_and_run_execution_block(pb, subnet_allocator)


if __name__ == "__main__":
    main()
